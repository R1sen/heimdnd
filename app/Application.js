/**
 * The main application class. An instance of this class is created by app.js when it
 * calls Ext.application(). This is the ideal place to handle application launch and
 * initialization details.
 */
Ext.enableAriaPanels = false;

Ext.define('DwD2.Application', {
    extend: 'Ext.app.Application',
    
    name: 'DwD2',

    stores: [
        // TODO: add global / shared stores here
		'CharsInLoc',
        'CharsInLocRaw',
        'AllTargets',
        'SpellsTree',
        'PowersTree',
        'ActualSlots',
        'HandSelect',
        'ImprovisedStore',
        'LocsStore',
        'InfoSpellsTree',
        'InfoPowersTree',
        'NPCTree',
        'NPCStore',
        'DiscretSelect',
        'FactorSelect',
        'MapStore',
        'BacksStore',
        'RacesStore',
        'ClassesStore',
        'AllSpellsTree',
        'AlignmentList',
        'MarketTree'

    ],
    
    launch: function () {
        // TODO - Launch the application
    },

	controllers: [
        'Root@DwD2.controller'
    ],

    onAppUpdate: function () {
        Ext.Msg.confirm('Application Update', 'This application has an update, reload?',
            function (choice) {
                if (choice === 'yes') {
                    window.location.reload();
                }
            }
        );
    }
});
