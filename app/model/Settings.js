Ext.define('DwD2.model.Settings', {
    extend: 'DwD2.model.Base',
    
    fields: [
        { name: 'ssid', type: 'auto' },
        { name: 'lastmid', type: 'auto' },
		{ name: 'dm', type: 'auto' },
		{ name: 'minidm', type: 'auto' },
		{ name: 'cid', type: 'auto' }
    ],

	proxy: {
		type: 'memory',
		reader: {
		    type: 'json',
		    rootProperty: 'settings'
		}
	}
});
