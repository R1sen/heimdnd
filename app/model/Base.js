/**
 * Created by gg_truschalov on 18.08.2016.
 */
Ext.define('DwD2.model.Base', {
    extend: 'Ext.data.Model',

    fields: [{
        name: 'baseId',
        type: 'auto'
    }],

    idProperty: 'baseId',

    schema: {
        namespace: 'DwD2.model'
    }
});
