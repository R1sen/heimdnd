Ext.define('DwD2.model.CharInLoc', {
    extend: 'DwD2.model.Base',
    
    fields: [
        { name: 'id', type: 'auto' },
        { name: 'name', type: 'auto' }
    ],

	proxy: {
		type: 'memory',
        reader: {
            type: 'json'
        }
	}
});
