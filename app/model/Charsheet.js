Ext.define('DwD2.field.PlusF', {
    extend: 'Ext.data.field.Field',

    alias: 'data.field.plusf',

    convert: function (value) {
        if (value >= 0){
            value = '+' + value;
        }

        return value;
    }
});

Ext.define('DwD2.field.ProfF', {
    extend: 'Ext.data.field.Field',

    alias: 'data.field.proff',

    convert: function (value) {
        if (value){
            value = '●';
        } else {
            value = '○';
        }

        return value;
    }
});

Ext.define('DwD2.field.ProfSaveF', {
    extend: 'Ext.data.field.Field',

    alias: 'data.field.profsavef',

    convert: function (value) {
        if (value){
            value = '■';
        } else {
            value = '□';
        }

        return value;
    }
});

Ext.define('DwD2.field.UseHitDieF', {
    extend: 'Ext.data.field.Field',

    alias: 'data.field.usehitdief',

    convert: function (value) {
        if (value){
            var d_regex = /\d{1,2}d\d{1,2}/g;

            return value.replace(d_regex, "<a onclick=\"Ext.getCmp('charlist-tabpanel').fireEvent('usehitdie', '$&')\">$&</a>");
        }
    }
});

Ext.define('DwD2.field.InspirationF', {
    extend: 'Ext.data.field.Field',

    alias: 'data.field.inspirationf',

    convert: function (value) {
        if (value && value == 1){
            value = 'Есть';
        } else {
            value = 'Нет';
        }

        return value;
    }
});

Ext.define('DwD2.model.Charsheet', {
    extend: 'DwD2.model.Base',
    
    fields: [
        { name: 'id', type: 'auto' },
        { name: 'name', type: 'auto' },
        { name: 'class', type: 'auto' },
        { name: 'race_name', type: 'auto' },
        { name: 'exp', type: 'auto' },
        { name: 'lvl', type: 'auto' },
        { name: 'next_lvl', type: 'auto' },
        { name: 'back_name', type: 'auto' },
        { name: 'size', type: 'auto' },
        { name: 'inspiration', type: 'inspirationf' },
        { name: 'align', type: 'auto' },

        { name: 'lvlup_ready', type: 'auto' },

        { name: 'dm', type: 'auto' },

        { name: 'prof_bonus', type: 'plusf' },

        { name: 'b_icon', type: 'auto' },
        { name: 'picture', type: 'auto' },

        { name: 'ac', type: 'auto' },
        { name: 'hp', type: 'auto' },
        { name: 'total_hp', type: 'auto' },
        { name: 'temp_hp', type: 'auto' },
        { name: 'hit_dice', type: 'usehitdief' },
        { name: 'hit_dice_total', type: 'auto' },
        { name: 'death_saves_true', type: 'auto' },
        { name: 'death_saves_false', type: 'auto' },

        { name: 'initiative', type: 'plusf' },
        { name: 'speed', type: 'auto' },
        { name: 'fly', type: 'auto' },
        { name: 'swim', type: 'auto' },
        { name: 'long_jump', type: 'auto' },
        { name: 'high_jump', type: 'auto' },
        { name: 'crawl', type: 'auto' },
        { name: 'climb', type: 'auto' },
        { name: 'speed_cell', type: 'auto' },
        { name: 'fly_cell', type: 'auto' },
        { name: 'swim_cell', type: 'auto' },
        { name: 'long_jump_cell', type: 'auto' },
        { name: 'high_jump_cell', type: 'auto' },
        { name: 'crawl_cell', type: 'auto' },
        { name: 'climb_cell', type: 'auto' },

        { name: 'passive_perception', type: 'auto' },
        { name: 'passive_investigation', type: 'auto' },

        { name: 'vis_spec_sen', type: 'auto' },

        { name: 'str', type: 'auto' },
        { name: 'dex', type: 'auto' },
        { name: 'con', type: 'auto' },
        { name: 'intl', type: 'auto' },
        { name: 'wis', type: 'auto' },
        { name: 'cha', type: 'auto' },

        { name: 'str_save', type: 'plusf' },
        { name: 'dex_save', type: 'plusf' },
        { name: 'con_save', type: 'plusf' },
        { name: 'intl_save', type: 'plusf' },
        { name: 'wis_save', type: 'plusf' },
        { name: 'cha_save', type: 'plusf' },

        { name: 'str_save_prof', type: 'profsavef' },
        { name: 'dex_save_prof', type: 'profsavef' },
        { name: 'con_save_prof', type: 'profsavef' },
        { name: 'intl_save_prof', type: 'profsavef' },
        { name: 'wis_save_prof', type: 'profsavef' },
        { name: 'cha_save_prof', type: 'profsavef' },

        { name: 'athletics', type: 'plusf' },
        { name: 'athletics_prof', type: 'proff' },

        { name: 'weight', type: 'auto' },
        { name: 'weight_max', type: 'auto' },
        { name: 'push_drag_lift', type: 'auto' },
        { name: 'weight_kg', type: 'auto' },
        { name: 'weight_max_kg', type: 'auto' },
        { name: 'push_drag_lift_kg', type: 'auto' },

        { name: 'acrobatics', type: 'plusf' },
        { name: 'acrobatics_prof', type: 'proff' },
        { name: 'sleight', type: 'plusf' },
        { name: 'sleight_prof', type: 'proff' },
        { name: 'stealth', type: 'plusf' },
        { name: 'stealth_prof', type: 'proff' },

        { name: 'breath', type: 'auto' },
        { name: 'suffocating', type: 'auto' },

        { name: 'investigation', type: 'plusf' },
        { name: 'investigation_prof', type: 'proff' },
        { name: 'arcana', type: 'plusf' },
        { name: 'arcana_prof', type: 'proff' },
        { name: 'history', type: 'plusf' },
        { name: 'history_prof', type: 'proff' },
        { name: 'nature', type: 'plusf' },
        { name: 'nature_prof', type: 'proff' },
        { name: 'religion', type: 'plusf' },
        { name: 'religion_prof', type: 'proff' },

        { name: 'perception', type: 'plusf' },
        { name: 'perception_prof', type: 'proff' },
        { name: 'medicine', type: 'plusf' },
        { name: 'medicine_prof', type: 'proff' },
        { name: 'survival', type: 'plusf' },
        { name: 'survival_prof', type: 'proff' },
        { name: 'insight', type: 'plusf' },
        { name: 'insight_prof', type: 'proff' },
        { name: 'animal', type: 'plusf' },
        { name: 'animal_prof', type: 'proff' },

        { name: 'performance', type: 'plusf' },
        { name: 'performance_prof', type: 'proff' },
        { name: 'intimidation', type: 'plusf' },
        { name: 'intimidation_prof', type: 'proff' },
        { name: 'persuasion', type: 'plusf' },
        { name: 'persuasion_prof', type: 'proff' },
        { name: 'deception', type: 'plusf' },
        { name: 'deception_prof', type: 'proff' },

        { name: 'items', type: 'auto' },
        { name: 'powers', type: 'auto' },
        { name: 'feats', type: 'auto' },

        { name: 'class_info', type: 'auto' },

        { name: 'spellcaster', type: 'boolean' },
        { name: 'spells', type: 'auto' },
        { name: 'spells_tree', type: 'auto' },
        { name: 'actual_slots', type: 'auto' },

        { name: 'slot_1', calculate: function (data) {
            if (data.actual_slots){
                return data.actual_slots[0];
            }
        }},
        { name: 'slot_2', calculate: function (data) {
            if (data.actual_slots){
                return data.actual_slots[1];
            }
        } },
        { name: 'slot_3', calculate: function (data) {
            if (data.actual_slots){
                return data.actual_slots[2];
            }
        } },
        { name: 'slot_4', calculate: function (data) {
            if (data.actual_slots){
                return data.actual_slots[3];
            }
        } },
        { name: 'slot_5', calculate: function (data) {
            if (data.actual_slots){
                return data.actual_slots[4];
            }
        } },
        { name: 'slot_6', calculate: function (data) {
            if (data.actual_slots){
                return data.actual_slots[5];
            }
        } },
        { name: 'slot_7', calculate: function (data) {
            if (data.actual_slots){
                return data.actual_slots[6];
            }
        } },
        { name: 'slot_8', calculate: function (data) {
            if (data.actual_slots){
                return data.actual_slots[7];
            }
        } },
        { name: 'slot_9', calculate: function (data) {
            if (data.actual_slots){
                return data.actual_slots[8];
            }
        } },

        { name: 'spellcaster_class', type: 'auto' },
        { name: 'spellcaster_attrs', type: 'auto' },
        { name: 'spellcaster_saves', type: 'auto' },
        { name: 'spellcaster_prepares', type: 'auto' },
        { name: 'spellcaster_lvl', type: 'auto' },

        { name: 'spells_slots', type: 'auto' },

        { name: 'powers_tree', type: 'auto' },

        { name: 'eyes', type: 'auto' },
        { name: 'head', type: 'auto' },
        { name: 'neck', type: 'auto' },
        { name: 'coat', type: 'auto' },
        { name: 'ring_1', type: 'auto' },
        { name: 'ring_2', type: 'auto' },
        { name: 'arms', type: 'auto' },
        { name: 'hands', type: 'auto' },
        { name: 'armor', type: 'auto' },
        { name: 'mainhand', type: 'auto' },
        { name: 'offhand', type: 'auto' },
        { name: 'torso', type: 'auto' },
        { name: 'waist', type: 'auto' },
        { name: 'feet', type: 'auto' },
        { name: 'wondrous_1', type: 'auto' },
        { name: 'wondrous_2', type: 'auto' },
        { name: 'wondrous_3', type: 'auto' },
        { name: 'wondrous_4', type: 'auto' },
        { name: 'wondrous_5', type: 'auto' },

        { name: 'eyes_object', type: 'auto' },
        { name: 'head_object', type: 'auto' },
        { name: 'neck_object', type: 'auto' },
        { name: 'coat_object', type: 'auto' },
        { name: 'ring_1_object', type: 'auto' },
        { name: 'ring_2_object', type: 'auto' },
        { name: 'arms_object', type: 'auto' },
        { name: 'hands_object', type: 'auto' },
        { name: 'armor_object', type: 'auto' },
        { name: 'mainhand_object', type: 'auto' },
        { name: 'offhand_object', type: 'auto' },
        { name: 'torso_object', type: 'auto' },
        { name: 'waist_object', type: 'auto' },
        { name: 'feet_object', type: 'auto' },
        { name: 'wondrous_1_object', type: 'auto' },
        { name: 'wondrous_2_object', type: 'auto' },
        { name: 'wondrous_3_object', type: 'auto' },
        { name: 'wondrous_4_object', type: 'auto' },
        { name: 'wondrous_5_object', type: 'auto' },

        { name: 'platinum', type: 'auto' },
        { name: 'gold', type: 'auto' },
        { name: 'silver', type: 'auto' },
        { name: 'copper', type: 'auto' },

        { name: 'warslot_1', type: 'auto' },
        { name: 'warslot_2', type: 'auto' },
        { name: 'warslot_3', type: 'auto' },
        { name: 'warslot_4', type: 'auto' },
        { name: 'warslot_5', type: 'auto' },
        { name: 'warlock', type: 'auto' }
    ],

    proxy: {
        type: 'memory',
        reader: {
            type: 'json',
            rootProperty: 'char'
        }
    }

});
