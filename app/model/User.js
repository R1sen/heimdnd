Ext.define('DwD2.model.User', {
    extend: 'DwD2.model.Base',
    
    fields: [
        { name: 'login', type: 'auto' },
        { name: 'pass', type: 'auto' },
		{ name: 'chars', type: 'auto' }
    ],

	proxy: {
		type: 'memory',
        reader: {
            type: 'json',
            rootProperty: 'user'
        }
	}
});
