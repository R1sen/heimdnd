/**
 * Created by gg_truschalov on 08.09.2016.
 */
Ext.define('DwD2.store.PowersTree', {
    extend: 'Ext.data.TreeStore',
    storeId: 'powersTree',

    proxy: {
        type: 'memory',
        reader: {
            type: 'json'
        }
    },

    sorters: [{
        property: 'leaf',
        direction: 'DESC'
    }, {
        property: 'text',
        direction: 'ASC'
    }]
});