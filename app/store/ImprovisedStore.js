/**
 * Created by gg_truschalov on 12.09.2016.
 */
Ext.define('DwD2.store.ImprovisedStore', {
    extend: 'Ext.data.Store',
    model: 'DwD2.model.CharInLoc',
    storeId: 'improvisedStore',

    data: [{
        name: 'Рукопашное',
        id: 1
    },{
        name: 'Дальнобойное',
        id: 2
    }],

    proxy: {
        type: 'memory',
        reader: {
            type: 'json'
        }
    }
});