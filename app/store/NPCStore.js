/**
 * Created by gg_truschalov on 22.09.2016.
 */
Ext.define('DwD2.store.NPCStore', {
    extend: 'Ext.data.Store',
    model: 'DwD2.model.CharInLoc',
    storeId: 'npcStore',

    proxy: {
        type: 'memory',
        reader: {
            type: 'json',
            rootProperty: 'npcs'
        }
    }
});