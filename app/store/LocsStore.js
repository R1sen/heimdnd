/**
 * Created by gg_truschalov on 13.09.2016.
 */
Ext.define('DwD2.store.LocsStore', {
    extend: 'Ext.data.Store',
    model: 'DwD2.model.CharInLoc',
    storeId: 'locsStore',
    
    proxy: {
        type: 'memory',
        reader: {
            type: 'json',
            rootProperty: 'locs'
        }
    }
});