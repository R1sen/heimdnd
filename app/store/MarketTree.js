/**
 * Created by gg_truschalov on 08.09.2016.
 */
Ext.define('DwD2.store.MarketTree', {
    extend: 'Ext.data.TreeStore',
    storeId: 'marketItemsTree',

    proxy: {
        type: 'memory',
        reader: {
            type: 'json'
        }
    },

    sorters: [{
        property: 'leaf',
        direction: 'DESC'
    }, {
        property: 'text',
        direction: 'ASC'
    }]
});