/**
 * Created by Bugzy on 11.09.2016.
 */
Ext.define('DwD2.store.ActualSlots', {
    extend: 'Ext.data.Store',
    model: 'DwD2.model.CharInLoc',
    storeId: 'actualSlots',

    data: [{
        name: 'Заговор/Талант',
        id: 0
    }],

    proxy: {
        type: 'memory',
        reader: {
            type: 'json'
        }
    }
});