/**
 * Created by gg_truschalov on 03.10.2016.
 */
Ext.define('DwD2.store.BacksStore', {
    extend: 'Ext.data.Store',
    model: 'DwD2.model.CharInLoc',
    storeId: 'backsStore',

    proxy: {
        type: 'memory',
        reader: {
            type: 'json',
            rootProperty: 'backs'
        }
    }
});