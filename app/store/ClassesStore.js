/**
 * Created by gg_truschalov on 03.10.2016.
 */
Ext.define('DwD2.store.ClassesStore', {
    extend: 'Ext.data.Store',
    model: 'DwD2.model.CharInLoc',
    storeId: 'classesStore',

    proxy: {
        type: 'memory',
        reader: {
            type: 'json',
            rootProperty: 'classes'
        }
    }
});