/**
 * Created by gg_truschalov on 14.10.2016.
 */
Ext.define('DwD2.store.AllSpellsTree', {
    extend: 'Ext.data.TreeStore',
    storeId: 'allSpellsTree',

    proxy: {
        type: 'memory',
        reader: {
            type: 'json'
        }
    },

    sorters: [{
        property: 'leaf',
        direction: 'DESC'
    }, {
        property: 'text',
        direction: 'ASC'
    }]
});