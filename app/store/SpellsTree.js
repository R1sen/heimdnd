/**
 * Created by gg_truschalov on 06.09.2016.
 */
Ext.define('DwD2.store.SpellsTree', {
    extend: 'Ext.data.TreeStore',
    storeId: 'spellsTree',

    proxy: {
        type: 'memory',
        reader: {
            type: 'json'
        }
    },

    sorters: [{
        property: 'leaf',
        direction: 'DESC'
    }, {
        property: 'text',
        direction: 'ASC'
    }]
});