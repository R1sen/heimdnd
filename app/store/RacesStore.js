/**
 * Created by gg_truschalov on 03.10.2016.
 */
Ext.define('DwD2.store.RacesStore', {
    extend: 'Ext.data.Store',
    model: 'DwD2.model.CharInLoc',
    storeId: 'racesStore',

    proxy: {
        type: 'memory',
        reader: {
            type: 'json',
            rootProperty: 'races'
        }
    }
});