/**
 * Created by gg_truschalov on 23.09.2016.
 */
Ext.define('DwD2.store.DiscretSelect', {
    extend: 'Ext.data.Store',
    model: 'DwD2.model.CharInLoc',
    storeId: 'discretSelect',

    data: [{
        id: 0,
        name: 'По кубику'
    },{
        id: 1,
        name: 'Фиксированный'
    }],

    proxy: {
        type: 'memory',
        reader: {
            type: 'json'
        }
    }
});