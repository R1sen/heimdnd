/**
 * Created by Bugzy on 03.01.2017.
 */
Ext.define('DwD2.store.AlignmentList', {
    extend: 'Ext.data.Store',
    model: 'DwD2.model.CharInLoc',
    storeId: 'alignmentList',

    data: [{
        name: 'Законно-доброе',
        id: 0
    },{
        name: 'Нейтрально-доброе',
        id: 1
    },{
        name: 'Хаотично-доброе',
        id: 2
    },{
        name: 'Законно-нейтральное',
        id: 3
    },{
        name: 'Нейтральное',
        id: 4
    },{
        name: 'Хаотично-нейтральное',
        id: 5
    },{
        name: 'Законно-злое',
        id: 6
    },{
        name: 'Нейтрально-злое',
        id: 7
    },{
        name: 'Хаотично-злое',
        id: 8
    }],

    proxy: {
        type: 'memory',
        reader: {
            type: 'json'
        }
    }
});