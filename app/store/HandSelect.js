/**
 * Created by gg_truschalov on 12.09.2016.
 */
Ext.define('DwD2.store.HandSelect', {
    extend: 'Ext.data.Store',
    model: 'DwD2.model.CharInLoc',
    storeId: 'handSelect',

    proxy: {
        type: 'memory',
        reader: {
            type: 'json'
        }
    }
});