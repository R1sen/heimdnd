/**
 * Created by Bugzy on 17.09.2016.
 */
Ext.define('DwD2.store.InfoPowersTree', {
    extend: 'Ext.data.TreeStore',
    storeId: 'infoPowersTree',

    proxy: {
        type: 'memory',
        reader: {
            type: 'json'
        }
    },

    sorters: [{
        property: 'leaf',
        direction: 'DESC'
    }, {
        property: 'text',
        direction: 'ASC'
    }]
});