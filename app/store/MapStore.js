/**
 * Created by gg_truschalov on 26.09.2016.
 */
Ext.define('DwD2.store.MapStore', {
    extend: 'Ext.data.Store',
    model: 'DwD2.model.CharInLoc',
    storeId: 'mapStore',

    proxy: {
        type: 'memory',
        reader: {
            type: 'json',
            rootProperty: 'maps'
        }
    }
});