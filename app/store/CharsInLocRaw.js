/**
 * Created by gg_truschalov on 24.08.2016.
 */
Ext.define('DwD2.store.CharsInLocRaw', {
    extend: 'Ext.data.Store',
    model: 'DwD2.model.CharInLoc',
    storeId: 'charsInLocRaw',

    proxy: {
        type: 'memory',
        reader: {
            type: 'json',
            rootProperty: 'allchars'
        }
    }
});