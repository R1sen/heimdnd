/**
 * Created by gg_truschalov on 20.09.2016.
 */
Ext.define('DwD2.store.NPCTree', {
    extend: 'Ext.data.TreeStore',
    storeId: 'npcTree',

    proxy: {
        type: 'memory',
        reader: {
            type: 'json'
        }
    },

    sorters: [{
        property: 'leaf',
        direction: 'DESC'
    }, {
        property: 'text',
        direction: 'ASC'
    }]
});