/**
 * Created by Bugzy on 11.09.2016.
 */
Ext.define('DwD2.store.AllTargets', {
    extend: 'Ext.data.Store',
    model: 'DwD2.model.CharInLoc',
    storeId: 'allTargets',

    data: [{
        name: 'Неопределённая цель',
        id: 0
    }],

    proxy: {
        type: 'memory',
        reader: {
            type: 'json',
            rootProperty: 'chars'
        }
    }
});