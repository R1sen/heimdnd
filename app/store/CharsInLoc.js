Ext.define('DwD2.store.CharsInLoc', {
    extend: 'Ext.data.Store',
    model: 'DwD2.model.CharInLoc',
    storeId: 'charsInLoc',

	data: [{
		name: 'В чат',
		id: 0
	}],
	
	proxy: {
		type: 'memory',
        reader: {
            type: 'json',
            rootProperty: 'chars'
        }
	}
});