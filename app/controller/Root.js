Ext.define('DwD2.controller.Root', {
    extend: 'Ext.app.Controller',

    requires: [
		'DwD2.model.*',
		'DwD2.store.*',
        'DwD2.view.charchooser.Charchooser',
		'DwD2.view.login.Login',
		'DwD2.RequestManager',
		'DwD2.view.main.Main',
		'DwD2.view.switchloc.Switchloc',
		'DwD2.view.charcreator.Charcreator',
		'DwD2.view.inwin.Basewin',
		'DwD2.view.inwin.charlist.Charlist',
		'DwD2.view.inwin.makecheck.Makecheck',
		'DwD2.view.inwin.useitem.Useitem',
		'DwD2.view.inwin.showpower.Showpower',
		'DwD2.view.inwin.usespell.Usespell',
		'DwD2.view.inwin.showspell.Showspell',
		'DwD2.view.inwin.showfeat.Showfeat',
		'DwD2.view.inwin.makeattack.Makeattack',
		'DwD2.view.inwin.makedamage.Makedamage',
		'DwD2.view.inwin.dices.Dices',
		'DwD2.view.inwin.echo.Echo',
		'DwD2.view.inwin.whowhere.Whowhere',
		'DwD2.view.inwin.powerdice.Powerdice',
		'DwD2.view.inwin.spellattack.Spellattack',
		'DwD2.view.inwin.spelldamage.Spelldamage',
		'DwD2.view.inwin.charinfo.Charinfo',
		'DwD2.view.inwin.dm.Dm',
		'DwD2.view.inwin.usenpc.Usenpc',
		'DwD2.view.inwin.npcattack.Npcattack',
		'DwD2.view.inwin.npcdamage.Npcdamage',
		'DwD2.view.inwin.npccheck.Npccheck',
		'DwD2.view.inwin.map.Map',
		'DwD2.view.inwin.npcinfo.Npcinfo',
		'DwD2.view.inwin.showhelp.Showhelp',
		'DwD2.view.inwin.showback.Showback',
		'DwD2.view.inwin.lvlup.Lvlup',
        'DwD2.view.inwin.market.Market'
	],

	onLaunch: function () {
        // TODO - Launch the application
		Ext.tip.QuickTipManager.init();
		
		this.session = Ext.create('Ext.data.Session', {
            autoDestroy: false
        });

		this.requestManager = Ext.create('DwD2.RequestManager', {
			session: this.session,
			listeners: {
				scope: this,
				error: 'onError'
			}
		});

		this.viewport = Ext.create('widget.app-main', {
			session: this.session,
			requestManager: this.requestManager,
			windowManager: new Array(
				{
					id: "reserved",
					opened: false,
					minimized: false
				}
			),
			listeners: {
				scope: this,
				logout: 'onLogout',
				switchloc: 'onSwitchloc',
				switchedloc: 'onSwitchedloc',
				openwin: 'onOpenWin',
				closewin: 'onCloseWin',
				minimizewin: 'onMinimizeWin',
				restorewin: 'onRestoreWin',
				opencharlist: 'onCharlist',
				opendices: 'onOpenDices',
				openecho: 'onOpenEcho',
				showpower: 'onShowPower',
				showspell: 'onShowSpell',
				showfeat: 'onShowFeat',
				showbackwin: 'onShowBackWin',
				whowhere: 'onWhoWhere',
                market: 'onMarket',
				showinfo: 'onShowInfo',
				map: 'onOpenMap',
				dm: 'onShowDm',
				reloadmap: 'onReloadMap',
				shownpcinfo: 'onShowNpcinfo',
				createchar: 'onCreateChar',
				charcreated: 'onChooserBack',
				iosmode: 'onIosMode',
				wtpmode: 'onWtpMode',
				awaymode: 'onAwayMode'
			}
		});

		this.login = Ext.create('widget.login', {
			session: this.session,
			requestManager: this.requestManager,
			listeners: {
                scope: this,
                login: 'onLogin'
            }
		});	
		
		Ext.get('loading').destroy();
    },

	onMinimizeWin: function(obj){
		var id = obj.getId();
		var index = this.getWindowIndex(id);

		this.viewport.windowManager[index].minimized = true;

		obj.hide();

		var tb = this.viewport.mainmenu;

		tb.add({
			tooltip: obj.getHeader().getEl().dom.textContent,
			id: 'minimized-' + id,
			iconCls: id,
			handler: function(){
				obj.show();
				Ext.getCmp('minimized-' + id).destroy();
				Ext.getCmp('main-viewport').fireEvent('restorewin', obj);
			}
		});
	},

	onRestoreWin: function(obj){
		var index = this.getWindowIndex(obj.getId());

		this.viewport.windowManager[index].minimized = false;
	},

	onOpenWin: function(obj){
		var windows = this.viewport.windowManager;
		var id = obj.getId();
		var index = this.getWindowIndex(id);
		var opened = this.isOpened(id);

		if (!index && !opened){
			windows.push({id: id, opened: true, minimized: false});
		} else {
			windows[index].opened = true;
			windows[index].minimized = false;
		}
	},

	onCloseWin: function(obj){
		var windows = this.viewport.windowManager;
		var opened = this.getWindowIndex(obj.getId());

		windows[opened].opened = false;
	},

	onLogout: function(){
		var central = this.viewport.central.getController();

		if (!this.delayedLogout){
			this.delayedLogout = new Ext.util.DelayedTask(this.onLogout, this);
		}

		if (central.getmess){
			this.delayedLogout.delay(1000);
		} else {
			central.getmess = true;
			central.stopChat();

			var data = {
				ssid: this.settings.get('ssid')
			};

			this.requestManager.go({
				url: 'logout.php',
				data: data,
				scope: this,
				success: this.onLogoutSuccess,
				failure: this.onLogoutFailure
			});
		}
	},

	onLogoutSuccess: function(){
		this.viewport.central.getController().getmess = false;

		this.settings.erase();
		this.user.erase();
		this.viewport.settings.erase();
		this.viewport.user.erase();

		Ext.get('mainchat-panel-innerCt').update('Добро пожаловать!<br />');

		this.viewport.central.getController().hideTopic();
		this.viewport.east.getController().clearList();

		this.viewport.central.disable();
		this.viewport.south.disable();
		this.viewport.east.disable();
		this.viewport.mainmenu.disable();

		if (this.switchloc){
			this.switchloc.close();
		}

		this.closeWindows();

		Ext.getCmp('dm-button').disable();

		this.login.show();
	},

	onLogoutFailure: function(){
		return;
	},

	closeWindows: function(){
		var windows = this.viewport.windowManager;

		windows.forEach(function(value, key){
			if (value.opened){
				Ext.getCmp(value.id).close();

				if (value.minimized){
					Ext.getCmp('minimized-' + value.id).destroy();
					windows[key].minimized = false;
				}
			}
		});
	},
    
    onLogin: function(obj, user){
		this.user = user;

		this.charchooser = Ext.create('widget.charchooser', {
			session: this.session,
			requestManager: this.requestManager,
			listeners: {
                scope: this,
                choose: 'onChoose',
				back: 'onChooserBack'
            },
			user: this.user
		});
	},

	onViewError: function(obj, text){
		Ext.Msg.show({
			title: 'Ошибка',
			msg: text,
			modal: true,
			icon: Ext.Msg.ERROR,
			buttons: Ext.Msg.OK
		});
	},

	onError: function(text, failure, scope){
		Ext.Msg.show({
			title: 'Ошибка',
			msg: text,
			modal: true,
			icon: Ext.Msg.ERROR,
			buttons: Ext.Msg.OK,
			fn: function(btn){
				if (failure){
					Ext.callback(failure, scope);
				}
			}
		});
	},

	onChoose: function(obj, user, settings){
		user.set('pass', '');

		this.user = user;
		this.viewport.user = this.user;

		this.settings = settings;
		this.viewport.settings = this.settings;

		Ext.getBody().unmask();
		this.viewport.central.enable();
		this.viewport.south.enable();
		this.viewport.east.enable();
		this.viewport.mainmenu.enable();

		this.viewport.central.getController().startChat();
		this.viewport.central.getController().getTopic();

		if (this.settings.get('dm') == 1 || this.settings.get('minidm') == 1)
			Ext.getCmp('dm-button').enable();

		if (Ext.isSafari)
			Ext.getCmp('ios-mode-button').enable();
	},

	onChooserBack: function(){
		this.charchooser.close();
		this.user.erase();
		this.login.show();
	},

	onSwitchloc: function(){
		this.switchloc = Ext.create('widget.switchloc', {
			session: this.session,
			requestManager: this.requestManager,
			viewport: this.viewport,
			listeners: {
                scope: this,
                exitloc: 'onExitloc'
            }
		});
	},

	onSwitchedloc: function(){
		this.switchloc.getController().getExits();
	},

	onExitloc: function(obj, lid){
		var central = this.viewport.central.getController();

		if (!this.delayedExitloc){
			this.delayedExitloc = new Ext.util.DelayedTask(this.onExitloc, this);
		}

		if (central.getmess){
			this.delayedExitloc.delay(1000);
		} else {
			central.getmess = true;
			central.stopChat();

			var data = {
				ssid: this.settings.get('ssid'),
				mode: 1,
				lid: lid
			};

			this.requestManager.go({
				url: 'switchloc.php',
				data: data,
				scope: central,
				success: central.onGetMessSuccess,
				failure: central.onGetMessFailure
			});
		}
	},

	getWindowIndex: function(id){
		var windows = this.viewport.windowManager;
		var index = false;

		windows.forEach(function(value, key){
			if (value.id == id){
				index = key;
			}
		});

		return index;
	},

	isOpened: function(id){
		var windows = this.viewport.windowManager;
		var index = this.getWindowIndex(id);

		if (!index){
			return false;
		} else {
			return windows[index].opened;
		}
	},

	isMinimized: function(id){
		var windows = this.viewport.windowManager;
		var index = this.getWindowIndex(id);

		if (!index){
			return false;
		} else {
			return windows[index].minimized;
		}
	},

	onFailure: function(response){
		return;
	},

	onSuccessReloadCharlist: function(response){
		Ext.getCmp('charlist-tabpanel').getController().getCharsheet();
	},

	onSuccessReloadDm: function(response){
		Ext.getCmp('dm-tabpanel').getController().getDm();
	},

	onUnsettleNpc: function(id){
		this.dm.mask('Выселяем...');

		var data = {
			ssid: this.settings.get('ssid'),
			id: id,
			mode: 'unsettle'
		};

		this.requestManager.go({
			url: 'dm.php',
			data: data,
			scope: this,
			success: this.onSuccessReloadDm,
			failure: this.onFailure
		});
	},

	onIosMode: function(){
		var data = {
			ssid: this.settings.get('ssid'),
			mode: 'ios'
		};

		this.requestManager.go({
			url: 'charaction.php',
			data: data,
			scope: this,
			success: this.onSuccessIos,
			failure: this.onFailure
		});
	},

	onWtpMode: function(){
		var data = {
			ssid: this.settings.get('ssid'),
			mode: 'wtp'
		};

		this.requestManager.go({
			url: 'charaction.php',
			data: data,
			scope: this,
			success: this.onSuccessWtp,
			failure: this.onFailure
		});

		Ext.Msg.show({
			title: 'Переключение',
			msg: 'Режим "хочу играть" переключается.',
			modal: true,
			icon: Ext.Msg.INFO,
			buttons: Ext.Msg.OK
		});
	},

	onAwayMode: function(){
		var data = {
			ssid: this.settings.get('ssid'),
			mode: 'away'
		};

		this.requestManager.go({
			url: 'charaction.php',
			data: data,
			scope: this,
			success: this.onSuccessAway,
			failure: this.onFailure
		});

		Ext.Msg.show({
			title: 'Переключение',
			msg: 'Режим "отошёл" переключается.',
			modal: true,
			icon: Ext.Msg.INFO,
			buttons: Ext.Msg.OK
		});
	},

	onSuccessIos: function(){
		Ext.getCmp('ios-mode-button').disable();

		Ext.Msg.show({
			title: 'Включено',
			msg: 'Режим планшета включен.',
			modal: true,
			icon: Ext.Msg.INFO,
			buttons: Ext.Msg.OK
		});
	},

	onSuccessWtp: function(response){
		var data = Ext.JSON.decode(response.responseText);

		if (data.wtp == 2){
			Ext.getCmp('wtp-mode-button').setText('Не хочу играть!');
		} else {
			Ext.getCmp('wtp-mode-button').setText('Хочу играть!');
			Ext.getCmp('away-mode-button').setText('Отойти');
		}
	},

	onSuccessAway: function(response){
		var data = Ext.JSON.decode(response.responseText);

		if (data.away == 1){
			Ext.getCmp('away-mode-button').setText('Вернуться');
		} else {
			Ext.getCmp('wtp-mode-button').setText('Хочу играть!');
			Ext.getCmp('away-mode-button').setText('Отойти');
		}
	},

	onUseHitDie: function(hit_die){
		Ext.Msg.show({
			title: 'Использовать кость хитов?',
			message: 'Хотите ли вы автоматически получить лечение? (Нажатие на "нет" использует кость без лечения)',
			buttons: Ext.Msg.YESNOCANCEL,
			icon: Ext.Msg.QUESTION,
			fn: function(btn, txt, opt) {
				var data = {
					ssid: this.settings.get('ssid'),
					hit_dice: opt.hd_val,
					mode: 'hit_dice'
				};

				if (btn === 'yes') {
					data.heal = 1;
				} else if (btn === 'no') {
					data.heal = 0;
				} else {
					return;
				}

				Ext.getCmp('charlist-win').mask('Используем кость...');

				this.requestManager.go({
					url: 'charaction.php',
					data: data,
					scope: this,
					success: this.onSuccessReloadCharlist,
					failure: this.onFailure
				});
			},
			scope: this,
			hd_val: hit_die
		});
	},

	onUseFeat: function(id){
		Ext.Msg.show({
			title: 'Использовать черту?',
			message: 'Хотите ли вы высветить черту в лог чата?',
			buttons: Ext.Msg.YESNO,
			icon: Ext.Msg.QUESTION,
			fn: function(btn, txt, opt) {
				var data = {
					ssid: this.settings.get('ssid'),
					fid: opt.fid,
					mode: 'usefeat'
				};

				if (btn === 'yes') {
					Ext.getCmp('charlist-win').mask('Используем черту...');

					this.requestManager.go({
						url: 'charaction.php',
						data: data,
						scope: this,
						success: this.onSuccessReloadCharlist,
						failure: this.onFailure
					});
				} else {
					return;
				}
			},
			scope: this,
			fid: id
		});
	},

	onUseSpell: function(obj, id){
		var winId = 'usespell-win';

		if (this.isMinimized(winId)) return;

		if (this.isOpened(winId)){
			this.useSpell.close();
		}

		this.useSpell = Ext.create('widget.basewin', {
			width: 250,
			height: 150,
			id: winId,
			title: 'Использовать заклинание',
			minimizable: false,
			modal: true,
			resizable: false,
			draggable: false,
			items: [{
				xtype: 'usespell',
				session: this.session,
				viewport: this.viewport,
				requestManager: this.requestManager,
				useSpell: id
			}]
		});
	},

	onMakeAttack: function(){
		var winId = 'makeattack-win';

		if (this.isMinimized(winId)) return;

		if (this.isOpened(winId)){
			this.makeAttack.close();
		}

		this.makeAttack = Ext.create('widget.basewin', {
			width: 400,
			height: 315,
			id: winId,
			title: 'Атаковать цель',
			minimizable: false,
			modal: true,
			resizable: false,
			draggable: false,
			items: [{
				xtype: 'makeattack',
				session: this.session,
				viewport: this.viewport,
				requestManager: this.requestManager
			}]
		});
	},

	onNpcAttack: function(str, nid){
		var winId = 'npcattack-win';

		if (this.isMinimized(winId)) return;

		if (this.isOpened(winId)){
			this.npcAttack.close();
		}

		this.npcAttack = Ext.create('widget.basewin', {
			width: 400,
			height: 310,
			id: winId,
			title: 'Атаковать цель',
			minimizable: false,
			modal: true,
			resizable: false,
			draggable: false,
			items: [{
				xtype: 'npcattack',
				session: this.session,
				viewport: this.viewport,
				requestManager: this.requestManager,
				string: str,
				npcId: nid
			}]
		});
	},

	onNpcDamage: function(str, nid){
		var winId = 'npcdamage-win';

		if (this.isMinimized(winId)) return;

		if (this.isOpened(winId)){
			this.npcDamage.close();
		}

		this.npcDamage = Ext.create('widget.basewin', {
			width: 400,
			height: 275,
			id: winId,
			title: 'Нанести урон цели',
			minimizable: false,
			modal: true,
			resizable: false,
			draggable: false,
			items: [{
				xtype: 'npcdamage',
				session: this.session,
				viewport: this.viewport,
				requestManager: this.requestManager,
				string: str,
				npcId: nid
			}]
		});
	},

	onMakeDamage: function(){
		var winId = 'makedamage-win';

		if (this.isMinimized(winId)) return;

		if (this.isOpened(winId)){
			this.makeDamage.close();
		}

		this.makeDamage = Ext.create('widget.basewin', {
			width: 400,
			height: 315,
			id: winId,
			title: 'Нанести урон цели',
			minimizable: false,
			modal: true,
			resizable: false,
			draggable: false,
			items: [{
				xtype: 'makedamage',
				session: this.session,
				viewport: this.viewport,
				requestManager: this.requestManager
			}]
		});
	},

	onPowerDice: function(str, pid){
		var winId = 'powerdice-win';

		if (this.isMinimized(winId)) return;

		if (this.isOpened(winId)){
			this.powerDice.close();
		}

		this.powerDice = Ext.create('widget.basewin', {
			width: 400,
			height: 170,
			id: winId,
			title: 'Использовать кубик таланта',
			minimizable: false,
			modal: true,
			resizable: false,
			draggable: false,
			items: [{
				xtype: 'powerdice',
				session: this.session,
				viewport: this.viewport,
				requestManager: this.requestManager,
				string: str,
				pid: pid
			}]
		});
	},

	onSpellAttack: function(str, sid, clid){
		var winId = 'spellattack-win';

		if (this.isMinimized(winId)) return;

		if (this.isOpened(winId)){
			this.spellAttack.close();
		}

		this.spellAttack = Ext.create('widget.basewin', {
			width: 350,
			height: 215,
			id: winId,
			title: 'Атака заклинанием',
			minimizable: false,
			modal: true,
			resizable: false,
			draggable: false,
			items: [{
				xtype: 'spellattack',
				session: this.session,
				viewport: this.viewport,
				requestManager: this.requestManager,
				spellString: str,
				spellId: sid,
				spellClid: clid
			}]
		});
	},

	onSpellDamage: function(str, sid, clid){
		var winId = 'spelldamage-win';

		if (this.isMinimized(winId)) return;

		if (this.isOpened(winId)){
			this.spellDamage.close();
		}

		this.spellDamage = Ext.create('widget.basewin', {
			width: 350,
			height: 215,
			id: winId,
			title: 'Урон заклинанием',
			minimizable: false,
			modal: true,
			resizable: false,
			draggable: false,
			items: [{
				xtype: 'spelldamage',
				session: this.session,
				viewport: this.viewport,
				requestManager: this.requestManager,
				spellString: str,
				spellId: sid,
				spellClid: clid
			}]
		});
	},

	onCharlist: function(){
		var winId = 'charlist-win';

		if (this.isMinimized(winId)) return;

		if (this.isOpened(winId)){
			this.charlist.close();
		}

		this.charlist = Ext.create('widget.basewin', {
			width: 800,
			height: 520,
			id: winId,
			title: 'Лист персонажа',
			items: [{
				xtype: 'charlist',
				session: this.session,
				viewport: this.viewport,
				requestManager: this.requestManager,
				listeners: {
					makecheck: 'onMakeCheck',
					useitem: 'onUseItem',
					usehitdie: 'onUseHitDie',
					usespell: 'onUseSpell',
					makeattack: 'onMakeAttack',
					usefeat: 'onUseFeat',
					makedamage: 'onMakeDamage',
					powerdice: 'onPowerDice',
					spellattack: 'onSpellAttack',
					spelldamage: 'onSpellDamage',
					error: 'onViewError',
					showhelp: 'onShowHelp',
					showback: 'onShowBack',
					lvlup: 'onLvlup',
					scope: this
				}
			}]
		});
	},

	onShowBack: function(back){
		Ext.Msg.show({
			title: 'Показать предысторию?',
			message: 'Хотите ли вы показать предысторию в чат? (Нажатие на "нет" покажет предысторию только вам)',
			buttons: Ext.Msg.YESNOCANCEL,
			icon: Ext.Msg.QUESTION,
			fn: function(btn, txt, opt) {
				var data = {
					ssid: this.settings.get('ssid'),
					back: opt.back
				};

				if (btn === 'yes') {
					data.mode = 'showback';
				} else if (btn === 'no') {
					data.mode = 'quickshowback';

					this.requestManager.go({
						url: 'charaction.php',
						data: data,
						scope: this,
						success: this.onSuccessQuickBack,
						failure: this.onFailure
					});

					return;
				} else {
					return;
				}

				Ext.getCmp('charlist-win').mask('Показываем...');

				this.requestManager.go({
					url: 'charaction.php',
					data: data,
					scope: this,
					success: this.onSuccessReloadCharlist,
					failure: this.onFailure
				});
			},
			scope: this,
			back: back
		});
	},

	onSuccessQuickBack: function(response){
		var data = Ext.JSON.decode(response.responseText);

		this.onShowBackWin(data.back_id);
	},

	onShowHelp: function(help){
		var winId = 'showhelp-win';

		if (this.isMinimized(winId)) return;

		if (this.isOpened(winId)){
			this.showHelp.close();
		}

		this.showHelp = Ext.create('widget.basewin', {
			width: 450,
			height: 300,
			id: winId,
			title: 'Просмотр справки',
			items: [{
				xtype: 'showhelp',
				help: help,
				session: this.session,
				viewport: this.viewport,
				requestManager: this.requestManager
			}]
		});
	},

	onShowInfo: function(cid){
		var winId = 'charinfo-win';

		if (this.isMinimized(winId)) return;

		if (this.isOpened(winId)){
			this.charInfo.close();
		}

		this.charInfo = Ext.create('widget.basewin', {
			width: 800,
			height: 520,
			id: winId,
			items: [{
				xtype: 'charinfo',
				session: this.session,
				viewport: this.viewport,
				requestManager: this.requestManager,
				charId: cid
			}]
		});
	},

	onShowPower: function(id){
		var winId = 'showpower-win';

		if (this.isMinimized(winId)) return;

		if (this.isOpened(winId)){
			this.showPower.close();
		}

		this.showPower = Ext.create('widget.basewin', {
			width: 450,
			height: 300,
			id: winId,
			title: 'Просмотр таланта',
			items: [{
				xtype: 'showpower',
				powerId: id,
				session: this.session,
				viewport: this.viewport,
				requestManager: this.requestManager
			}]
		});
	},

	onShowBackWin: function(id){
		var winId = 'showback-win';

		if (this.isMinimized(winId)) return;

		if (this.isOpened(winId)){
			this.showBack.close();
		}

		this.showBack = Ext.create('widget.basewin', {
			width: 450,
			height: 300,
			id: winId,
			title: 'Просмотр предыстории',
			items: [{
				xtype: 'showback',
				backId: id,
				session: this.session,
				viewport: this.viewport,
				requestManager: this.requestManager
			}]
		});
	},

	onShowFeat: function(id){
		var winId = 'showfeat-win';

		if (this.isMinimized(winId)) return;

		if (this.isOpened(winId)){
			this.showFeat.close();
		}

		this.showFeat = Ext.create('widget.basewin', {
			width: 450,
			height: 300,
			id: winId,
			title: 'Просмотр черты',
			items: [{
				xtype: 'showfeat',
				featId: id,
				session: this.session,
				viewport: this.viewport,
				requestManager: this.requestManager
			}]
		});
	},

	onShowSpell: function(id){
		var winId = 'showspell-win';

		if (this.isMinimized(winId)) return;

		if (this.isOpened(winId)){
			this.showSpell.close();
		}

		this.showSpell = Ext.create('widget.basewin', {
			width: 450,
			height: 300,
			id: winId,
			title: 'Просмотр заклинания',
			items: [{
				xtype: 'showspell',
				spellId: id,
				session: this.session,
				viewport: this.viewport,
				requestManager: this.requestManager
			}]
		});
	},

	onUseItem: function(obj, id){
		var winId = 'useitem-win';

		if (this.isMinimized(winId)) return;

		if (this.isOpened(winId)){
			this.useItem.close();
		}

		this.useItem = Ext.create('widget.basewin', {
			width: 250,
			height: 200,
			id: winId,
			title: 'Использовать предмет',
			minimizable: false,
			modal: true,
			resizable: false,
			draggable: false,
			items: [{
				xtype: 'useitem',
				session: this.session,
				viewport: this.viewport,
				requestManager: this.requestManager,
				useItem: id
			}]
		});
	},

	onMakeCheck: function(id){
		var winId = 'makecheck-win';

		if (this.isMinimized(winId)) return;

		if (this.isOpened(winId)){
			this.makeCheck.close();
		}

		this.makeCheck = Ext.create('widget.basewin', {
			width: 250,
			height: 200,
			id: winId,
			title: 'Бросок проверки',
			minimizable: false,
			modal: true,
			resizable: false,
			draggable: false,
			items: [{
				xtype: 'makecheck',
				session: this.session,
				viewport: this.viewport,
				requestManager: this.requestManager,
				makeCheck: id
			}]
		});
	},

	onNpcCheck: function(check, str){
		var winId = 'npccheck-win';

		if (this.isMinimized(winId)) return;

		if (this.isOpened(winId)){
			this.npcCheck.close();
		}

		this.npcCheck = Ext.create('widget.basewin', {
			width: 350,
			height: 225,
			id: winId,
			title: 'Бросок проверки',
			minimizable: false,
			modal: true,
			resizable: false,
			draggable: false,
			items: [{
				xtype: 'npccheck',
				session: this.session,
				viewport: this.viewport,
				requestManager: this.requestManager,
				makeCheck: check,
				string: str
			}]
		});
	},

	onOpenDices: function(){
		var winId = 'dices-win';

		if (this.isMinimized(winId)) return;

		if (this.isOpened(winId)){
			this.dices.close();
		}

		this.dices = Ext.create('widget.basewin', {
			width: 280,
			height: 200,
			id: winId,
			title: 'Бросок кубиков',
			items: [{
				xtype: 'dices',
				session: this.session,
				viewport: this.viewport,
				requestManager: this.requestManager
			}]
		});
	},

	onOpenEcho: function(){
		var winId = 'echo-win';

		if (this.isMinimized(winId)) return;

		if (this.isOpened(winId)){
			this.echo.close();
		}

		this.echo = Ext.create('widget.basewin', {
			width: 800,
			height: 600,
			id: winId,
			title: 'Эхо',
			items: [{
				xtype: 'echo',
				session: this.session,
				viewport: this.viewport,
				requestManager: this.requestManager
			}]
		});
	},

	onWhoWhere: function(){
		var winId = 'whowhere-win';

		if (this.isMinimized(winId)) return;

		if (this.isOpened(winId)){
			this.whoWhere.close();
		}

		this.whoWhere = Ext.create('widget.basewin', {
			width: 300,
			height: 450,
			id: winId,
			title: 'Кто где?',
			items: [{
				xtype: 'whowhere',
				session: this.session,
				viewport: this.viewport,
				requestManager: this.requestManager
			}]
		});
	},

    onMarket: function(){
        var winId = 'market-win';

        if (this.isMinimized(winId)) return;

        if (this.isOpened(winId)){
            this.market.close();
        }

        this.market = Ext.create('widget.basewin', {
            width: 800,
            height: 550,
            id: winId,
            title: 'Магазин',
            items: [{
                xtype: 'market',
                session: this.session,
                viewport: this.viewport,
                requestManager: this.requestManager
            }]
        });
    },

	onUseNpc: function(obj, npc){
		var winId = 'usenpc-win';

		if (this.isMinimized(winId)) return;

		if (this.isOpened(winId)){
			this.useNpc.close();
		}

		this.useNpc = Ext.create('widget.basewin', {
			width: 300,
			height: 180,
			id: winId,
			title: 'Заселить NPC',
			minimizable: false,
			modal: true,
			resizable: false,
			draggable: false,
			items: [{
				xtype: 'usenpc',
				session: this.session,
				viewport: this.viewport,
				requestManager: this.requestManager,
				useId: npc.id,
				useHp: npc.hp,
				useName: npc.mob
			}]
		});
	},

	onShowDm: function(){
		var winId = 'dm-win';

		if (this.isMinimized(winId)) return;

		if (this.isOpened(winId)){
			this.dm.close();
		}

		this.dm = Ext.create('widget.basewin', {
			width: 800,
			height: 550,
			id: winId,
			title: 'Мастерская',
			items: [{
				xtype: 'dm',
				session: this.session,
				viewport: this.viewport,
				requestManager: this.requestManager,
				listeners: {
					usenpc: 'onUseNpc',
					unsettle: 'onUnsettleNpc',
					npcattack: 'onNpcAttack',
					npcdamage: 'onNpcDamage',
					npccheck: 'onNpcCheck',
					scope: this
				}
			}]
		});
	},

	onOpenMap: function(){
		var winId = 'map-win';

		if (this.isMinimized(winId)) return;

		if (this.isOpened(winId)){
			this.lenzMap.close();
		}

		this.lenzMap = Ext.create('widget.basewin', {
			width: 800,
			height: 620,
			maximizable: true,
			id: winId,
			title: 'Карты',
			items: [{
				xtype: 'lenzmap',
				session: this.session,
				viewport: this.viewport,
				requestManager: this.requestManager
			}]
		});
	},

	onReloadMap: function(){
		var winId = 'map-win';

		//if (this.isMinimized(winId)) return;

		if (this.isOpened(winId)){
			Ext.getCmp('map-tabpanel').getController().refreshMap();
		}
	},

	onShowNpcinfo: function(nid){
		var winId = 'npcinfo-win';

		if (this.isMinimized(winId)) return;

		if (this.isOpened(winId)){
			this.npcInfo.close();
		}

		this.npcInfo = Ext.create('widget.basewin', {
			width: 650,
			height: 430,
			id: winId,
			items: [{
				xtype: 'npcinfo',
				session: this.session,
				viewport: this.viewport,
				requestManager: this.requestManager,
				npcId: nid,
				listeners: {
					npcattack: 'onNpcAttack',
					npcdamage: 'onNpcDamage',
					npccheck: 'onNpcCheck',
					scope: this
				}
			}]
		});
	},

	onLvlup: function(){
		var winId = 'lvlup-win';

		if (this.isMinimized(winId)) return;

		if (this.isOpened(winId)){
			this.lvlUp.close();
		}

		this.lvlUp = Ext.create('widget.basewin', {
			width: 800,
			height: 560,
			id: winId,
			resizable: false,
			draggable: false,
			title: 'Повышение уровня',
			items: [{
				xtype: 'lvlup',
				session: this.session,
				viewport: this.viewport,
				requestManager: this.requestManager
			}]
		});
	},

	onCreateChar: function(user){
		var winId = 'charcreator-win';

		if (this.isMinimized(winId)) return;

		if (this.isOpened(winId)){
			this.charCreator.close();
		}

		this.charCreator = Ext.create('widget.basewin', {
			width: 800,
			height: 560,
			id: winId,
			resizable: false,
			minimizable: false,
			draggable: false,
			title: 'Создание персонажа',
			items: [{
				xtype: 'charcreator',
				session: this.session,
				viewport: this.viewport,
				requestManager: this.requestManager,
				user: user
			}]
		});
	}
});

function get_movie(){
	movie_name = 'december';
	if (navigator.appName.indexOf("Microsoft") != -1){
		return window[movie_name];
	} else {
		return document[movie_name];
	}
}

function recive_move_from_as(msg){
	//Ext.getCmp('map-panel').setNewMap(msg, true);
}

function recive_units_from_as(msg){
	//Ext.getCmp('map-panel').setNewMap(msg, false); // сюда приходит енкоженая в джисон строка
	Ext.getCmp('map-tabpanel').getController().setMap(msg);
}

function send_to_as(str){
	//var str = '{"Scena":[{"map":"http://bugzykon2.valuehost.ru/data/maps/melni.png","id":1}],"Char":[{"template":1,"x":1120,"id":1,"y":224},{"template":3,"x":1152,"id":3,"y":224},{"template":5,"x":96,"id":5,"y":384}],"CharProto":[{"pic":"http://bugzykon2.valuehost.ru/dwd/data/faces/t/Daelar.png","size":1,"id":1,"name":"Даэлар"},{"pic":"http://bugzykon2.valuehost.ru/dwd/data/faces/t/Valero.png","size":1,"id":3,"name":"Валеро"},{"pic":"http://bugzykon2.valuehost.ru/dwd/data/faces/t/male.png","size":1,"id":5,"name":"Странный старик"}]}'; // енкоженая в джисон строка
	get_movie().send_units_to_as(str);
}

function translit(str){
	var arr={'а' : 'a', 'б' : 'b', 'в' : 'v', 'г' : 'g', 'д' : 'd', 'е' : 'e', 'ё' : 'e', 'ж' : 'zh', 'з' : 'z', 'и' : 'i', 'й' : 'y', 'к' : 'k', 'л' : 'l', 'м' : 'm', 'н' : 'n', 'о' : 'o', 'п' : 'p', 'р' : 'r', 'с' : 's', 'т' : 't', 'у' : 'u', 'ф' : 'f', 'х' : 'kh', 'ц' : 'ts', 'ч' : 'ch', 'ш' : 'sh', 'щ' : 'shch', 'ы' : 'y', 'э' : 'e', 'ю' : 'yu', 'я' : 'ya', 'А' : 'A', 'Б' : 'B', 'В' : 'V', 'Г' :  'G', 'Д' : 'D', 'Е' : 'E', 'Ё' : 'E', 'Ж' : 'Zh', 'З' : 'Z', 'И' : 'I', 'Й' : 'Y', 'К' : 'K', 'Л' : 'L', 'М' : 'M', 'Н' : 'N', 'О' : 'O', 'П' : 'P', 'Р' : 'R', 'С' : 'S', 'Т' : 'T', 'У' : 'U', 'Ф' : 'F', 'Х' : 'Kh', 'Ц' : 'Ts', 'Ч' : 'Ch', 'Ш' : 'Sh', 'Щ' : 'Shch', 'Ы' : 'Y', 'Э' : 'E', 'Ю' : 'Yu', 'Я' : 'Ya', 'ь' : '', 'Ь' : '', 'ъ' : '', 'Ъ' : '', ' ' : '_'};
	var replacer=function(a){return arr[a]};
	return str.replace(/[А-яёЁ\s]/g,replacer)
}

