
Ext.define("DwD2.view.main.South",{
    extend: "Ext.form.Panel",

	requires: [
        'DwD2.view.main.SouthController',
        'DwD2.view.main.SouthModel'
    ],

    controller: "main-south",
    viewModel: {
        type: "main-south"
    },

	alias: 'widget.southpanel',

    layout: 'column',
	frame: true,
	reference: 'sendmessage-form',

	items:[{
		columnWidth: 0.1,
		hideLabels: true,
		layout: 'fit',
		margin: '0 0 0 0',

		items: [{
			xtype: 'combo',
			editable: false,
			emptyText: 'В приват..',
			queryMode: 'local',
			//selectOnFocus: true,
			store: 'charsInLoc',
			displayField: 'name',
			name: 'private_list',
			valueField: 'id',
			triggerAction: 'all',
			reference: 'private_combo'
		/*},{
			layout: 'column',

			items: [{
				id: 'recode-button',
				xtype: 'button',
				text: 'En>Ru',
				tooltip: 'Нажмите, если ошиблись раскладкой при наборе текста и произойдёт автоматическая перекодировка! (хоткей SHIFT+ENTER)',
				//formBind: true,
				scope: this/*,
				handler: this.recodeText
			},{
				id: 'nr-button',
				xtype: 'button',
				text: 'Нрпг',
				tooltip: 'Нажмите, если хотите вставить код нрпг сообщения в окно ввода',
				//formBind: true,
				scope: this,
				handler: this.insertNRPG
			},{
				id: 'nt-button',
				xtype: 'button',
				text: 'Note',
				tooltip: 'Нажмите, если хотите вставить код сообщения памятки перед ходом в окно ввода',
				//formBind: true,
				scope: this,
				handler: this.insertNT
			},{
				id: 'sn-button',
				xtype: 'button',
				text: 'Звук',
				tooltip: 'Нажмите, если хотите вставить код звуковой памятки-уведомления после сообщения персонажа в окно ввода',
				//formBind: true,
				scope: this,
				handler: this.insertSN
			},{
				id: 'ver-button',
				xtype: 'button',
				text: '?',
				tooltip: 'Нажмите, чтобы узнать версию загруженных в ваш браузер скриптов чата',
				//formBind: true,
				scope: this,
				handler: this.showVer
			}]*/
		}]
	},{
		columnWidth: 0.7,
		layout: 'fit',
		margin: '0 0 0 2',

		items: [{
			xtype:'textarea',
			hideLabel: true,
			name: 'post_msg',
			//allowBlank: false,
			reference: 'text_to_send',
			listeners: {
				specialKey: 'onSpecialKey'
			}
		}]
	},{
		columnWidth: 0.2,
		margin: '0 0 0 2',
		border: false,
		bodyCls: 'panel-filling',
		bodyPadding: 10,

		items: [{
			reference: 'post_button',
			xtype: 'button',
			text: 'Отправить',
			tooltip: 'Хоткей CTRL+ENTER',
			//formBind: true,
			listeners: {
				click: 'onSendMessageClick'
			}
		},{
			reference: 'nrpg_button',
			xtype: 'button',
			text: 'Нрпг',
			tooltip: 'Нажмите, если хотите вставить код нрпг сообщения в окно ввода',
			//formBind: true,
			listeners: {
				click: 'onSendNRPGClick'
			}
		},{
			reference: 'recode_button',
			xtype: 'button',
			text: 'En>Ru',
			tooltip: 'Нажмите, если ошиблись раскладкой при наборе текста (хоткей SHIFT+ENTER)',
			//formBind: true,
			listeners: {
				click: 'recodeText'
			}
		}/*,{
			xtype: 'panel',
			layout: 'hbox',
			margin: '0 0 0 0',
			frame: true,

			items: [{
				id: 'recode-button',
				xtype: 'button',
				text: 'En>Ru',
				tooltip: 'Нажмите, если ошиблись раскладкой при наборе текста и произойдёт автоматическая перекодировка! (хоткей SHIFT+ENTER)',
				//formBind: true,
				scope: this
				//handler: this.recodeText
			},{
				id: 'nr-button',
				xtype: 'button',
				text: 'Нрпг',
				tooltip: 'Нажмите, если хотите вставить код нрпг сообщения в окно ввода',
				//formBind: true,
				scope: this,
				handler: this.insertNRPG
			}]
		},{
			xtype: 'panel',
			layout: 'hbox',
			margin: '0 0 0 0',
			
			items: [{
				id: 'nt-button',
				xtype: 'button',
				text: 'Note',
				tooltip: 'Нажмите, если хотите вставить код сообщения памятки перед ходом в окно ввода',
				//formBind: true,
				scope: this,
				handler: this.insertNT
			},{
				id: 'sn-button',
				xtype: 'button',
				text: 'Звук',
				tooltip: 'Нажмите, если хотите вставить код звуковой памятки-уведомления после сообщения персонажа в окно ввода',
				//formBind: true,
				scope: this,
				handler: this.insertSN
			},{
				id: 'ver-button',
				xtype: 'button',
				text: '?',
				tooltip: 'Нажмите, чтобы узнать версию загруженных в ваш браузер скриптов чата',
				//formBind: true,
				scope: this,
				handler: this.showVer
			}]
		}*/]
	}]	
});
