
Ext.define("DwD2.view.main.East",{
    extend: "Ext.panel.Panel",

	requires: [
        'DwD2.view.main.EastController',
        'DwD2.view.main.EastModel'
    ],

    controller: "main-east",
    viewModel: {
        type: "main-east"
    },

	alias: 'widget.eastpanel',

    scrollable: true,
	bodyStyle: '{font:12px verdana,tahoma,arial,sans-serif;}',
	frame: true
});
