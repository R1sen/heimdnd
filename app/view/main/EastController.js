Ext.define('DwD2.view.main.EastController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.main-east',

	clearList: function(){
		this.getView().removeAll();
	},

	populateList: function(character) {
		var view = this.getView();

		if (character.status == 2){
			var tt = character.name + ' (!)';
		} else if (character.status == 1){
			var tt = character.name + ' (N/A)';
		} else {
			var tt = character.name;
		}

		view.add({
			id: 'char-' + character.id,
			layout: 'border',
			xtype: 'panel',
			height: 71,
			width: 196,
			title: tt,
			collapsible: true,
			margins: '0 0 0 0',
			items: [{
				collapsible: false,
				bodyCls: 'eastpanel-text',
				region: 'center',
				margins: '0 0 0 0',
				html: '<center><a onClick="Ext.getCmp(\'main-viewport\').south.getController().setPrivate(' + character.id + ')"><b><u>'+ character.name +'</u></b></a><br/><small>' + character.alias + '</small></center>'
			},{
				collapsible: false,
				//border: true,
				region: 'west',
				width: 42,
				layout: 'fit',
				margins: '0 0 0 0',
				listeners: {
					click: {
						fn: 'onShowCharinfo',
						element: 'body'
					}
				},
				items: [{
					xtype: 'image',
					alt: 'char_' + character.id,
					src: 'resources/data/faces/s/' + character.s_icon
				}]
			}]
		});
	},

	populateNPCList: function(npc){
		var view = this.getView();
		var settings = Ext.getCmp('main-viewport').settings;

		if (settings.get('cid') == npc.owner) npc.alias = npc.hp + ' хп, ' + npc.temp_hp + ' вр.';
		else npc.alias = '';

		view.add({
			id: 'npc-' + npc.id,
			layout: 'border',
			xtype: 'panel',
			height: 71,
			width: 196,
			title: npc.name,
			collapsible: true,
			margins: '0 0 0 0',
			items: [{
				collapsible: false,
				bodyCls: 'eastpanel-text',
				region: 'center',
				margins: '0 0 0 0',
				html: '<center><b>'+ npc.name +'</b><br/><small>' + npc.alias + '</small></center>'
			},{
				collapsible: false,
				region: 'west',
				width: 42,
				layout: 'fit',
				margins: '0 0 0 0',
				listeners: {
					click: {
						fn: 'onShowNpcinfo',
						element: 'body'
					}
				},
				items: [{
					xtype: 'image',
					alt: 'npc_' + npc.id,
					src: 'resources/data/faces/npc/s/' + translit(npc.original) + '.jpg'
				}]
			}]
		});
	},

	onShowCharinfo: function(obj, t){
		var spl = t.alt.split('_');

		Ext.getCmp('main-viewport').fireEvent('showinfo', spl[spl.length - 1]);
	},

	onShowNpcinfo: function(obj, t){
		var spl = t.alt.split('_');

		Ext.getCmp('main-viewport').fireEvent('shownpcinfo', spl[spl.length - 1]);
	},

	reloadList: function(){
		var view = this.getView();
		var rm = view.requestManager;
		var settings = Ext.getCmp('main-viewport').settings;
		var data = {
			ssid: settings.get('ssid')
		};

		rm.go({
			url: 'who.php',
			data: data,
			scope: this,
			success: this.onReloadListSuccess,
			failure: this.onReloadListFailure
		});	
	},

	onReloadListSuccess: function(response){
		var data = Ext.JSON.decode(response.responseText);

		this.clearList();

		Ext.each(data.chars, function(character){
			this.populateList(character);
		}, this);

		//Ext.getStore('charsInLocRaw').loadRawData(data);

		var data_all = {};
		data_all.chars = [];
		
		var only_npcs = {};
		only_npcs.npcs = [];

		Ext.each(data.chars, function(character){
			data_all.chars.push(character);
		});

		if (data.npcs)
			Ext.each(data.npcs, function(npc){
				this.populateNPCList(npc);

				only_npcs.npcs.push({name: npc.name, id: npc.id});
				npc.id *= -1;
				data_all.chars.push(npc);
			}, this);

		data_all.chars.push({name: "Неопределённая цель", id: 0});
		Ext.getStore('allTargets').loadRawData(data_all);

		data.chars.push({name: "В чат", id: 0});
		Ext.getStore('charsInLoc').loadRawData(data);

		only_npcs.npcs.push({name: "Неопределённая цель", id: 0}, {name: "Как виртуальный монстр", id: -1})
		Ext.getStore('npcStore').loadRawData(only_npcs);
	},

	onReloadListFailure: function(){
		return;
	}
});
