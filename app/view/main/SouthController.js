Ext.define('DwD2.view.main.SouthController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.main-south',

	onSpecialKey: function(field, e) {
        if (e.getKey() === e.ENTER && e.ctrlKey) {
            this.sendMessage();
        } else if (e.getKey() === e.ENTER && e.shiftKey){
			e.preventDefault();
			this.recodeText();
		}
    },

	onSendMessageClick: function(button){
		this.sendMessage();
	},

	sendMessage: function(){
		var view = this.getView();
		var chat = Ext.getCmp('main-viewport').central.getController();

		chat.stopChat();

		if (!this.sendmess){
			var form = view.getForm();

			this.sendmess = true;

			this.lookupReference('post_button').disable();
		
			if (form.isValid()) {
				var data = form.getValues();
				var rm = view.requestManager;
				var settings = Ext.getCmp('main-viewport').settings;

				data.ssid = settings.get('ssid');

				rm.go({
					url: 'post.php',
					data: data,
					scope: this,
					success: this.onPostSuccess,
					failure: this.onPostFailure
				});			
			}
		}
	},

	onPostSuccess: function(){
		var chat = Ext.getCmp('main-viewport').central.getController();
		var txtf = this.lookupReference('text_to_send');

		txtf.setValue('');
		txtf.focus(false, 1000);

		this.sendmess = false;

		chat.startChat();
		this.lookupReference('post_button').enable();
	},
	
	onPostFailure: function(){
		if (this.sendmess){
			this.sendmess = false;
			this.lookupReference('post_button').enable();
		}

		return;
	},

	/*loadPrivateList: function(data){
		var list = this.lookupReference('private_combo');

		list.getStore().loadRawData(data);
	},*/

	setPrivate: function(who){
		var list = this.lookupReference('private_combo');
		var area = this.lookupReference('text_to_send');

		list.setValue(who);
		
		area.focus(false, 500);
	},

	recodeText: function(){
		var sumbol_mas_out = ["q","w","e","r","t","y","u","i","o","p","[","]","a","s","d","f","g","h","j","k","l",";","'","z","x","c","v","b","n","m",",",".","/",":",'"',"`","~","Q","W","E","R","T","Y","U","I","O","P","{","}","A","S","D","F","G","H","J","K","L","Z","X","C","V","B","N","M","<",">","?","&","\""];
		var sumbol_mas_in  = ["й","ц","у","к","е","н","г","ш","щ","з","х","ъ","ф","ы","в","а","п","р","о","л","д","ж","э","я","ч","с","м","и","т","ь","б","ю",".","Ж",'"',"ё","~","Й","Ц","У","К","Е","Н","Г","Ш","Щ","З","Х","Ъ","Ф","Ы","В","А","П","Р","О","Л","Д","Я","Ч","С","М","И","Т","Ь","Б","Ю",",","?","Э"];
		var new_str_var = "";
		var tt = this.lookupReference('text_to_send');

		new_str_var = this.coding_mash(tt.getValue(), sumbol_mas_out, sumbol_mas_in, 0);
		tt.setValue(new_str_var);
	},

	onSendNRPGClick: function(){
		var area = this.lookupReference('text_to_send');

		area.setValue('/nr ' + area.getValue());
		area.focus();
	},

	coding_mash: function(changed_str, sumbol_mas_out, sumbol_mas_in, obr) {
		var new_str_val = "";
		var uncoded_nick = "";
		var nick_found_ok = 0;
		var str_length = changed_str.length;

		for (var j = 0; j <= str_length-1; j++) {
			var letter_s = changed_str.substring(j,j+1);

			if (nick_found_ok == 0) {
				uncoded_nick = uncoded_nick + letter_s;
			}

			if (letter_s == '>') {
				nick_found_ok = 1;
			}
		}

		if (nick_found_ok == 1) {
			changed_str = changed_str.substring(uncoded_nick.length, str_length);
			str_length = changed_str.length;
		} else {
			uncoded_nick = "";
		}


		for (var j = 0; j <= str_length-1; j++) {
			change_st = 0;

			for(var i = 0; i <= sumbol_mas_out.length-1; i++){
				if (change_st == 0){
					tmp = changed_str.substring(j,j+sumbol_mas_out[i].length);
				}

				if (tmp == sumbol_mas_out[i]){
					change_st = 1; tmp = sumbol_mas_in[i];

					if(sumbol_mas_out[i].length>1){
						j=j+sumbol_mas_out[i].length-1;
					}
				} else {
					if (change_st == 0){
						tmp = changed_str.substring(j,j+sumbol_mas_in[i].length);
					}

					if ((tmp == sumbol_mas_in[i]) && (obr == 1)){
						change_st = 1; tmp = sumbol_mas_out[i];

						if(sumbol_mas_in[i].length>1){
							j=j+sumbol_mas_in[i].length-1;
						}
					}
				}
			}

			if (change_st == 0){
				tmp = changed_str.substring(j,j+1);
			}

			new_str_val = new_str_val+tmp;
		}

		new_str_val = uncoded_nick + new_str_val;

		return new_str_val;
	}
});
