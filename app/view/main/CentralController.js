Ext.define('DwD2.view.main.CentralController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.main-central',

	startChat: function(){
		if (!this.chattask){
			this.chattask = {
				run: this.getMess,
				interval: 5000, //5 second
				scope: this
			};
		}

		Ext.TaskManager.start(this.chattask);
	},

	stopChat: function(){
		Ext.TaskManager.stop(this.chattask);
	},

	delayScroll: function(){
		if (!this.delayedScroll){
			this.delayedScroll = new Ext.util.DelayedTask(this.scroll, this);
		}

		this.delayedScroll.delay(1500);
	},

	getMess: function(){
		if (this.getmess){
			return;
		} else {
			this.getmess = true;
			var view = this.getView();
			var rm = view.requestManager;
			var settings = Ext.getCmp('main-viewport').settings;
			var data = {
				ssid: settings.get('ssid'),
				lastmid: settings.get('lastmid')
			};

			rm.go({
				url: 'getmess.php',
				data: data,
				scope: this,
				success: this.onGetMessSuccess,
				failure: this.onGetMessFailure,
				inmess: true
			});	
		}
	},

	getTopic: function(){
		var view = this.getView();
		var rm = view.requestManager;
		var settings = Ext.getCmp('main-viewport').settings;
		var data = {
			ssid: settings.get('ssid')
		};

		rm.go({
			url: 'gettopic.php',
			data: data,
			scope: this,
			success: this.onGetTopicSuccess,
			failure: this.onGetTopicFailure
		});	
	},

	onGetTopicSuccess: function(response){
		var data = Ext.JSON.decode(response.responseText);

		Ext.get('topic').update('<center><b>' + data.loc_name + '</b></center><br/>' + data.topic);
		this.lookupReference('topic-panel').expand();
		this.delayScroll();
	},

	hideTopic: function(){
		this.lookupReference('topic-panel').collapse();
	},

	scroll: function(){
		this.lookupReference('chat-panel').getScrollable().scrollBy(null, 1000, {duration: 2000});
	},

	onGetMessSuccess: function(response){
		var data = Ext.JSON.decode(response.responseText);
		var beacon = this.lookupReference('chat-panel');
		var vp = Ext.getCmp('main-viewport');
		var settings = vp.settings;

		this.getmess = false;

		beacon.setData(data);

		settings.set('lastmid', data.lastmid);

		if (data.mess){
			this.scroll();
		}

		if (data.reload_east){
			vp.east.getController().reloadList();
		}
		
		if (data.reload_map){
			vp.fireEvent('reloadmap');
		}

		if (data.return_run){
			vp.fireEvent('switchedloc');

			this.getTopic();

			this.startChat();
		}
	},

	onGetMessFailure: function(){
		if (this.getmess){
			this.getmess = false;
		}
	},

	onGetTopicFailure: function(){
		return;
	}
});
