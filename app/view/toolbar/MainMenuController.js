Ext.define('DwD2.view.toolbar.MainMenuController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.toolbar-mainmenu',

	onLogoutClick: function(){
		Ext.getCmp('main-viewport').fireEvent('logout');
	},

	onOpenCharlist: function(){
		Ext.getCmp('main-viewport').fireEvent('opencharlist');
	},

	onOpenDices: function(){
		Ext.getCmp('main-viewport').fireEvent('opendices');
	},

	onOpenEcho: function(){
		Ext.getCmp('main-viewport').fireEvent('openecho');
	},

	onWhoWhere: function(){
		Ext.getCmp('main-viewport').fireEvent('whowhere');
	},

    onMarket: function(){
        Ext.getCmp('main-viewport').fireEvent('market');
    },

	onDm: function(){
		Ext.getCmp('main-viewport').fireEvent('dm');
	},

	onMap: function(){
		Ext.getCmp('main-viewport').fireEvent('map');
	}
});
