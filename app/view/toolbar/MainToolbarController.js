Ext.define('DwD2.view.toolbar.MainToolbarController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.toolbar-maintoolbar',

	onSwitchlocClick: function(){
		Ext.getCmp('main-viewport').fireEvent('switchloc');
	},

	onIosClick: function(){
		Ext.getCmp('main-viewport').fireEvent('iosmode');
	},

	onWtpClick: function(){
		Ext.getCmp('main-viewport').fireEvent('wtpmode');
	},

	onAwayClick: function(){
		Ext.getCmp('main-viewport').fireEvent('awaymode');
	}
});
