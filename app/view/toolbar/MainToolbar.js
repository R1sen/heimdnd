
Ext.define("DwD2.view.toolbar.MainToolbar",{
    extend: "Ext.toolbar.Toolbar",

	requires: [
		'DwD2.view.toolbar.MainToolbarController',
        'DwD2.view.toolbar.MainToolbarModel',
        'DwD2.view.toolbar.MainMenu'
	],

    controller: "toolbar-maintoolbar",
    viewModel: {
        type: "toolbar-maintoolbar"
    },

	alias: 'widget.maintoolbar',
	disabled: true,

    items: [{
		text: "Главное меню",
		menu: {
			xtype: 'mainmenu'
		}
	},{
		text: 'Переход в...',
		tooltip: 'Переход в соседнюю локацию',
		iconCls: 'switchloc',
		listeners: {
			click: 'onSwitchlocClick'
		}
	}, '-', {
		text: 'Форум',
		iconCls: 'forumbutton',
		id: 'forum-button',
		tooltip: 'Открыть форум игры',
		handler: function(){
			window.open("/lenz", "Lenz Forum");
		}
	},{
		text: 'Отойти',
		tooltip: 'Сигнализировать о вашей недоступности',
		iconCls: 'awaybutton',
		id: 'away-mode-button',
		listeners: {
			click: 'onAwayClick'
		}
	},{
		text: 'Хочу играть!',
		tooltip: 'Сигнализировать о желании поиграть в чате',
		iconCls: 'wtpbutton',
		id: 'wtp-mode-button',
		listeners: {
			click: 'onWtpClick'
		}
	},{
		text: 'Планшет',
		tooltip: 'Включить режим планшета',
		disabled: true,
		id: 'ios-mode-button',
		listeners: {
			click: 'onIosClick'
		}
	}, '-']
});
