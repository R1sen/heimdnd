
Ext.define("DwD2.view.toolbar.MainMenu",{
    extend: "Ext.menu.Menu",

	requires: [
        'DwD2.view.toolbar.MainMenuController',
        'DwD2.view.toolbar.MainMenuModel'
    ],

    controller: "toolbar-mainmenu",
    viewModel: {
        type: "toolbar-mainmenu"
    },

	alias: 'widget.mainmenu',

    items: [{
		text: 'Окно чата',
		menu: {
			items: [{
				text: 'Очистить чат',
				iconCls: 'erase',
				handler: function() { Ext.get('mainchat-panel-innerCt').update(''); }
			}]
		}
	}, '-', {
		text: 'Лист персонажа',
		iconCls: 'charlist-win',
		listeners: {
			click: 'onOpenCharlist'
		}
	},{
		text: 'Кубики',
		iconCls: 'dices-win',
		listeners: {
			click: 'onOpenDices'
		}
	},{
		text: 'Эхо',
		iconCls: 'echo-win',
		listeners: {
			click: 'onOpenEcho'
		}
	},{
		text: 'Кто где?',
		iconCls: 'whowhere-win',
		listeners: {
			click: 'onWhoWhere'
		}
	},{
        text: 'Магазин',
		iconCls: 'market-win',
        listeners: {
            click: 'onMarket'
        }
    },{
		text: 'Карты',
		iconCls: 'map-win',
		listeners: {
			click: 'onMap'
		}
	},{
		text: 'Мастерская',
		iconCls: 'dm-win',
		disabled: true,
		id: 'dm-button',
		listeners: {
			click: 'onDm'
		}
	},{
		text: 'Выйти',
		iconCls: 'logout',
		listeners: {
			click: 'onLogoutClick'
		}
	}]
});
