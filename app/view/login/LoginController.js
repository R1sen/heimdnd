Ext.define('DwD2.view.login.LoginController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.login-login',

	loginText: 'Входим...',

    onSpecialKey: function(field, e) {
        if (e.getKey() === e.ENTER) {
            this.doLogin();
        } else if (e.getKey() === e.TAB){
			e.preventDefault();
			if (field.getReference() == 'login_textfield'){
				this.lookupReference('password_textfield').focus();
			} else {
				this.lookupReference('login_textfield').focus();
			}
        }
    },
    
    onLoginClick: function() {
        this.doLogin();
    },
    
	doLogin: function() {
        var form = this.lookupReference('login-form');
        
        if (form.isValid()) {
			var data = form.getValues();
			var view = this.getView();
			var rm = view.requestManager;

			Ext.getBody().mask(this.loginText);
			view.hide();

			rm.go({
				url: 'login.php',
				data: data,
				scope: this,
				success: this.onLoginSuccess,
				failure: this.onLoginFailure
			});			
        }
    },

    onLoginSuccess: function(response) {
		var userModel = Ext.create('DwD2.model.User');

		this.lookupReference('password_textfield').setValue('');
		
		var userSet = userModel.getProxy().getReader().read(response, {
                recordCreator: this.getSession().recordCreator
        });

        this.fireViewEvent('login', userSet.getRecords()[0]);
		
		userSet.destroy();
    },

	onLoginFailure: function() {
		this.getView().show();
	}
});
