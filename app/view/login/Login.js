
Ext.define("DwD2.view.login.Login",{
    extend: "Ext.window.Window",

	requires: [
        'DwD2.view.login.LoginController',
        'DwD2.view.login.LoginModel',
		'DwD2.model.User'
    ],

    controller: "login-login",
    viewModel: {
        type: "login-login"
    },

	bodyPadding: 10,
    title: Ext.get('page-title').dom.innerHTML,
    closable: false,
	autoShow: true,
	alias: 'widget.login',
	resizable: false,
	draggable: false,

    items: [{
        xtype: 'form',
        reference: 'login-form',
		frame: true,

        items: [{
            xtype: 'textfield',
            name: 'username',
			reference: 'login_textfield',
            fieldLabel: 'Логин',
            allowBlank: false,
            enableKeyEvents: true,
            listeners: {
                specialKey: 'onSpecialKey'
            }
        },{
            xtype: 'textfield',
            name: 'password',
            inputType: 'password',
			reference: 'password_textfield',
            fieldLabel: 'Пароль',
            allowBlank: false,
            enableKeyEvents: true,
            cls: 'password',
            listeners: {
                specialKey: 'onSpecialKey'
            }
        }]
    }],

    buttons: [{
        text: 'Войти',
        listeners: {
            click: 'onLoginClick'
        }
    }]
});
