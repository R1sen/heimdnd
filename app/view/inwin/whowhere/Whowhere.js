
Ext.define('DwD2.view.inwin.whowhere.Whowhere',{
    extend: 'Ext.panel.Panel',

    requires: [
        'DwD2.view.inwin.whowhere.WhowhereController',
        'DwD2.view.inwin.whowhere.WhowhereModel'
    ],

    controller: 'inwin-whowhere-whowhere',
    viewModel: {
        type: 'inwin-whowhere-whowhere'
    },

    bodyCls: 'itemdescr-panel',
    xtype: 'whowhere',
    scrollable: true,
    frame: true,

    onRender: function(){
        this.callParent();

        this.getController().getList();
    }
});
