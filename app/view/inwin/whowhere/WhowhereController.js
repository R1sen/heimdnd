Ext.define('DwD2.view.inwin.whowhere.WhowhereController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.inwin-whowhere-whowhere',

    getWhoText: 'Загружаем...',

    getList: function(id){
        var view = this.getView();
        var rm = view.requestManager;
        var settings = view.viewport.settings;
        var data = {
            ssid: settings.get('ssid')
        };

        view.up('window').mask(this.getWhoText);

        rm.go({
            url: 'whowhere.php',
            data: data,
            scope: this,
            success: this.onGetSuccess,
            failure: this.onFailure
        });
    },

    onGetSuccess: function(response){
        var ww = Ext.JSON.decode(response.responseText);
        var view = this.getView();
        var window = view.up('window');

        window.unmask();

        if (ww.whowhere == 0){
            view.update('Никого нет в чате');
        } else {
            var i = 0;

            while (ww.whowhere[i]){
                var j = 0;
                var list = '';

                while (ww.whowhere[i].chars[j]){
                    list += ww.whowhere[i].chars[j] + '<br/>';

                    j++;
                }

                view.add({
                    xtype: 'fieldset',
                    width: 250,
                    title: ww.whowhere[i].lname,
                    items: [{
                        border: false,
                        bodyCls: 'itemdescr-panel',
                        html: list
                    }]
                });

                i++;
            }
        }
    },

    onFailure: function(){
        var window = this.getView().up('window');

        window.unmask();
    }
    
});
