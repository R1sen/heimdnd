
Ext.define('DwD2.view.inwin.makedamage.Makedamage',{
    extend: 'Ext.form.Panel',

    requires: [
        'DwD2.view.inwin.makedamage.MakedamageController',
        'DwD2.view.inwin.makedamage.MakedamageModel'
    ],

    controller: 'inwin-makedamage-makedamage',
    viewModel: {
        type: 'inwin-makedamage-makedamage'
    },

    frame: true,
    xtype: 'makedamage',
    layout: 'anchor',
    defaults: {
        anchor: '100%',
        labelWidth: 140
    },
    bodyPadding: 10,

    items: [{
        xtype: 'combobox',
        fieldLabel: 'Рука',
        //labelWidth: 45,
        editable: false,
        emptyText: 'Выберите руку..',
        queryMode: 'local',
        store: 'handSelect',
        displayField: 'name',
        name: 'hand',
        valueField: 'id',
        triggerAction: 'all',
        reference: 'hand_combo'
    },{
        xtype: 'textfield',
        name: 'mod',
        reference: 'mod_textfield',
        fieldLabel: 'Модификатор',
        regex: /^[\+\-]?\d{1,2}$/,
        regexText: 'Допускается только целое число с плюсом или минусом, не большее 99.',
        allowBlank: false,
        enableKeyEvents: true,
        value: '0',
        listeners: {
            specialKey: 'onSpecialKey'
        }
    },{
        xtype: 'combobox',
        fieldLabel: 'Цель',
        //labelWidth: 45,
        editable: false,
        emptyText: 'Выберите цель..',
        queryMode: 'local',
        store: 'allTargets',
        displayField: 'name',
        name: 'target',
        valueField: 'id',
        triggerAction: 'all',
        reference: 'target_combo'
    },{
        xtype: 'checkboxfield',
        reference: 'crit_box',
        fieldLabel: 'Крит',
        inputValue: '1',
        name: 'crit'
    },{
        xtype: 'checkboxfield',
        reference: 'pair_box',
        fieldLabel: 'Парная (второй урон)',
        inputValue: '1',
        name: 'pair_damage'
    },{
        xtype: 'checkboxfield',
        reference: 'improvised_box',
        fieldLabel: 'Импровизированная',
        inputValue: '1',
        name: 'improv',
        listeners: {
            change: 'onImprovChange',
            scope: 'controller'
        }
    },{
        xtype: 'combobox',
        hideLabel: true,
        //labelWidth: 45,
        disabled: true,
        editable: false,
        emptyText: 'Выберите тип..',
        queryMode: 'local',
        store: 'improvisedStore',
        displayField: 'name',
        name: 'improv_type',
        valueField: 'id',
        triggerAction: 'all',
        reference: 'improvised_combo'
    }],

    buttons: [{
        text: 'Урон',
        formBind: true,
        disabled: true,
        handler: 'onMakeDamage'
    }]
});
