Ext.define('DwD2.view.inwin.charlist.CharlistController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.inwin-charlist-charlist',

    fixedInfo: false,
    takeOffText: 'Снимаем...',
    usePowerText: 'Высвечиваем в чат...',
    getCharsheetText: 'Загрузка...',
    saveInfoText: 'Сохраняем...',
    useRestText: 'Отдыхаем...',

    selectedPower: 0,
    selectedSpell: 0,

    lockInfo: function(e, t, o){
        e.preventDefault();

        this.fixedInfo = !this.fixedInfo;
    },

    getItem: function(id){
        var target = false;

        Ext.each(this.char.get('items'), function(item){
            if (item.id == id){
                target = item;
                return false;
            }
        });

        return target;
    },

    getMore: function(e, t, o){
        if (this.fixedInfo) return;

        if (Ext.get(t.id).component.items){
            var what = Ext.get(t.id).component.items.items[0].alt;
        } else {
            var what = t.alt;
        }

        var spl = what.split('-');

        var txt = '';
        var node = false;
        var base = '';
        var enchant_info = '';
        var enchant_rarity = {common: 'Обычное', uncommon: 'Необычное', rare: 'Редкое', 'very rare': 'Очень редкое', legendary: 'Легендарное'};

        if (spl[2] == 0){
            return;
        }

        if (spl[1] == 'item'){
            node = this.getItem(spl[2]);
            base = node.base;
        } else {
            base = spl[1];
            node = this.char.get(base + '_object');
        }

        /*switch (base){
            case 'armor':
                if (!node) node = this.char.get('armor_object');
                break;

            case 'mainhand':

            default: return;
        }*/

        if(node.enchant_id != 0)
            enchant_info = '<br><br><i>Зачарование</i><br><b>' + node.enchant_object.name + '</b>(' +  enchant_rarity[node.enchant_object.rarity] + ')<br><b>Описание:</b>' +  node.enchant_object.descr;


        this.lookupReference('item_more_pic').setSrc('resources/data/items/' + node.icon);
        this.lookupReference('item_more_desc').update('<b>' + node.name + '</b><br/><br/>'  + node.descr +  enchant_info);
    },

    addItem: function(item){
        var items_table = this.lookupReference('inventory-items');

        items_table.add({
            //panel is for underlying element events
            items: [{
                xtype: 'image',
                width: 50,
                height: 50,
                alt: 'char-item-' + item.id,
                src: 'resources/data/items/' + item.icon
            }]
        });
    },

    clearInventory: function(){
        this.lookupReference('inventory-items').removeAll();
    },

    takeOff: function(e, t, o){
        var what = t.alt;
        var spl = what.split('-');

        if (spl[2] == 0) return;

        var view = this.getView();
        var rm = view.requestManager;
        var settings = view.viewport.settings;

        var data = {
            ssid: settings.get('ssid')
        };

        data.slot = spl[1];
        data.mode = 'takeoff';

        view.up('window').mask(this.takeOffText);

        rm.go({
            url: 'charaction.php',
            data: data,
            scope: this,
            success: this.onSuccessAndReload,
            failure: this.onFailure
        });
    },

    onFailure: function(){
        var window = this.getView().up('window');

        window.unmask();
    },

    onSuccessAndReload: function(response){
        var window = this.getView().up('window');

        window.unmask();

        this.getCharsheet();
    },

    onSuccess: function(response){
        var window = this.getView().up('window');

        window.unmask();
    },

    usePower: function(){
        if (this.selectedPower == 0) return;

        var view = this.getView();
        var rm = view.requestManager;
        var settings = view.viewport.settings;
        var data = {
            ssid: settings.get('ssid')
        };

        data.mode = 'usepower';
        data.power_id = this.selectedPower;

        view.up('window').mask(this.usePowerText);

        rm.go({
            url: 'charaction.php',
            data: data,
            scope: this,
            success: this.onSuccess,
            failure: this.onFailure
        });
    },

    takeOn: function(e, t, o){
        var what = t.alt;
        var spl = what.split('-');
        
        this.fireViewEvent('useitem', spl[2]);
    },

    getCharsheet: function(){
        var view = this.getView();
        var rm = view.requestManager;
        var settings = view.viewport.settings;
        var data = {
            ssid: settings.get('ssid')
        };

        view.up('window').mask(this.getCharsheetText);

        rm.go({
            url: 'getchar.php',
            data: data,
            scope: this,
            success: this.onGetChar,
            failure: this.onFailure
        });
    },

    onGetChar: function(response){
        if (this.char) this.char.erase();

        this.clearInventory();

        var charModel = Ext.create('DwD2.model.Charsheet');

        var charSet = charModel.getProxy().getReader().read(response, {
            recordCreator: this.getSession().recordCreator
        });

        var chara = charSet.getRecords()[0];
        this.char = chara;

        charSet.destroy();

        this.getViewModel().linkTo('char', this.char);

        Ext.each(this.char.get('items'), function(item){
            this.addItem(item);
        }, this);

        Ext.getStore('spellsTree').setRoot(this.char.get('spells_tree'));
        Ext.getStore('powersTree').setRoot(this.char.get('powers_tree'));

        if ((this.char.get('mainhand') == this.char.get('offhand')) && this.char.get('mainhand') != 0){
            this.lookupReference('offhand-puppetimage').disable();
        } else {
            this.lookupReference('offhand-puppetimage').enable();
        }

        //var moneys = '<b>Монеты:</b> |<span style="color: steelBlue  ">' + this.char.get('platinum') + ' платины</span>|<span style="color: DarkGoldenRod ">' + this.char.get('gold') + ' золота</span>|<span style="color: silver ">' + this.char.get('silver') + ' серебра</span>|<span style="color: chocolate  ">' + this.char.get('copper') + ' медных</span>|';

        //this.lookupReference('item_money').update(moneys);

        this.loadSlotsStore();

        this.loadFeats();

        this.onSuccess();
    },

    loadFeats: function(){
        var panel = this.lookupReference('char-featspanel');
        var feats = this.char.get('feats');

        panel.removeAll();

        if (feats.length == 0){
            panel.update('У вас нету черт для отображения.');
            this.lookupReference('char-featspanel').disable();
        } else {
            Ext.each(feats, function(feat){
                panel.add({
                    xtype: 'fieldset',
                    collapsible: true,
                    collapsed: true,
                    title: '<a onclick="Ext.getCmp(\'charlist-tabpanel\').fireEvent(\'usefeat\', ' + feat.id + ');">' + feat.title + '</a>',
                    toggleOnTitleClick: false,
                    items: [{
                        bodyCls: 'itemdescr-panel',
                        border: false,
                        html: feat.descr
                    }]
                });
            }, this);
        }
    },

    loadSlotsStore: function(lvlcap){
        var actual_slots = [];
        var warslots = [this.char.get('warslot_1'), this.char.get('warslot_2'), this.char.get('warslot_3'), this.char.get('warslot_4'), this.char.get('warslot_5')];

        if (!lvlcap) lvlcap = 0;

        actual_slots.push({
            name: 'Заговор/Талант',
            id: 0
        });

        Ext.each(this.char.get('actual_slots'), function(vals, index){
            var warslot = '';

            if (warslots[index] != -1){
                warslot = ' Колдун: ' + warslots[index];
            }

            if ((vals != 0 || warslots[index] > 0) && lvlcap <= index + 1){
                actual_slots.push({
                    name: 'Уровень ' + (index + 1) + ': ' + vals + ' доступно.' + warslot,
                    id: index + 1
                });
            }
        }, this);

        Ext.getStore('actualSlots').loadRawData(actual_slots);
    },

    calcSlots: function(slot){
        var act = this.char.get('actual_slots');
        var warslots = [this.char.get('warslot_1'), this.char.get('warslot_2'), this.char.get('warslot_3'), this.char.get('warslot_4'), this.char.get('warslot_5')];

        if (warslots[slot - 1] > 0){
            warslots[slot - 1]--;

            this.char.set('warslot_' + slot, warslots[slot - 1]);
        } else {
            act[slot - 1]--;

            this.char.set('actual_slots', act);
            this.char.set('slot_' + slot, act[slot - 1]);
        }

        this.loadSlotsStore();
    },

    onSpellSelect: function(obj, rec, index, opt){
        var val = rec.getData();

        if (!val.leaf){
            this.selectedSpell = 0;
            this.lookupReference('usespell_button').disable();
            this.lookupReference('useritual_button').disable();
            return;
        }

        if (val.checked == null || val.checked){
            this.selectedSpell = val.sid;
            this.lookupReference('usespell_button').enable();
        } else {
            this.selectedSpell = 0;
            this.lookupReference('usespell_button').disable();
        }

        if (val.ritual == 1){
            this.lookupReference('useritual_button').enable();
            var wizard = this.getPowerIndex('Использование заклинаний (волшебник)');

            if (wizard) this.selectedSpell = val.sid;
        } else {
            this.lookupReference('useritual_button').disable();
        }

        var spell = this.char.get('spells')[this.getSpellIndex(val.text)];

        var attack_regex = /(спасброс(?:ок|ке|ки))\s(Силы|Телосложения|Ловкости|Интеллекта|Мудрости|Харизмы)|(рукопашную)\s(атаку\sзаклинанием)|(дальнобойную)\s(атаку\sзаклинанием)/gi;
        var damage_regex = /(колющий|дробящий|рубящий\s)?(урон\s)?(?:(огнем|огнём|звуком|излучением|кислотой|некротической\sэнергией|психической\sэнергией|силовым\sполем|холодом|электричеством|ядом|излучением\sи\sогнём)\s)?((?:\d{1,2})?[dкд]{1}\d{1,2})/gi;
        
        var descr = spell.descr.replace(attack_regex, "<a onclick=\"Ext.getCmp('charlist-tabpanel').fireEvent('spellattack', '$&', " + spell.id + ", " + val.clid + ")\">$&</a>");
        descr = descr.replace(damage_regex, "<a onclick=\"Ext.getCmp('charlist-tabpanel').fireEvent('spelldamage', '$&', " + spell.id + ", " + val.clid + ")\">$&</a>");

        var txt = '<b>' + spell.name + '</b><br/><i>' + spell.lvl + ' уровень, ' + spell.cat + '</i><br/><br/><b>Время накладывания:</b> ' + spell.timing + '<br/><b>Дистанция:</b> ' + spell.distance + '<br/><span data-qtip="В - вербальный, С - соматический, М - материальный"><b>Компоненты:</b> ' + spell.components + '</span><br/><b>Длительность:</b> ' + spell.duration + '<br/><br/>' + descr;

        this.lookupReference('spelldescr-panel').update(txt);
    },

    getSpellIndex: function(txt){
        var ret = false;

        Ext.each(this.char.get('spells'), function(spell, index){
            if (spell.name == txt){
                ret = index;
                return false;
            }
        });

        return ret;
    },

    getSpell: function(id){
        var ret = false;

        Ext.each(this.char.get('spells'), function(spell, index){
            if (spell.id == id){
                ret = spell;
                return false;
            }
        });

        return ret;
    },

    onUseSlot: function(){
        this.loadSlotsStore();

        this.fireViewEvent('usespell', -1);
    },

    onUseSpell: function(){
        if (this.selectedSpell == 0) return;

        var caplvl = this.getSpell(this.selectedSpell).lvl;

        this.loadSlotsStore(caplvl);

        this.fireViewEvent('usespell', this.selectedSpell);
    },

    onBeforeCheckChange: function(record, checkedState, e){
        var clid = record.get('clid');
        var tc = record.get('to_count');
        var checked = this.lookupReference('spells_treepanel').getChecked();
        var slots = this.char.get('spells_slots');
        var actual_prep = 0;

        if (tc == 0) return;

        Ext.each(checked, function(rec, index){
            if (rec.get('to_count') == 1 && !checkedState && rec.get('clid') == clid){
                actual_prep++;
            }
        });

        if (slots[clid]["type"] == 'prepare' && actual_prep == slots[clid][slots[clid]["type"]] && !checkedState){
            Ext.toast('Достигнут лимит по запоминанию заклинаний! (max: ' + slots[clid][slots[clid]["type"]] + ')', null, 't');
            return false;
        }
    },

    onPrepareSpells: function(){
        var checked = this.lookupReference('spells_treepanel').getChecked();
        var out = [];

        Ext.each(checked, function(rec){
            var obj = {};

            obj.sid = rec.get('sid');
            obj.clid = rec.get('clid');

            out.push(obj);
        });

        var view = this.getView();
        var rm = view.requestManager;
        var settings = view.viewport.settings;
        var data = {
            ssid: settings.get('ssid'),
            prepare: Ext.JSON.encode(out),
            mode: 'prepare'
        };

        view.up('window').mask(this.getCharsheetText);

        rm.go({
            url: 'charaction.php',
            data: data,
            scope: this,
            success: this.onSuccess,
            failure: this.onFailure
        });
    },

    onUseRitual: function(){
        var wizard = this.getPowerIndex('Использование заклинаний (волшебник)');

        if (!wizard && this.selectedSpell <= 0) return;

        var view = this.getView();
        var rm = view.requestManager;
        var settings = view.viewport.settings;
        var data = {
            ssid: settings.get('ssid'),
            sid: this.selectedSpell,
            mode: 'useritual'
        };

        view.up('window').mask(this.getCharsheetText);

        rm.go({
            url: 'charaction.php',
            data: data,
            scope: this,
            success: this.onSuccess,
            failure: this.onFailure
        });
    },

    onPowerSelect: function(obj, rec, index, opt){
        var val = rec.getData();

        if (val.parentId == 'root'){
            this.selectedPower = 0;
            this.lookupReference('usepower_button').disable();
            return;
        }

        var power = this.char.get('powers')[this.getPowerIndex(val.text)];

        this.selectedPower = power.id;
        this.lookupReference('usepower_button').enable();

        var dice_regex = /\d{1,2}?[dкд]{1}\d{1,2}/gi;
        var descr = power.descr.replace(dice_regex, "<a onclick=\"Ext.getCmp('charlist-tabpanel').fireEvent('powerdice', '$&', " + power.id + ")\">$&</a>");

        var txt = '<b>' + power.title + '</b><br/><br/>' + descr;

        this.lookupReference('powerdescr-panel').update(txt);
    },

    getPowerIndex: function(txt){
        var ret = false;

        Ext.each(this.char.get('powers'), function(power, index){
            if (power.title == txt){
                ret = index;
                return false;
            }
        });

        return ret;
    },

    onColorSelect: function(obj, color){
        this.lookupReference('info_char_color').setValue('#' + color);
    },

    onError: function(fp, o){
        var window = this.getView().up('window');

        window.unmask();

        Ext.Msg.show({
            title: 'Ошибка',
            msg: o.result.error,
            modal: true,
            icon: Ext.Msg.ERROR,
            buttons: Ext.Msg.OK
        });
    },

    onInfoSave: function(){
        var form = this.lookupReference('char-infopanel');
        var view = this.getView();
        var settings = view.viewport.settings;

        if (form.isValid()) {
            view.up('window').mask(this.saveInfoText);

            form.submit({
                url: 'resources/server/charaction.php',
                params: {
                    ssid: settings.get('ssid'),
                    mode: 'saveinfo'
                },
                scope: this,
                success: function(fp, o) {
                    this.getCharsheet();
                    Ext.getCmp('main-viewport').east.getController().reloadList();
                },
                failure: this.onError
            });
        }
    },

    loadHandStore: function(){
        var data = [];
        var main = this.char.get('mainhand_object');
        var off = this.char.get('offhand_object');

        if (this.char.get('mainhand') == 0){
            data.push({
                id: 0,
                name: 'Основная: Безоружный'
            });
        } else if (main.base == 'weapon'){
            data.push({
                id: 0,
                name: 'Основная: ' + main.name
            });
        }

        if (this.char.get('offhand') == 0){
            data.push({
                id: 1,
                name: 'Вторая: Безоружный'
            });
        } else if (off.base == 'weapon'){
            data.push({
                id: 1,
                name: 'Вторая: ' + off.name
            });
        }

        Ext.getStore('handSelect').loadRawData(data);
    },

    onMakeAttack: function(){
        this.loadHandStore();

        this.fireViewEvent('makeattack');
    },

    onMakeDamage: function(){
        this.loadHandStore();

        this.fireViewEvent('makedamage');
    },

    onLongRest: function(){
        var view = this.getView();
        var rm = view.requestManager;
        var settings = view.viewport.settings;
        var data = {
            ssid: settings.get('ssid'),
            mode: 'longrest'
        };

        view.up('window').mask(this.useRestText);

        rm.go({
            url: 'charaction.php',
            data: data,
            scope: this,
            success: this.onSuccessAndReload,
            failure: this.onFailure
        });
    },

    onShortRest: function(){
        var view = this.getView();
        var rm = view.requestManager;
        var settings = view.viewport.settings;
        var data = {
            ssid: settings.get('ssid'),
            mode: 'shortrest'
        };

        view.up('window').mask(this.useRestText);

        rm.go({
            url: 'charaction.php',
            data: data,
            scope: this,
            success: this.onSuccessAndReload,
            failure: this.onFailure
        });
    },

    onAction: function(obj){
        var view = this.getView();
        var rm = view.requestManager;
        var settings = view.viewport.settings;
        var data = {
            ssid: settings.get('ssid'),
            mode: obj.getReference()
        };

        view.up('window').mask(this.useRestText);

        rm.go({
            url: 'charaction.php',
            data: data,
            scope: this,
            success: this.onSuccess,
            failure: this.onFailure
        });
    },

    onLvlup: function(){
        this.fireViewEvent('lvlup');
    }

});
