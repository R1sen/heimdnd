
Ext.define('DwD2.view.inwin.charlist.Charlist',{
    extend: 'Ext.tab.Panel',

    requires: [
        'DwD2.view.inwin.charlist.CharlistController',
        'DwD2.view.inwin.charlist.CharlistModel',
        'DwD2.view.inwin.puppetimage.Puppetimage'
    ],

    controller: 'inwin-charlist-charlist',
    viewModel: {
        type: 'inwin-charlist-charlist'
    },
    
    id: 'charlist-tabpanel',
    xtype: 'charlist',

    items: [{
        scrollable: true,
        title: 'Информация',
        tooltip: 'Общая информация о персонаже',
        frame: true,
        layout: {
            type: 'table',
            columns: 3,
            tableAttrs: {
                cls: 'main-tables'
            }
        },
        defaults: {
            layout: 'fit',
            cellCls: 'top-align'
        },
        items: [{
            xtype: 'fieldset',
            title: 'Данные',
            rowspan: 2,
            height: 229,
            bind: {
                html: '<table cellspacing="0" cellpadding="0" class="chartable">' +
                '<tr>' +
                '<td><b>Имя:</b></td><td>{char.name}</td>' +
                '</tr>' +
                '<tr>' +
                '<td><b>Класс:</b></td><td>{char.class}</td>' +
                '</tr>' +
                '<tr>' +
                '<td><b>Раса:</b></td><td>{char.race_name}</td>' +
                '</tr>' +
                '<tr>' +
                '<td><b>Размер:</b></td><td>{char.size}</td>' +
                '</tr>' +
                '<tr>' +
                '<td><b>Предыстория:</b></td><td><a onclick="Ext.getCmp(\'charlist-tabpanel\').fireEvent(\'showback\', \'{char.back_name}\');">{char.back_name}</a></td>' +
                '</tr>' +
                '<tr>' +
                '<td><b>Мировоззрение:</b></td><td>{char.align}</td>' +
                '</tr>' +
                '<tr>' +
                '<td><b>Уровень:</b></td><td>{char.lvl}</td>' +
                '</tr>' +
                '<tr>' +
                '<td><b>Опыт:</b></td><td>{char.exp}</td>' +
                '</tr>' +
                '<tr>' +
                '<td><b>След. уровень:</b></td><td>{char.next_lvl}</td>' +
                '</tr>' +
                '<tr>' +
                '<td><b>Вдохновение:</b></td><td>{char.inspiration}</td>' +
                '</tr>' +
                '</table>'
            }
        },{
            xtype: 'fieldset',
            title: 'Бонус мастерства',
            bind: {
                html: '<table cellspacing="0" cellpadding="0" class="chartable">' +
                '<tr>' +
                '<td><b>Бонус мастерства:</b></td><td>{char.prof_bonus}</td>' +
                '</tr>' +
                '</table>'
            }
        },{
            xtype: 'fieldset',
            title: 'Аватар',
            width: 142,
            height: 229,
            rowspan: 2,
            items: [{
                xtype: 'image',
                alt: 'Картинка персонажа',
                bind: {
                    src: 'resources/data/faces/b/{char.b_icon}'
                }
            }]
        },{
            xtype: 'fieldset',
            title: 'Здоровье и броня',
            height: 176,
            bind: {
                html: '<table cellspacing="0" cellpadding="0" class="chartable">' +
                '<tr>' +
                '<td><b>КД:</b></td><td>{char.ac}</td>' +
                '</tr>' +
                '<tr>' +
                '<td><b>ХП:</b></td><td>{char.hp}/{char.total_hp}</td>' +
                '</tr>' +
                '<tr>' +
                '<td><b>Временные ХП:</b></td><td>{char.temp_hp}</td>' +
                '</tr>' +
                '<tr>' +
                '<td><b>Кости хитов доступно:</b></td><td>{char.hit_dice}</td>' +
                '</tr>' +
                '<tr>' +
                '<td><b>Кости хитов всего:</b></td><td>{char.hit_dice_total}</td>' +
                '</tr>' +
                '<tr>' +
                '<td><b>Спасы от смерти:</b></td><td>{char.death_saves_true} успех/{char.death_saves_false} провал</td>' +
                '</tr>' +
                '</table>'
            }
        },{
            xtype: 'fieldset',
            title: 'Скорость и инициатива',
            rowspan: 2,
            bind: {
                html: '<table cellspacing="0" cellpadding="0" class="chartable">' +
                '<tr>' +
                '<td><a onclick="Ext.getCmp(\'charlist-tabpanel\').fireEvent(\'makecheck\', \'initiative\');"><b>Инициатива:</b></a></td><td><a onclick="Ext.getCmp(\'charlist-tabpanel\').fireEvent(\'makecheck\', \'initiative\');">{char.initiative}</a></td>' +
                '</tr>' +
                '<tr>' +
                '<td><b>Скорость:</b></td><td>{char.speed} фут ({char.speed_cell} кл.)</td>' +
                '</tr>' +
                '<tr>' +
                '<td><b>Полёт:</b></td><td>{char.fly} фут ({char.fly_cell} кл.)</td>' +
                '</tr>' +
                '<tr>' +
                '<td><b>Плавание:</b></td><td>{char.swim} фут ({char.swim_cell} кл.)</td>' +
                '</tr>' +
                '<tr>' +
                '<td><b>Прыжок в длину:</b></td><td>{char.long_jump} фут ({char.long_jump_cell} кл.)</td>' +
                '</tr>' +
                '<tr>' +
                '<td><b>Прыжок в высоту:</b></td><td>{char.high_jump} фут ({char.high_jump_cell} кл.)</td>' +
                '</tr>' +
                '<tr>' +
                '<td><b>Ползание:</b></td><td>{char.crawl} фут ({char.crawl_cell} кл.)</td>' +
                '</tr>' +
                '<tr>' +
                '<td><b>Вскарабкивание:</b></td><td>{char.climb} фут ({char.climb_cell} кл.)</td>' +
                '</tr>' +
                '</table>'
            }
        },{
            xtype: 'fieldset',
            title: 'Пассивные навыки',
            colspan: 2,
            bind: {
                html: '<table cellspacing="0" cellpadding="0" class="chartable">' +
                '<tr>' +
                '<td><b>Пассивная внимательность:</b></td><td>{char.passive_perception}</td>' +
                '</tr>' +
                '<tr>' +
                '<td><b>Пассивный анализ:</b></td><td>{char.passive_investigation}</td>' +
                '</tr>' +
                '</table>'
            }
        },{
            xtype: 'fieldset',
            title: 'Зрение и чувства',
            colspan: 2,
            height: 110,
            bind: {
                html: '<table cellspacing="0" cellpadding="0" class="chartable">' +
                '<tr>' +
                '<td><b>Зрение и чувства:</b></td><td>{char.vis_spec_sen}</td>' +
                '</tr>' +
                '</table>'
            }
        }]
    },{
        scrollable: true,
        title: 'Характеристики',
        tooltip: 'Базовые характеристики и навыки',
        frame: true,
        layout: 'fit',
        items: [{
            xtype: 'fieldset',
            title: 'Характеристики, спасброски и навыки',
            autoScroll: true,
            bind: {
                html: '<table cellspacing="0" cellpadding="0" class="chartable">' +
                '<tr>' +
                '<td width="60%">' +
                '<table cellspacing="0" cellpadding="0" class="chartable-attr">' +
                '<tr>' +
                '<td colspan="2" class="centr"><b>Сила</b> <a onclick="Ext.getCmp(\'charlist-tabpanel\').fireEvent(\'showhelp\', \'Сила\');">(?)</a></td>' +
                '</tr>' +
                '<tr>' +
                '<td class="stat-cell-size"><a onclick="Ext.getCmp(\'charlist-tabpanel\').fireEvent(\'makecheck\', \'str\');"><img class="stat-size" src="resources/server/get_attr_img.php?attr=str&attr_val={char.str}" alt="Сила"/></a></td><td style="padding-left: 5px; width=100%;">{char.str_save_prof} <a onclick="Ext.getCmp(\'charlist-tabpanel\').fireEvent(\'makecheck\', \'str_save\');">Спасброски: {char.str_save}</a><br/>{char.athletics_prof} <a onclick="Ext.getCmp(\'charlist-tabpanel\').fireEvent(\'makecheck\', \'athletics\');">Атлетика: {char.athletics}</a><br/>Переносимый груз: {char.weight} фунт ({char.weight_kg} кг)<br/>Максимальный груз: {char.weight_max} фунт ({char.weight_max_kg} кг)<br/>Толкать, тянуть, поднимать: {char.push_drag_lift} фунт ({char.push_drag_lift_kg} кг)</td>' +
                '</tr>' +
                '</table>' +
                '</td><td>' +
                '<table cellspacing="0" cellpadding="0" class="chartable-attr">' +
                '<tr>' +
                '<td colspan="2" class="centr"><b>Ловкость</b> <a onclick="Ext.getCmp(\'charlist-tabpanel\').fireEvent(\'showhelp\', \'Ловкость\');">(?)</a></td>' +
                '</tr>' +
                '<tr>' +
                '<td class="stat-cell-size"><a onclick="Ext.getCmp(\'charlist-tabpanel\').fireEvent(\'makecheck\', \'dex\');"><img class="stat-size" src="resources/server/get_attr_img.php?attr=dex&attr_val={char.dex}" alt="Ловкость"/></a></td><td style="padding-left: 5px; width=100%;">{char.dex_save_prof} <a onclick="Ext.getCmp(\'charlist-tabpanel\').fireEvent(\'makecheck\', \'dex_save\');">Спасброски: {char.dex_save}</a><br/>{char.acrobatics_prof} <a onclick="Ext.getCmp(\'charlist-tabpanel\').fireEvent(\'makecheck\', \'acrobatics\');">Акробатика: {char.acrobatics}</a><br/>{char.sleight_prof} <a onclick="Ext.getCmp(\'charlist-tabpanel\').fireEvent(\'makecheck\', \'sleight\');">Ловкость рук: {char.sleight}</a><br/>{char.stealth_prof} <a onclick="Ext.getCmp(\'charlist-tabpanel\').fireEvent(\'makecheck\', \'stealth\');">Скрытность: {char.stealth}</a></td>' +
                '</tr>' +
                '</table>' +
                '</td>' +
                '</tr>' +
                '<tr>' +
                '<td>' +
                '<table cellspacing="0" cellpadding="0" class="chartable-attr">' +
                '<tr>' +
                '<td colspan="2" class="centr"><b>Телосложение</b> <a onclick="Ext.getCmp(\'charlist-tabpanel\').fireEvent(\'showhelp\', \'Телосложение\');">(?)</a></td>' +
                '</tr>' +
                '<tr>' +
                '<td class="stat-cell-size"><a onclick="Ext.getCmp(\'charlist-tabpanel\').fireEvent(\'makecheck\', \'con\');"><img class="stat-size" src="resources/server/get_attr_img.php?attr=cons&attr_val={char.con}" alt="Телосложение"/></a></td><td style="padding-left: 5px; width=100%;">{char.con_save_prof} <a onclick="Ext.getCmp(\'charlist-tabpanel\').fireEvent(\'makecheck\', \'con_save\');">Спасброски: {char.con_save}</a><br/>Задержка дыхания: {char.breath}<br/>Нехватка воздуха: {char.suffocating} раунда</td>' +
                '</tr>' +
                '</table>' +
                '</td>' +
                '<td>' +
                '<table cellspacing="0" cellpadding="0" class="chartable-attr">' +
                '<tr>' +
                '<td colspan="2" class="centr"><b>Интеллект</b> <a onclick="Ext.getCmp(\'charlist-tabpanel\').fireEvent(\'showhelp\', \'Интеллект\');">(?)</a></td>' +
                '</tr>' +
                '<tr>' +
                '<td class="stat-cell-size"><a onclick="Ext.getCmp(\'charlist-tabpanel\').fireEvent(\'makecheck\', \'intl\');"><img class="stat-size" src="resources/server/get_attr_img.php?attr=intl&attr_val={char.intl}" alt="Интеллект"/></a></td><td style="padding-left: 5px; width=100%;">{char.intl_save_prof} <a onclick="Ext.getCmp(\'charlist-tabpanel\').fireEvent(\'makecheck\', \'intl_save\');">Спасброски: {char.intl_save}</a><br/>{char.investigation_prof} <a onclick="Ext.getCmp(\'charlist-tabpanel\').fireEvent(\'makecheck\', \'investigation\');">Анализ: {char.investigation}</a><br/>{char.history_prof} <a onclick="Ext.getCmp(\'charlist-tabpanel\').fireEvent(\'makecheck\', \'history\');">История: {char.history}</a><br/>{char.arcana_prof} <a onclick="Ext.getCmp(\'charlist-tabpanel\').fireEvent(\'makecheck\', \'arcana\');">Магия: {char.arcana}</a><br/>{char.nature_prof} <a onclick="Ext.getCmp(\'charlist-tabpanel\').fireEvent(\'makecheck\', \'nature\');">Природа: {char.nature}</a><br/>{char.religion_prof} <a onclick="Ext.getCmp(\'charlist-tabpanel\').fireEvent(\'makecheck\', \'religion\');">Религия: {char.religion}</a></td>' +
                '</tr>' +
                '</table>' +
                '</td>' +
                '</tr>' +
                '<tr>' +
                '<td>' +
                '<table cellspacing="0" cellpadding="0" class="chartable-attr">' +
                '<tr>' +
                '<td colspan="2" class="centr"><b>Мудрость</b> <a onclick="Ext.getCmp(\'charlist-tabpanel\').fireEvent(\'showhelp\', \'Мудрость\');">(?)</a></td>' +
                '</tr>' +
                '<tr>' +
                '<td class="stat-cell-size"><a onclick="Ext.getCmp(\'charlist-tabpanel\').fireEvent(\'makecheck\', \'wis\');"><img class="stat-size" src="resources/server/get_attr_img.php?attr=wis&attr_val={char.wis}" alt="Мудрость"/></a></td><td style="padding-left: 5px; width=100%;">{char.wis_save_prof} <a onclick="Ext.getCmp(\'charlist-tabpanel\').fireEvent(\'makecheck\', \'wis_save\');">Спасброски: {char.wis_save}</a><br/>{char.perception_prof} <a onclick="Ext.getCmp(\'charlist-tabpanel\').fireEvent(\'makecheck\', \'perception\');">Внимательность: {char.perception}</a><br/>{char.survival_prof} <a onclick="Ext.getCmp(\'charlist-tabpanel\').fireEvent(\'makecheck\', \'survival\');">Выживание: {char.survival}</a><br/>{char.medicine_prof} <a onclick="Ext.getCmp(\'charlist-tabpanel\').fireEvent(\'makecheck\', \'medicine\');">Медицина: {char.medicine}</a><br/>{char.insight_prof} <a onclick="Ext.getCmp(\'charlist-tabpanel\').fireEvent(\'makecheck\', \'insight\');">Проницательность: {char.insight}</a><br/>{char.animal_prof} <a onclick="Ext.getCmp(\'charlist-tabpanel\').fireEvent(\'makecheck\', \'animal\');">Уход за животными: {char.animal}</a></td>' +
                '</tr>' +
                '</table>' +
                '</td><td>' +
                '<table cellspacing="0" cellpadding="0" class="chartable-attr">' +
                '<tr>' +
                '<td colspan="2" class="centr"><b>Харизма</b> <a onclick="Ext.getCmp(\'charlist-tabpanel\').fireEvent(\'showhelp\', \'Харизма\');">(?)</a></td>' +
                '</tr>' +
                '<tr>' +
                '<td class="stat-cell-size"><a onclick="Ext.getCmp(\'charlist-tabpanel\').fireEvent(\'makecheck\', \'cha\');"><img class="stat-size" src="resources/server/get_attr_img.php?attr=cha&attr_val={char.cha}" alt="Харизма"/></a></td><td style="padding-left: 5px; width=100%;">{char.cha_save_prof} <a onclick="Ext.getCmp(\'charlist-tabpanel\').fireEvent(\'makecheck\', \'cha_save\');">Спасброски: {char.cha_save}</a><br/>{char.performance_prof} <a onclick="Ext.getCmp(\'charlist-tabpanel\').fireEvent(\'makecheck\', \'performance\');">Выступление: {char.performance}</a><br/>{char.intimidation_prof} <a onclick="Ext.getCmp(\'charlist-tabpanel\').fireEvent(\'makecheck\', \'intimidation\');">Запугивание: {char.intimidation}</a><br/>{char.deception_prof} <a onclick="Ext.getCmp(\'charlist-tabpanel\').fireEvent(\'makecheck\', \'deception\');">Обман: {char.deception}</a><br/>{char.persuasion_prof} <a onclick="Ext.getCmp(\'charlist-tabpanel\').fireEvent(\'makecheck\', \'persuasion\');">Убеждение: {char.persuasion}</a></td>' +
                '</tr>' +
                '</table>' +
                '</td>' +
                '</tr>' +
                '</table>'
            }
        }]
    },{
        title: 'Инвентарь',
        tooltip: 'Предметы на персонаже',
        layout: 'border',
        
        items: [{
            region: 'center',
            frame: true,
            layout: {
                type: 'table',
                columns: 5
            },

            defaults: {
                /*bodyStyle: {
                    backgroundColor: '#dfe9f6',
                    cursor: 'pointer'
                },*/
                bodyCls: 'item-filling',
                border: false,
                listeners: {
                    contextmenu: {
                        fn: 'lockInfo',
                        element: 'body'
                    },
                    mouseenter: {
                        fn: 'getMore',
                        element: 'body'
                    },
                    click: {
                        fn: 'takeOff',
                        element: 'body'
                    },
                    scope: 'controller'
                }
            },
            
            items: [{
                //panel is for underlying element events
                items: [{
                    xtype: 'puppetimage',
                    alt: 'Глаза',
                    bind: {
                        src: 'resources/data/items/{char.eyes_object.icon}',
                        alt: 'char-eyes-{char.eyes}'
                    }
                }]
            },{
                //panel is for underlying element events
                items: [{
                    xtype: 'puppetimage',
                    alt: 'Голова',
                    bind: {
                        src: 'resources/data/items/{char.head_object.icon}',
                        alt: 'char-head-{char.head}'
                    }
                }]
            },{
                //panel is for underlying element events
                items: [{
                    xtype: 'puppetimage',
                    alt: 'Шея',
                    bind: {
                        src: 'resources/data/items/{char.neck_object.icon}',
                        alt: 'char-neck-{char.neck}'
                    }
                }]
            },{
                //panel is for underlying element events
                items: [{
                    xtype: 'puppetimage',
                    alt: 'Кольцо 1',
                    bind: {
                        src: 'resources/data/items/{char.ring_1_object.icon}',
                        alt: 'char-ring_1-{char.ring_1}'
                    }
                }]
            },{
                //panel is for underlying element events
                items: [{
                    xtype: 'puppetimage',
                    alt: 'Кольцо 2',
                    bind: {
                        src: 'resources/data/items/{char.ring_2_object.icon}',
                        alt: 'char-ring_2-{char.ring_2}'
                    }
                }]
            },{
                //panel is for underlying element events
                items: [{
                    xtype: 'puppetimage',
                    alt: 'Плечи',
                    bind: {
                        src: 'resources/data/items/{char.coat_object.icon}',
                        alt: 'char-coat-{char.coat}'
                    }
                }]
            },{
                colspan: 2,
                rowspan: 3,
                xtype: 'image',
                alt: 'Аватар',
                height: 150,
                width: 100,
                bind: {
                    src: 'resources/data/faces/p/{char.picture}'
                }
            },{
                //panel is for underlying element events
                items: [{
                    xtype: 'puppetimage',
                    alt: 'Предплечья',
                    bind: {
                        src: 'resources/data/items/{char.arms_object.icon}',
                        alt: 'char-arms-{char.arms}'
                    }
                }]
            },{
                //panel is for underlying element events
                items: [{
                    xtype: 'puppetimage',
                    alt: 'Ладони',
                    bind: {
                        src: 'resources/data/items/{char.hands_object.icon}',
                        alt: 'char-hands-{char.hands}'
                    }
                }]
            },{
                //panel is for underlying element events 
                items: [{
                    xtype: 'puppetimage',
                    alt: 'Броня',
                    bind: {
                        src: 'resources/data/items/{char.armor_object.icon}',
                        alt: 'char-armor-{char.armor}'
                    }
                }]
            },{
                //panel is for underlying element events
                items: [{
                    xtype: 'puppetimage',
                    alt: 'Основная рука',
                    bind: {
                        src: 'resources/data/items/{char.mainhand_object.icon}',
                        alt: 'char-mainhand-{char.mainhand}'
                    }
                }]
            },{
                //panel is for underlying element events
                items: [{
                    xtype: 'puppetimage',
                    alt: 'Предмет 1',
                    bind: {
                        src: 'resources/data/items/{char.wondrous_1_object.icon}',
                        alt: 'char-wondrous_1-{char.wondrous_1}'
                    }
                }]
            },{
                //panel is for underlying element events
                items: [{
                    xtype: 'puppetimage',
                    alt: 'Торс',
                    bind: {
                        src: 'resources/data/items/{char.torso_object.icon}',
                        alt: 'char-torso-{char.torso}'
                    }
                }]
            },{
                //panel is for underlying element events
                reference: 'offhand-puppetimage',
                items: [{
                    xtype: 'puppetimage',
                    alt: 'Вторая рука',
                    bind: {
                        src: 'resources/data/items/{char.offhand_object.icon}',
                        alt: 'char-offhand-{char.offhand}'
                    }
                }]
            },{
                //panel is for underlying element events
                items: [{
                    xtype: 'puppetimage',
                    alt: 'Предмет 2',
                    bind: {
                        src: 'resources/data/items/{char.wondrous_2_object.icon}',
                        alt: 'char-wondrous_2-{char.wondrous_2}'
                    }
                }]
            },{
                //panel is for underlying element events
                items: [{
                    xtype: 'puppetimage',
                    alt: 'Пояс',
                    bind: {
                        src: 'resources/data/items/{char.waist_object.icon}',
                        alt: 'char-waist-{char.waist}'
                    }
                }]
            },{
                //panel is for underlying element events
                items: [{
                    xtype: 'puppetimage',
                    alt: 'Ноги',
                    bind: {
                        src: 'resources/data/items/{char.feet_object.icon}',
                        alt: 'char-feet-{char.feet}'
                    }
                }]
            },{
                //panel is for underlying element events
                items: [{
                    xtype: 'puppetimage',
                    alt: 'Предмет 3',
                    bind: {
                        src: 'resources/data/items/{char.wondrous_3_object.icon}',
                        alt: 'char-wondrous_3-{char.wondrous_3}'
                    }
                }]
            },{
                //panel is for underlying element events
                items: [{
                    xtype: 'puppetimage',
                    alt: 'Предмет 4',
                    bind: {
                        src: 'resources/data/items/{char.wondrous_4_object.icon}',
                        alt: 'char-wondrous_4-{char.wondrous_4}'
                    }
                }]
            },{
                //panel is for underlying element events
                items: [{
                    xtype: 'puppetimage',
                    alt: 'Предмет 5',
                    bind: {
                        src: 'resources/data/items/{char.wondrous_5_object.icon}',
                        alt: 'char-wondrous_5-{char.wondrous_5}'
                    }
                }]
            }]
        },{
            region: 'south',
            frame: true,
            height: 170,
            title: 'Инвентарь',
            reference: 'inventory-panel',
            scrollable: true,
            items: [{
                /*bodyStyle: {
                    backgroundColor: '#dfe9f6'
                },*/
                bodyCls: 'panel-filling',
                border: false,
                reference: 'inventory-items',
                layout: {
                    type: 'table',
                    columns: 13,
                    tableAttrs: {//border-top: 1px solid black; border-left: 1px solid black;
                        style: 'table-layout: fixed'
                    }
                },
                defaults: {
                    bodyCls: 'item-filling-inv',
                    listeners: {
                        contextmenu: {
                            fn: 'lockInfo',
                            element: 'body'
                        },
                        mouseenter: {
                            fn: 'getMore',
                            element: 'body'
                        },
                        singletap: {
                            fn: 'getMore',
                            element: 'body'
                        },
                        click: {
                            fn: 'takeOn',
                            element: 'body'
                        },
                        scope: 'controller'
                    }
                }
            }]
        },{
            region: 'east',
            width: 520,
            title: 'Подробное описание предметов и действия с листом персонажа',
            layout: 'border',
            defaults: {
                /*bodyStyle: {
                    backgroundColor: '#dfe9f6'
                },*/
                bodyCls: 'panel-filling',
                border: false
            },
            items: [{
                region: 'west',
                width: 180,
                items: [{
                    reference: 'item_more_pic',
                    xtype: 'image',
                    width: 180,
                    height: 180,
                    alt: 'Увеличенное изображение',
                    src: 'resources/data/items/empty.png'
                }]
            },{
                bodyCls: 'itemdescr-panel',
                region: 'center',
                reference: 'item_more_desc',
                scrollable: true
            },{
                region: 'north',
                tbar: [{
                    tooltip: 'Платина',
                    iconCls: 'platinum',
                    bind: {
                        text: '{char.platinum}'
                    }
                },{
                    tooltip: 'Золото',
                    iconCls: 'gold',
                    bind: {
                        text: '{char.gold}'
                    }
                },{
                    tooltip: 'Серебро',
                    iconCls: 'silver',
                    bind: {
                        text: '{char.silver}'
                    }
                },{
                    tooltip: 'Медь',
                    iconCls: 'copper',
                    bind: {
                        text: '{char.copper}'
                    }
                },{
                    text: 'Обновить',
                    tooltip: 'Обновить окно персонажа принудительно',
                    handler: 'getCharsheet'
                },{
                    text: 'Атака',
                    tooltip: 'Атаковать оружием',
                    handler: 'onMakeAttack'
                },{
                    text: 'Урон',
                    tooltip: 'Нанести урон оружием',
                    handler: 'onMakeDamage'
                },{
                    text: 'Отдых',
                    tooltip: 'Провести отдых',
                    menu: [{
                        text: 'Короткий отдых',
                        handler: 'onShortRest'
                    },{
                        text: 'Длинный отдых',
                        handler: 'onLongRest'
                    }]
                },{
                    text: 'Действия',
                    tooltip: 'Стандартные действия',
                    menu: [{
                        text: 'Рывок',
                        handler: 'onAction',
                        reference: 'st_dash'
                    },{
                        text: 'Отход',
                        handler: 'onAction',
                        reference: 'st_disengage'
                    },{
                        text: 'Уклонение',
                        handler: 'onAction',
                        reference: 'st_dodge'
                    },{
                        text: 'Помощь',
                        handler: 'onAction',
                        reference: 'st_help'
                    },{
                        text: 'Засада',
                        handler: 'onAction',
                        reference: 'st_hide'
                    },{
                        text: 'Подготовка',
                        handler: 'onAction',
                        reference: 'st_ready'
                    },{
                        text: 'Поиск',
                        handler: 'onAction',
                        reference: 'st_search'
                    },{
                        text: 'Использование предмета',
                        handler: 'onAction',
                        reference: 'st_useobject'
                    }]
                },{
                    text: 'Уровень',
                    tooltip: 'Повысить уровень',
                    handler: 'onLvlup',
                    bind: {
                        disabled: '{!char.lvlup_ready}'
                    }
                }]
            }]
        }/*,{
            bodyCls: 'itemdescr-panel',
            region: 'north',
            reference: 'item_money',
            height: 23
        }*/]
    },{
        title: 'Таланты',
        tooltip: 'Информация о талантах',
        reference: 'char-powerspanel',

        layout: 'border',
        items: [{
            region: 'west',
            width: 350,
            xtype: 'treepanel',
            store: 'powersTree',
            rootVisible: false,
            useArrows: true,
            frame: true,
            title: 'Информация о талантах',
            bufferedRenderer: false,
            animate: true,
            listeners: {
                select: 'onPowerSelect',
                scope: 'controller'
            }
        },{
            scrollable: true,
            region: 'center',
            reference: 'powerdescr-panel',
            bodyCls: 'itemdescr-panel',
            title: 'Описание таланта',
            tbar: [{
                text: 'Высветить',
                tooltip: 'Высветить в чат наличие таланта',
                disabled: true,
                reference: 'usepower_button',
                handler: 'usePower'
            }]
        }]
    },{
        title: 'Заклинания',
        tooltip: 'Информация о заклинаниях',
        reference: 'char-spellspanel',
        bind: {
            disabled: '{!char.spellcaster}'
        },
        layout: 'border',
        items: [{
            region: 'west',
            width: 350,
            xtype: 'treepanel',
            store: 'spellsTree',
            rootVisible: false,
            useArrows: true,
            frame: true,
            title: 'Доступные заклинания',
            bufferedRenderer: false,
            animate: true,
            reference: 'spells_treepanel',
            listeners: {
                select: 'onSpellSelect',
                beforecheckchange: 'onBeforeCheckChange',
                scope: 'controller'
            }
        },{
            region: 'center',
            scrollable: true,
            reference: 'spelldescr-panel',
            bodyCls: 'itemdescr-panel',
            title: 'Описание заклинания',
            tbar: [{
                text: 'Подготовить',
                tooltip: 'Подготовить выбранные заклинания',
                handler: 'onPrepareSpells'
            },{
                text: 'Использовать',
                tooltip: 'Использовать заклинание',
                reference: 'usespell_button',
                disabled: true,
                handler: 'onUseSpell'
            },{
                text: 'Потратить слот',
                tooltip: 'Использовать слот заклинаний для иных действий',
                reference: 'useslot_button',
                handler: 'onUseSlot'
            },{
                text: 'Ритуал',
                tooltip: 'Использовать заклинание как ритуал',
                reference: 'useritual_button',
                disabled: true,
                handler: 'onUseRitual'
            }]
        },{
            region: 'north',
            xtype: 'fieldset',
            title: 'Информация о заклинателе',
            bind: {
                html: '<table cellspacing="0" cellpadding="0" class="chartable">' +
                '<tr>' +
                '<td class="centr"><b>Класс</b></td><td class="centr"><b>Атрибуты</b></td><td class="centr"><b>Спасброски</b></td><td class="centr"><b>Уровень</b></td><td class="centr"><b>Подготов/Знание</b></td>' +
                '</tr>' +
                '<tr>' +
                '<td class="centr">{char.spellcaster_class}</td><td class="centr">{char.spellcaster_attrs}</td><td class="centr">{char.spellcaster_saves}</td><td class="centr">{char.spellcaster_lvl}</td><td class="centr">{char.spellcaster_prepares}</td>' +
                '</tr>' +
                '<tr>' +
                '<td class="centr"><b>Ячеек доступно:</b></td><td colspan="4">' +
                '<table cellspacing="0" cellpadding="0" class="chartable"><tr>' +
                '<td><img src="resources/server/get_cell_img.php?cell=1&val={char.slot_1}&warslot={char.warslot_1}"/></td><td><img src="resources/server/get_cell_img.php?cell=2&val={char.slot_2}&warslot={char.warslot_2}"/></td><td><img src="resources/server/get_cell_img.php?cell=3&val={char.slot_3}&warslot={char.warslot_3}"/></td><td><img src="resources/server/get_cell_img.php?cell=4&val={char.slot_4}&warslot={char.warslot_4}"/></td><td><img src="resources/server/get_cell_img.php?cell=5&val={char.slot_5}&warslot={char.warslot_5}"/></td><td><img src="resources/server/get_cell_img.php?cell=6&val={char.slot_6}"/></td><td><img src="resources/server/get_cell_img.php?cell=7&val={char.slot_7}"/></td><td><img src="resources/server/get_cell_img.php?cell=8&val={char.slot_8}"/></td><td><img src="resources/server/get_cell_img.php?cell=9&val={char.slot_9}"/></td>' +
                '</tr>' +
                '</table>' +
                '</td>' +
                '</tr>' +
                '</table>'
            }
        }]
    },{
        title: 'Черты',
        tooltip: 'Информация о чертах',
        reference: 'char-featspanel',
        scrollable: true,
        frame: true
    },{
        scrollable: true,
        title: 'Инфо',
        tooltip: 'Настройки и описание персонажа',
        frame: true,
        reference: 'char-infopanel',
        xtype: 'form',
        listeners: {
            afterrender: function(){

                var cp = new Ext.ColorPalette({
                    cls: 'custom-color-palette',
                    listeners: {
                        select: Ext.getCmp('charlist-tabpanel').getController().onColorSelect,
                        scope: Ext.getCmp('charlist-tabpanel').getController()
                    }
                });

                cp.colors = Ext.getCmp('main-viewport').safeColors;
                cp.render('info-colorpalette-div');
            }
        },
        items: [{
            fieldLabel: 'Цвет сообщений',
            xtype: 'field',
            submitValue: false,
            fieldSubTpl: '<div id="info-colorpalette-div"></div>'
        },{
            fieldLabel: 'HEX-цвет',
            name: 'char_color',
            xtype: 'textfield',
            allowEmpty: false,
            formBind: true,
            regex: /^#[A-Fa-f0-9]{6}$/,
            regexText: 'Неверный формат HEX-цвета',
            reference: 'info_char_color',
            bind: {
                value: '{char.color}'
            }
        },{
            fieldLabel: 'Малый аватар (40x40)',
            name: 's_icon',
            xtype: 'fileuploadfield',
            anchor: '50%',
            emptyText: 'Выберите JPG-изображение..',
            buttonText: 'Открыть...',
            reference: 'char_s_icon'
        },{
            fieldLabel: 'Большой аватар (120x210)',
            name: 'b_icon',
            xtype: 'fileuploadfield',
            anchor: '50%',
            emptyText: 'Выберите JPG-изображение..',
            buttonText: 'Открыть...',
            reference: 'char_b_icon'
        },{
            fieldLabel: 'Фишка (64x64)',
            name: 'token',
            xtype: 'fileuploadfield',
            anchor: '50%',
            emptyText: 'Выберите PNG-изображение..',
            buttonText: 'Открыть...',
            id: 'char-token'
        },{
            fieldLabel: 'Описание',
            name: 'char_info',
            reference: 'char_fullinfo',
            xtype: 'htmleditor',
            width: 710,
            height: 380,
            bind: {
                value: '{char.notes}'
            }
        }],
        buttons: [{
            text: 'Сохранить',
            formBind: true,
            handler: 'onInfoSave'
        }]
    }],

    onRender: function(){
        this.callParent();

        this.getController().getCharsheet();
    }
});
