Ext.define('DwD2.view.inwin.npcinfo.NpcinfoController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.inwin-npcinfo-npcinfo',

    getNpcinfoText: 'Загрузка...',

    getNpcinfo: function(){
        var view = this.getView();
        var rm = view.requestManager;
        var settings = view.viewport.settings;
        var data = {
            ssid: settings.get('ssid'),
            nid: view.npcId,
            mode: 'npc'
        };

        view.up('window').mask(this.getNpcinfoText);

        rm.go({
            url: 'getinfo.php',
            data: data,
            scope: this,
            success: this.onGetNpcinfo,
            failure: this.onFailure
        });
    },

    onGetNpcinfo: function(response){
        var data = Ext.util.JSON.decode(response.responseText);

        this.getView().up('window').setTitle('Описание НПЦ - ' + data.info.settled_name);

        this.lookupReference('npcinfo_fs').setTitle(data.info.settled_name);
        this.lookupReference('npcinfo_pic').setSrc('resources/data/faces/npc/b/' + translit(data.info.original_name) + '.jpg');

        var txt = '<h3>' + data.info.settled_name + '</h3><i>' + data.info.type + '</i><br/><br/>' + data.info.mmdesc;

        this.lookupReference('npcinfo_descr').update(txt);

        var settings = this.getView().viewport.settings;
        if (settings.get('dm') == 1 || settings.get('minidm') == 1) this.lookupReference('npcinfo_dmtab').enable();

        this.addDmControl(data);

        this.onSuccess();
    },

    addDmControl: function(data){
        var attack_regex = /\+\d{1,2}\sк\sатаке/gi;
        var saves_regex = /(Сил|Ловк|Тел|Инт|Мудр|Хар)\s(\+\d{1,2})/gi;
        var skills_regex = /(Атлетика|Внимательность|Выживание|Медицина|Проницательность|Уход\sза\sживотными|Акробатика|Ловкость\sрук|Скрытность|Анализ|История|Магия|Природа|Религия|Выступление|Запугивание|Обман|Убеждение|Аркана)\s(\+\d{1,2})/gi;
        var damage_regex = /\d{1,2}\s\(\d{1,2}[dкд]{1}\d{1,2}(\s?[\+\-]{1}\s?\d{1,2})?\)(?:(?:\s(рубящего|колющего|дробящего|физического))?\sурона(?:\s(огнем|звуком|излучением|кислотой|некротической\sэнергией|психической\sэнергией|силовым\sполем|холодом|электричеством|ядом|излучением\sи\sогнем))?)?/gi;

        var actions = data.info.actions.replace(attack_regex, "<a onclick=\"Ext.getCmp('npcinfo-tabpanel').fireEvent('npcattack', '$&', " + data.info.id + ")\">$&</a>");
        actions = actions.replace(damage_regex, "<a onclick=\"Ext.getCmp('npcinfo-tabpanel').fireEvent('npcdamage', '$&', " + data.info.id + ")\">$&</a>");

        var traits = data.info.traits.replace(damage_regex, "<a onclick=\"Ext.getCmp('npcinfo-tabpanel').fireEvent('npcdamage', '$&', " + data.info.id + ")\">$&</a>");
        traits = traits.replace(attack_regex, "<a onclick=\"Ext.getCmp('npcinfo-tabpanel').fireEvent('npcattack', '$&', " + data.info.id + ")\">$&</a>");

        var legendary = data.info.legendary.replace(damage_regex, "<a onclick=\"Ext.getCmp('npcinfo-tabpanel').fireEvent('npcdamage', '$&', " + data.info.id + ")\">$&</a>");

        var str_check = '<a onclick="Ext.getCmp(\'npcinfo-tabpanel\').fireEvent(\'npccheck\', \'Сила\', \'' + this.getMod(data.info.str) + '\')">' + this.getMod(data.info.str) + '</a>';
        var dex_check = '<a onclick="Ext.getCmp(\'npcinfo-tabpanel\').fireEvent(\'npccheck\', \'Ловкость\', \'' + this.getMod(data.info.dex) + '\')">' + this.getMod(data.info.dex) + '</a>';
        var con_check = '<a onclick="Ext.getCmp(\'npcinfo-tabpanel\').fireEvent(\'npccheck\', \'Телосложение\', \'' + this.getMod(data.info.con) + '\')">' + this.getMod(data.info.con) + '</a>';
        var intl_check = '<a onclick="Ext.getCmp(\'npcinfo-tabpanel\').fireEvent(\'npccheck\', \'Интеллект\', \'' + this.getMod(data.info.intl) + '\')">' + this.getMod(data.info.intl) + '</a>';
        var wis_check = '<a onclick="Ext.getCmp(\'npcinfo-tabpanel\').fireEvent(\'npccheck\', \'Мудрость\', \'' + this.getMod(data.info.wis) + '\')">' + this.getMod(data.info.wis) + '</a>';
        var cha_check = '<a onclick="Ext.getCmp(\'npcinfo-tabpanel\').fireEvent(\'npccheck\', \'Харизма\', \'' + this.getMod(data.info.cha) + '\')">' + this.getMod(data.info.cha) + '</a>';

        var saves = data.info.saves.replace(saves_regex, "<a onclick=\"Ext.getCmp('npcinfo-tabpanel').fireEvent('npccheck', '$1', '$2')\">$&</a>");

        var skills = data.info.skills.replace(skills_regex, "<a onclick=\"Ext.getCmp('npcinfo-tabpanel').fireEvent('npccheck', '$1', '$2')\">$&</a>");

        var txt = '<h3>' + data.info.name + '</h3><i>' + data.info.type + '</i><br/><hr/><b>КД:</b> ' + data.info.ac + '<br/><b>ХП:</b> ' + data.info.hp + '<br/><b>Скорость:</b> ' + data.info.speed + '<br/><hr/><table cellspacing="0" cellpadding="0" class="chartable"><tr><td><b>СИЛ</b></td><td><b>ЛОВ</b></td><td><b>ТЕЛ</b></td><td><b>ИНТ</b></td><td><b>МУД</b></td><td><b>ХАР</b></td></tr><tr><td>' + data.info.str + ' (' + str_check + ')</td><td>' + data.info.dex + ' (' + dex_check + ')</td><td>' + data.info.con + ' (' + con_check + ')</td><td>' + data.info.intl + ' (' + intl_check + ')</td><td>' + data.info.wis + ' (' + wis_check + ')</td><td>' + data.info.cha + ' (' + cha_check + ')</td></tr></table><b>Спасброски:</b> ' + saves + '<br/><hr/><b>Навыки:</b> ' + skills + '<br/><b>Сопротивления:</b> ' + data.info.resist + '<br/><b>Уязвимости:</b> ' + data.info.vulnerab + '<br/><b>Иммунитет к урону:</b> ' + data.info.dmg_immune + '<br/><b>Иммунитет к состояниям:</b> ' + data.info.cond_immune + '<br/><b>Чувства:</b> ' + data.info.senses + '<br/><b>Языки:</b> ' + data.info.lang + '<br/><b>Уровень угрозы:</b> ' + data.info.challenge + ' (' + data.info.xp + ' опыта)<br/><hr/><b>Особенности:</b><br/>' + this.checkEmpty(traits) + '<h3>Действия</h3>' + this.checkEmpty(actions) + '<h3>Легендарные действия</h3>' + this.checkEmpty(legendary) + '<h3>Реакции</h3>' + this.checkEmpty(data.info.reactions);

        this.lookupReference('npcinfo_dm').update(txt);
    },

    getMod: function(val){
        var mod = Math.floor((val - 10) / 2);

        if (mod >= 0){
            mod = '+' + mod;
        }

        return mod;
    },

    checkEmpty: function(val){
        if (val == '') return '-';
        else return val;
    },

    onSuccess: function(){
        var window = this.getView().up('window');

        window.unmask();
    },

    onFailure: function(){
        var window = this.getView().up('window');

        window.unmask();
    }
    
});
