
Ext.define('DwD2.view.inwin.npcinfo.Npcinfo',{
    extend: 'Ext.tab.Panel',

    requires: [
        'DwD2.view.inwin.npcinfo.NpcinfoController',
        'DwD2.view.inwin.npcinfo.NpcinfoModel'
    ],

    controller: 'inwin-npcinfo-npcinfo',
    viewModel: {
        type: 'inwin-npcinfo-npcinfo'
    },

    id: 'npcinfo-tabpanel',
    xtype: 'npcinfo',

    items: [{
        title: 'Описание',
        tooltip: 'Общее описание НПЦ',
        layout: 'border',
        items: [{
            region: 'west',
            xtype: 'fieldset',
            width: 235,
            reference: 'npcinfo_fs',
            items: [{
                xtype: 'image',
                alt: 'Картинка НПЦ',
                reference: 'npcinfo_pic'
            }]
        }, {
            bodyPadding: 10,
            region: 'center',
            border: false,
            bodyCls: 'itemdescr-panel',
            reference: 'npcinfo_descr',
            scrollable: true
        }]
    },{
        scrollable: true,
        title: 'ДМ',
        tooltip: 'Мастерский контроль',
        reference: 'npcinfo_dmtab',
        disabled: true,
        items: [{
            scrollable: true,
            bodyPadding: 10,
            border: false,
            bodyCls: 'itemdescr-panel',
            reference: 'npcinfo_dm'
        }]
    }],

    onRender: function(){
        this.callParent();

        this.getController().getNpcinfo();
    }
});
