
Ext.define('DwD2.view.inwin.showpower.Showpower',{
    extend: 'Ext.panel.Panel',

    requires: [
        'DwD2.view.inwin.showpower.ShowpowerController',
        'DwD2.view.inwin.showpower.ShowpowerModel'
    ],

    controller: 'inwin-showpower-showpower',
    viewModel: {
        type: 'inwin-showpower-showpower'
    },

    bodyCls: 'itemdescr-panel',
    xtype: 'showpower',
    scrollable: true,
    frame: true,

    onRender: function(){
        this.callParent();

        this.getController().getPower(this.powerId);
    }
});
