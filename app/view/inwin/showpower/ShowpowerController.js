Ext.define('DwD2.view.inwin.showpower.ShowpowerController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.inwin-showpower-showpower',
    
    getPowerText: 'Загружаем...',

    getPower: function(id){
        var view = this.getView();
        var rm = view.requestManager;
        var settings = view.viewport.settings;
        var data = {
            ssid: settings.get('ssid')
        };

        data.mode = 'getpower';
        data.power_id = id;

        view.up('window').mask(this.getPowerText);

        rm.go({
            url: 'charaction.php',
            data: data,
            scope: this,
            success: this.onGetSuccess,
            failure: this.onFailure
        });
    },

    onGetSuccess: function(response){
        var data = Ext.JSON.decode(response.responseText);
        var view = this.getView();
        var window = view.up('window');

        window.unmask();

        var txt = '<b>' + data.power.title + '</b><br/><small><i>' + data.power.classname + '</i></small><br/><br/>' + data.power.descr;

        view.update(txt);
    },

    onFailure: function(){
        var window = this.getView().up('window');

        window.unmask();
    }
    
});
