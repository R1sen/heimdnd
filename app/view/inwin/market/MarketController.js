Ext.define('DwD2.view.inwin.market.MarketController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.inwin-market-market',

    dmText: 'Загружаем...',
    help: '',

    getMarket: function () {
        var view = this.getView();
        var rm = view.requestManager;
        var settings = view.viewport.settings;
        var data = {
            ssid: settings.get('ssid'),
            mode: 'download'
        };

        view.up('window').mask(this.dmText);

        rm.go({
            url: 'market.php',
            data: data,
            scope: this,
            success: this.onGetMarket,
            failure: this.onFailure
        });
    },

    onGetMarket: function (response) {
        var data = Ext.JSON.decode(response.responseText);

        Ext.getStore('marketItemsTree').setRoot(data.market_items);

        this.onSuccess();
    },

    onMarketItemBuy: function () {

        var rec = this.lookupReference('market-tree').getSelection();

        var folder = rec[0].get('cls');

        if (folder == 'folder') {
            return;
        }

        var view = this.getView();
        var rm = view.requestManager;
        var settings = view.viewport.settings;
        var data = {
            ssid: settings.get('ssid'),
            cat: rec[0].get('type'),
            cost: rec[0].get('cost'),
            id: rec[0].get('id'),
            mode: 'select'

        }

        rm.go({
            url: 'market.php',
            data: data,
            scope: this,
            success: this.onSuccess,
            failure: this.onFailure
        });
    },

    onMarketItemSelect: function (obj, rec, index, opt) {

        var folder = rec.get('cls');

        if (folder == 'folder') {
            return;
        }

        var view = this.getView();
        var rm = view.requestManager;
        var settings = view.viewport.settings;
        var data = {
            type: rec.get('type'),
            id: rec.get('id'),
            mode: 'gethelp'
        };

        this.help = rec.get('type');

        rm.go({
            url: 'market.php',
            data: data,
            scope: this,
            success: this.onMarketItemSelectSuccess,
            failure: this.onFailure
        });

    },

    onMarketItemSelectSuccess: function (response) {

        var data = Ext.JSON.decode(response.responseText);
        var panel = this.lookupReference('market_itemdescr-panel');

        var moneys = new Array( data.helpobj[0].cost / 1000 | 0, ((data.helpobj[0].cost % 1000) / 100 | 0), ((data.helpobj[0].cost % 100) / 10 | 0), data.helpobj[0].cost % 10);

        switch (this.help) {
            case 'weapons': {
                var dmg_type = {slashing: 'режущий', piersing: 'колющий', bludgeoning: 'дробящий'};
                var helptxt = '<b>Цена:</b> |<span style="color: steelBlue  ">' + moneys[0] + ' платины</span>|<span style="color: DarkGoldenRod ">' + moneys[1] + ' золота</span>|<span style="color: silver ">' + moneys[2] + ' серебра</span>|<span style="color: chocolate  ">' + moneys[3] + ' медных</span>|<br/><b>Урон:</b> ' + data.helpobj[0].dice + '<br/><b>Тип урона:</b> ' + dmg_type[data.helpobj[0].dmg_type] + '<br/><b>Свойства:</b> ' + data.helpobj[0].properties + '<br/><b>Вес:</b> ' + data.helpobj[0].weight + ' фунтов';
                break;
            }

            case 'armor': {
                var armor_penalty = {0: 'Нет', 1: 'Да'};
                var helptxt = '<b>Цена:</b> |<span style="color: steelBlue  ">' + moneys[0] + ' платины</span>|<span style="color: DarkGoldenRod ">' + moneys[1] + ' золота</span>|<span style="color: silver ">' + moneys[2] + ' серебра</span>|<span style="color: chocolate  ">' + moneys[3] + ' медных</span>|<br/><b>КД:</b> ' + data.helpobj[0].ac + '<br/><b>Штраф скрытности:</b> ' + armor_penalty[data.helpobj[0].stealth] + '<br/><b>Вес:</b> ' + data.helpobj[0].weight + ' фунтов';
                break;
            }

            case 'equip': {
                var helptxt = '<b>Цена:</b> |<span style="color: steelBlue  ">' + moneys[0] + ' платины</span>|<span style="color: DarkGoldenRod ">' + moneys[1] + ' золота</span>|<span style="color: silver ">' + moneys[2] + ' серебра</span>|<span style="color: chocolate  ">' + moneys[3] + ' медных</span>|<br/><b>Описание:</b> ' + data.helpobj[0].descr + '<br/><b>Вес:</b> ' + data.helpobj[0].weight + ' фунтов';
                break;
            }

            default: {
                var helptxt = 'Нету справки по этому объекту.';
            }
        }


        panel.update(helptxt);
        panel.unmask();

    },


    onFailure: function () {
        var window = this.getView().up('window');

        window.unmask();
    },


    onSuccess: function () {
        var window = this.getView().up('window');

        window.unmask();
    }

});
