Ext.define('DwD2.view.inwin.market.Market', {
    extend: 'Ext.panel.Panel',

    requires: [
        'DwD2.view.inwin.market.MarketController',
        'DwD2.view.inwin.market.MarketModel'
    ],

    controller: 'inwin-market-market',
    viewModel: {
        type: 'inwin-market-market'
    },

    xtype: 'market',
    id: 'market-panel',
    layout: 'border',
    reference: 'market_place',

    items: [{
        region: 'west',
        width: 275,
        xtype: 'treepanel',
        store: 'marketItemsTree',
        reference: 'market-tree',
        rootVisible: false,
        useArrows: true,
        frame: true,
        title: 'Предметы',
        bufferedRenderer: false,
        srollable: true,
        animate: true,
        listeners: {
            select: 'onMarketItemSelect',
            scope: 'controller'
        }
    }, {

        region: 'center',
        border: false,
        scrollable: true,
        reference: 'market_itemdescr-panel',
        bodyCls: 'itemdescr-panel',
        title: 'Описание предмета',

        buttons: [{
            text: 'Купить',
            //iconCls: 'rotate',
            tooltip: 'Купить предмет',
            reference: 'buy_button',
            handler: 'onMarketItemBuy'
        }]
    }],

    onRender: function () {
        this.callParent();

        this.getController().getMarket();
    }
});