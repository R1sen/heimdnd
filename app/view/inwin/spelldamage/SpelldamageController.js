Ext.define('DwD2.view.inwin.spelldamage.SpelldamageController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.inwin-spelldamage-spelldamage',

    damageText: 'Наносим урон...',

    onMakeDamage: function(){
        var form = this.getView();

        if (form.isValid()){
            var data = form.getValues();
            var rm = form.requestManager;
            var settings = form.viewport.settings;

            data.ssid = settings.get('ssid');
            data.mode = 'damage';
            data.sid = form.spellId;
            data.str = form.spellString;
            data.clid = form.spellClid;

            form.up('window').mask(this.damageText);

            rm.go({
                url: 'spellattack.php',
                data: data,
                scope: this,
                success: this.onDamageSuccess,
                failure: this.onDamageFailure
            });
        }
    },

    onDamageFailure: function(){
        var window = this.getView().up('window');

        window.unmask();
    },

    onDamageSuccess: function(response){
        var window = this.getView().up('window');

        window.unmask();
        window.close();
    },

    onSpecialKey: function(field, e) {
        if (e.getKey() === e.ENTER) {
            this.onMakeDamage();
        }
    }
    
});
