
Ext.define('DwD2.view.inwin.spelldamage.Spelldamage',{
    extend: 'Ext.form.Panel',

    requires: [
        'DwD2.view.inwin.spelldamage.SpelldamageController',
        'DwD2.view.inwin.spelldamage.SpelldamageModel'
    ],

    controller: 'inwin-spelldamage-spelldamage',
    viewModel: {
        type: 'inwin-spelldamage-spelldamage'
    },

    frame: true,
    xtype: 'spelldamage',
    layout: 'anchor',
    defaults: {
        anchor: '100%',
        labelWidth: 140
    },
    bodyPadding: 10,

    items: [{
        xtype: 'textfield',
        name: 'mod',
        reference: 'mod_textfield',
        fieldLabel: 'Модификатор',
        regex: /^[\+\-]?\d{1,2}$/,
        regexText: 'Допускается только целое число с плюсом или минусом, не большее 99.',
        allowBlank: false,
        enableKeyEvents: true,
        value: '0',
        listeners: {
            specialKey: 'onSpecialKey'
        }
    },{
        xtype: 'combobox',
        fieldLabel: 'Цель',
        editable: false,
        emptyText: 'Выберите цель..',
        queryMode: 'local',
        store: 'allTargets',
        displayField: 'name',
        name: 'target',
        valueField: 'id',
        triggerAction: 'all',
        reference: 'target_combo'
    },{
        xtype: 'checkboxfield',
        reference: 'crit_box',
        fieldLabel: 'Крит',
        inputValue: '1',
        name: 'crit'
    },{
        xtype: 'combobox',
        fieldLabel: 'Фактор',
        editable: false,
        emptyText: 'Выберите множитель..',
        queryMode: 'local',
        store: 'factorSelect',
        displayField: 'name',
        name: 'factor',
        valueField: 'id',
        triggerAction: 'all',
        reference: 'factor_combo'
    }],

    buttons: [{
        text: 'Урон',
        formBind: true,
        disabled: true,
        handler: 'onMakeDamage'
    }]
});
