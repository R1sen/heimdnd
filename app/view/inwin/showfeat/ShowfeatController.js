Ext.define('DwD2.view.inwin.showfeat.ShowfeatController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.inwin-showfeat-showfeat',

    getFeatText: 'Загружаем...',

    getFeat: function(id){
        var view = this.getView();
        var rm = view.requestManager;
        var settings = view.viewport.settings;
        var data = {
            ssid: settings.get('ssid')
        };

        data.mode = 'getfeat';
        data.fid = id;

        view.up('window').mask(this.getFeatText);

        rm.go({
            url: 'charaction.php',
            data: data,
            scope: this,
            success: this.onGetSuccess,
            failure: this.onFailure
        });
    },

    onGetSuccess: function(response){
        var data = Ext.JSON.decode(response.responseText);
        var view = this.getView();
        var window = view.up('window');

        window.unmask();

        var txt = '<b>' + data.feat.title + '</b><br/><br/>' + data.feat.descr;

        view.update(txt);
    },

    onFailure: function(){
        var window = this.getView().up('window');

        window.unmask();
    }
    
});
