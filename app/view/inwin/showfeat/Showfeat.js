
Ext.define('DwD2.view.inwin.showfeat.Showfeat',{
    extend: 'Ext.panel.Panel',

    requires: [
        'DwD2.view.inwin.showfeat.ShowfeatController',
        'DwD2.view.inwin.showfeat.ShowfeatModel'
    ],

    controller: 'inwin-showfeat-showfeat',
    viewModel: {
        type: 'inwin-showfeat-showfeat'
    },

    bodyCls: 'itemdescr-panel',
    xtype: 'showfeat',
    scrollable: true,
    frame: true,

    onRender: function(){
        this.callParent();

        this.getController().getFeat(this.featId);
    }
});
