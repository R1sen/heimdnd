Ext.define('DwD2.view.inwin.npcdamage.NpcdamageController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.inwin-npcdamage-npcdamage',

    damageText: 'Наносим урон...',

    onMakeDamage: function(){
        var form = this.getView();

        if (form.isValid()){
            var data = form.getValues();
            var rm = form.requestManager;
            var settings = form.viewport.settings;

            data.ssid = settings.get('ssid');
            data.mode = 'mmdamage';
            data.string = form.string;
            data.nid = form.npcId;

            form.up('window').mask(this.damageText);

            rm.go({
                url: 'dm.php',
                data: data,
                scope: this,
                success: this.onDamageSuccess,
                failure: this.onDamageFailure
            });
        }
    },

    onImprovChange: function(obj, newValue){
        if (newValue == 1){
            this.lookupReference('improvised_combo').enable();
        } else {
            this.lookupReference('improvised_combo').disable();
        }
    },

    onDamageFailure: function(){
        var window = this.getView().up('window');

        window.unmask();
    },

    onDamageSuccess: function(response){
        var window = this.getView().up('window');

        window.unmask();
        window.close();
    },

    onSpecialKey: function(field, e) {
        if (e.getKey() === e.ENTER) {
            this.onMakeDamage();
        }
    }
    
});
