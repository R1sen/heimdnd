
Ext.define('DwD2.view.inwin.npcdamage.Npcdamage',{
    extend: 'Ext.form.Panel',

    requires: [
        'DwD2.view.inwin.npcdamage.NpcdamageController',
        'DwD2.view.inwin.npcdamage.NpcdamageModel'
    ],

    controller: 'inwin-npcdamage-npcdamage',
    viewModel: {
        type: 'inwin-npcdamage-npcdamage'
    },

    frame: true,
    xtype: 'npcdamage',
    layout: 'anchor',
    defaults: {
        anchor: '100%',
        labelWidth: 140
    },
    bodyPadding: 10,

    items: [{
        xtype: 'combobox',
        fieldLabel: 'Источник',
        editable: false,
        emptyText: 'Выберите тип урона..',
        queryMode: 'local',
        store: 'discretSelect',
        displayField: 'name',
        name: 'discret',
        valueField: 'id',
        triggerAction: 'all',
        reference: 'hand_combo'
    },{
        xtype: 'textfield',
        name: 'mod',
        reference: 'mod_textfield',
        fieldLabel: 'Модификатор',
        regex: /^[\+\-]?\d{1,2}$/,
        regexText: 'Допускается только целое число с плюсом или минусом, не большее 99.',
        allowBlank: false,
        enableKeyEvents: true,
        value: '0',
        listeners: {
            specialKey: 'onSpecialKey'
        }
    },{
        xtype: 'combobox',
        fieldLabel: 'Цель',
        editable: false,
        emptyText: 'Выберите цель..',
        queryMode: 'local',
        store: 'allTargets',
        displayField: 'name',
        name: 'target',
        valueField: 'id',
        triggerAction: 'all',
        reference: 'target_combo'
    },{
        xtype: 'checkboxfield',
        reference: 'crit_box',
        fieldLabel: 'Крит',
        inputValue: '1',
        name: 'crit'
    },{
        xtype: 'combobox',
        fieldLabel: 'Как кто',
        editable: false,
        emptyText: 'Выберите кто атакует..',
        queryMode: 'local',
        store: 'npcStore',
        displayField: 'name',
        name: 'npc',
        valueField: 'id',
        triggerAction: 'all',
        reference: 'npc_combo'
    }],

    buttons: [{
        text: 'Урон',
        formBind: true,
        disabled: true,
        handler: 'onMakeDamage'
    }]
});
