
Ext.define('DwD2.view.inwin.Basewin',{
    extend: 'Ext.window.Window',

    requires: [
        'DwD2.view.inwin.BasewinController',
        'DwD2.view.inwin.BasewinModel'
    ],

    controller: 'inwin-basewin',
    viewModel: {
        type: 'inwin-basewin'
    },

    layout: 'fit',
    alias: 'widget.basewin',
    minimizable: true,
    closable: true,
    autoShow: true,
    xtype: 'basewin',
    listeners: {
        render: 'registerWindow',
        close: 'onClose',
        minimize: 'onMinimize',
        move: 'checkMove',
        resize: 'checkResize',
        scope: 'controller'
    }
});
