
Ext.define('DwD2.view.inwin.powerdice.Powerdice',{
    extend: 'Ext.form.Panel',

    requires: [
        'DwD2.view.inwin.powerdice.PowerdiceController',
        'DwD2.view.inwin.powerdice.PowerdiceModel'
    ],

    controller: 'inwin-powerdice-powerdice',
    viewModel: {
        type: 'inwin-powerdice-powerdice'
    },

    frame: true,
    xtype: 'powerdice',
    layout: 'anchor',
    defaults: {
        anchor: '100%',
        labelWidth: 140
    },
    bodyPadding: 10,

    items: [{
        xtype: 'textfield',
        name: 'mod',
        reference: 'mod_textfield',
        fieldLabel: 'Модификатор',
        regex: /^[\+\-]?\d{1,2}$/,
        regexText: 'Допускается только целое число с плюсом или минусом, не большее 99.',
        allowBlank: false,
        enableKeyEvents: true,
        value: '0',
        listeners: {
            specialKey: 'onSpecialKey'
        }
    },{
        xtype: 'combobox',
        fieldLabel: 'Цель',
        editable: false,
        emptyText: 'Выберите цель..',
        queryMode: 'local',
        store: 'allTargets',
        displayField: 'name',
        name: 'target',
        valueField: 'id',
        triggerAction: 'all',
        reference: 'target_combo'
    },{
        xtype: 'checkboxfield',
        reference: 'crit_box',
        fieldLabel: 'Крит',
        inputValue: '1',
        name: 'crit'
    }],

    buttons: [{
        text: 'Использовать',
        formBind: true,
        disabled: true,
        handler: 'onPowerDice'
    }]
});
