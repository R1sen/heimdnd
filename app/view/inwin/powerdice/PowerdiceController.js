Ext.define('DwD2.view.inwin.powerdice.PowerdiceController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.inwin-powerdice-powerdice',

    powerDiceText: 'Используем...',

    onPowerDice: function(){
        var form = this.getView();

        if (form.isValid()){
            var data = form.getValues();
            var rm = form.requestManager;
            var settings = form.viewport.settings;

            data.ssid = settings.get('ssid');
            data.mode = 'powerdice';
            data.string = form.string;
            data.pid = form.pid;

            form.up('window').mask(this.powerDiceText);

            rm.go({
                url: 'charaction.php',
                data: data,
                scope: this,
                success: this.onPowerDiceSuccess,
                failure: this.onPowerDiceFailure
            });
        }
    },

    onPowerDiceFailure: function(){
        var window = this.getView().up('window');

        window.unmask();
    },

    onPowerDiceSuccess: function(response){
        var window = this.getView().up('window');

        window.unmask();
        window.close();
    },

    onSpecialKey: function(field, e) {
        if (e.getKey() === e.ENTER) {
            this.onPowerDice();
        }
    }
    
});
