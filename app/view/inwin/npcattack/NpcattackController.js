Ext.define('DwD2.view.inwin.npcattack.NpcattackController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.inwin-npcattack-npcattack',

    attackText: 'Атакуем...',

    onNpcAttack: function(){
        var form = this.getView();

        if (form.isValid()){
            var data = form.getValues();
            var rm = form.requestManager;
            var settings = form.viewport.settings;

            data.ssid = settings.get('ssid');
            data.mode = 'mmattack';
            data.nid = form.npcId;
            data.string = form.string;

            form.up('window').mask(this.attackText);

            rm.go({
                url: 'dm.php',
                data: data,
                scope: this,
                success: this.onAttackSuccess,
                failure: this.onAttackFailure
            });
        }
    },

    onAttackFailure: function(){
        var window = this.getView().up('window');

        window.unmask();
    },

    onAttackSuccess: function(response){
        var window = this.getView().up('window');

        window.unmask();
        window.close();
    },

    onSpecialKey: function(field, e) {
        if (e.getKey() === e.ENTER) {
            this.onNpcAttack();
        }
    }
    
});
