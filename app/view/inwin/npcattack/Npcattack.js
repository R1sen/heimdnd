
Ext.define('DwD2.view.inwin.npcattack.Npcattack',{
    extend: 'Ext.form.Panel',

    requires: [
        'DwD2.view.inwin.npcattack.NpcattackController',
        'DwD2.view.inwin.npcattack.NpcattackModel'
    ],

    controller: 'inwin-npcattack-npcattack',
    viewModel: {
        type: 'inwin-npcattack-npcattack'
    },

    frame: true,
    xtype: 'npcattack',
    layout: 'anchor',
    defaults: {
        anchor: '100%',
        labelWidth: 140
    },
    bodyPadding: 10,
    
    items: [{
        xtype: 'textfield',
        name: 'mod',
        reference: 'mod_textfield',
        fieldLabel: 'Модификатор',
        regex: /^[\+\-]?\d{1,2}$/,
        regexText: 'Допускается только целое число с плюсом или минусом, не большее 99.',
        allowBlank: false,
        enableKeyEvents: true,
        value: '0',
        listeners: {
            specialKey: 'onSpecialKey'
        }
    },{
        xtype: 'textfield',
        name: 'diff',
        reference: 'diff_textfield',
        fieldLabel: 'Сложность',
        regex: /^\d{1,2}$/,
        regexText: 'Допускается только целочисленное значение не большее 99.',
        allowBlank: false,
        enableKeyEvents: true,
        value: '0',
        listeners: {
            specialKey: 'onSpecialKey'
        }
    },{
        xtype: 'combobox',
        fieldLabel: 'Цель',
        editable: false,
        emptyText: 'Выберите цель..',
        queryMode: 'local',
        store: 'allTargets',
        displayField: 'name',
        name: 'target',
        valueField: 'id',
        triggerAction: 'all',
        reference: 'target_combo'
    },{
        xtype: 'checkboxfield',
        name: 'adv',
        reference: 'advantage_box',
        fieldLabel: 'Преимущество',
        inputValue: '1'
    },{
        xtype: 'checkboxfield',
        name: 'disadv',
        reference: 'disadvantage_box',
        fieldLabel: 'Помеха',
        inputValue: '1'
    },{
        xtype: 'checkboxfield',
        name: 'twoattk',
        reference: 'twoattk_box',
        fieldLabel: 'Две атаки',
        inputValue: '1'
    },{
        xtype: 'combobox',
        fieldLabel: 'Как кто',
        editable: false,
        emptyText: 'Выберите кто атакует..',
        queryMode: 'local',
        store: 'npcStore',
        displayField: 'name',
        name: 'npc',
        valueField: 'id',
        triggerAction: 'all',
        reference: 'npc_combo'
    }],

    buttons: [{
        text: 'Атаковать',
        formBind: true,
        disabled: true,
        handler: 'onNpcAttack'
    }]
});
