
Ext.define('DwD2.view.inwin.npccheck.Npccheck',{
    extend: 'Ext.form.Panel',

    requires: [
        'DwD2.view.inwin.npccheck.NpccheckController',
        'DwD2.view.inwin.npccheck.NpccheckModel'
    ],

    controller: 'inwin-npccheck-npccheck',
    viewModel: {
        type: 'inwin-npccheck-npccheck'
    },

    frame: true,
    xtype: 'npccheck',
    layout: 'anchor',
    defaults: {
        anchor: '100%'
    },
    bodyPadding: 10,

    items: [{
        xtype: 'textfield',
        name: 'mod',
        reference: 'mod_textfield',
        fieldLabel: 'Модификатор',
        regex: /^[\+\-]?\d{1,2}$/,
        regexText: 'Допускается только целое число с плюсом или минусом, не большее 99.',
        allowBlank: false,
        enableKeyEvents: true,
        value: '0',
        listeners: {
            specialKey: 'onSpecialKey'
        }
    },{
        xtype: 'textfield',
        name: 'diff',
        reference: 'diff_textfield',
        fieldLabel: 'Сложность',
        regex: /^\d{1,2}$/,
        regexText: 'Допускается только целочисленное значение не большее 99.',
        allowBlank: false,
        enableKeyEvents: true,
        value: '0',
        listeners: {
            specialKey: 'onSpecialKey'
        }
    },{
        xtype: 'checkboxfield',
        name: 'adv',
        reference: 'advantage_box',
        fieldLabel: 'Преимущество',
        inputValue: '1'
    },{
        xtype: 'checkboxfield',
        name: 'disadv',
        reference: 'disadvantage_box',
        fieldLabel: 'Помеха',
        inputValue: '1'
    },{
        xtype: 'combobox',
        fieldLabel: 'За кого',
        editable: false,
        emptyText: 'Выберите кто атакует..',
        queryMode: 'local',
        store: 'npcStore',
        displayField: 'name',
        name: 'npc',
        valueField: 'id',
        triggerAction: 'all',
        reference: 'npc_combo'
    }],

    buttons: [{
        text: 'Проверить',
        formBind: true,
        disabled: true,
        handler: 'onMakeCheck'
    }]
});
