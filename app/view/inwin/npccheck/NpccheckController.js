Ext.define('DwD2.view.inwin.npccheck.NpccheckController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.inwin-npccheck-npccheck',

    checkText: 'Проверяем...',

    onMakeCheck: function(){
        var form = this.getView();

        if (form.isValid()){
            var data = form.getValues();
            var rm = form.requestManager;
            var settings = form.viewport.settings;

            data.ssid = settings.get('ssid');
            data.check = form.makeCheck;
            data.string = form.string;
            data.mode = 'npccheck';

            form.up('window').mask(this.checkText);

            rm.go({
                url: 'dm.php',
                data: data,
                scope: this,
                success: this.onCheckSuccess,
                failure: this.onCheckFailure
            });
        }
    },

    onCheckFailure: function(){
        var window = this.getView().up('window');

        window.unmask();
    },

    onCheckSuccess: function(response){
        var window = this.getView().up('window');

        window.unmask();
        window.close();
    },

    onSpecialKey: function(field, e) {
        if (e.getKey() === e.ENTER) {
            this.onMakeCheck();
        }
    }
    
});
