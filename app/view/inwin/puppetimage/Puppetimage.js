
Ext.define('DwD2.view.inwin.puppetimage.Puppetimage',{
    extend: 'Ext.Img',

    requires: [
        'DwD2.view.inwin.puppetimage.PuppetimageController',
        'DwD2.view.inwin.puppetimage.PuppetimageModel'
    ],

    controller: 'inwin-puppetimage-puppetimage',
    viewModel: {
        type: 'inwin-puppetimage-puppetimage'
    },

    puppetTipTxt: 'Щелчок правой кнопки мыши фиксирует описание на экране.<br/>Щелчок левой кнопки мыши снимает надетый предмет.',
    
    width: 50,
    height: 50,
    
    xtype: 'puppetimage',

    listeners: {
        afterrender: function (me) {
            Ext.tip.QuickTipManager.register({
                target: me.getId(),
                title: me.alt,
                text: me.puppetTipTxt
            });
        }
    }
});
