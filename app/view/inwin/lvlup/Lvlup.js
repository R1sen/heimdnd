
Ext.define('DwD2.view.inwin.lvlup.Lvlup',{
    extend: 'Ext.panel.Panel',

    requires: [
        'DwD2.view.inwin.lvlup.LvlupController',
        'DwD2.view.inwin.lvlup.LvlupModel'
    ],

    controller: 'inwin-lvlup-lvlup',
    viewModel: {
        type: 'inwin-lvlup-lvlup'
    },

    bbar: ['->', {
        text: 'Далее &raquo;',
        handler: 'nextStep'
    }],

    layout: 'card',
    id: 'lvlup-cardpanel',
    border: false,
    xtype: 'lvlup',

    items: [{
        bodyCls: 'panel-filling',
        border: false,
        html: 'Поздравляю, вы можете повысить уровень у вашего персонажа! Прошу обратить внимание, что для этого требуется двойной длительный отдых, поэтому убедитесь в том, что у вас есть возможность его сделать.<br><b>Внимание!</b> Начав процесс поднятия уровня, его нельзя будет отменить (но можно будет продолжить, переоткрыв окно), так что убедитесь заранее в том, что вы знаете, что хотите выбрать на уровне.'
    }]
});
