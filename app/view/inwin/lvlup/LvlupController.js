Ext.define('DwD2.view.inwin.lvlup.LvlupController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.inwin-lvlup-lvlup',

    nextText: 'Готовим шаг...',
    loadText: 'Загружаем...',

    selCount: 0,
    selNeed: 0,
    crStep: 0,
    crCid: 0,
    currentType: '',
    selectedItems: new Array(),
    returned: false,

    onFailure: function(){
        var window = this.getView().up('window');

        window.unmask();
    },

    onSuccess: function(response){
        var window = this.getView().up('window');

        window.unmask();
    },

    nextStep: function(){
        if (this.crStep == 0){
            var view = this.getView();
            var settings = view.viewport.settings;
            var rm = view.requestManager;
            var data = {};

            data.mode = 'select_class';
            data.ssid = settings.get('ssid');

            view.up('window').mask(this.nextText);

            rm.go({
                url: 'lvlup.php',
                data: data,
                scope: this,
                success: this.onNextSuccess,
                failure: this.onFailure
            });
        } else {
            if (this.currentType != 'finish' && this.currentType != 'skip' && (this.selCount == 0 || this.selCount != this.selNeed)){
                Ext.Msg.show({
                    title: 'Ошибка',
                    msg: 'Выбраны не все требуемые опции!',
                    modal: true,
                    icon: Ext.Msg.ERROR,
                    buttons: Ext.Msg.OK
                });

                return;
            }

            var view = this.getView();
            var rm = view.requestManager;
            var settings = view.viewport.settings;
            var data = {};

            if (this.crStep == 1 && !this.returned){
                data.mode = 'first'
            } else {
                data.mode = 'next';
            }

            data.ssid = settings.get('ssid');

            view.up('window').mask(this.nextText);

            switch (this.currentType){
                case 'selectitem':{
                    data.selected = this.selectedItems.join(';');

                    break;
                }

                case 'finish':{
                    var charlist = Ext.getCmp('charlist-tabpanel');

                    if (charlist) charlist.getController().getCharsheet();

                    view.up('window').close();

                    return;
                    break;
                }

                case 'skip':{
                    break;
                }
            }

            rm.go({
                url: 'lvlup.php',
                data: data,
                scope: this,
                success: this.onNextSuccess,
                failure: this.onFailure
            });
        }
    },

    onNextSuccess: function(response){
        var data = Ext.JSON.decode(response.responseText);
        var panel = Ext.getCmp('lvlup-cardpanel');

        this.crStep++;

        panel.add({
            layout: 'border',
            border: false,
            items: [{
                region: 'center',
                id: 'lvlupcard-' + this.crStep,
                border: false,
                bodyCls: 'panel-filling',
                scrollable: true,
                bodyPadding: 10
            },{
                region: 'east',
                width: 300,
                id: 'lvluphelp-' + this.crStep,
                border: false,
                bodyCls: 'panel-filling',
                scrollable: true,
                bodyPadding: 10
            }]
        });

        this.currentType = data.setup.type;
        this.selNeed = data.setup.need;
        this.crCid = data.setup.cid;
        this.help = data.setup.help;
        this.selCount = 0;
        this.selectedItems = new Array();

        if (data.return_setup){
            this.returned = true;
        } else {
            this.returned = false;
        }

        if (data.setup.nohelp){
            this.nohelp = data.setup.nohelp;
        } else {
            this.nohelp = new Array();
        }

        switch (this.currentType){
            case 'selectitem':{
                Ext.getCmp('lvlupcard-' + this.crStep).add({
                    xtype: 'form',
                    id: 'lvlupform_' + this.crStep,
                    border: false,
                    bodyCls: 'panel-filling',
                    scrollable: true,
                    items: [{
                        xtype: 'fieldset',
                        bodyCls: 'itemdescr-panel',
                        width: 440,
                        title: data.setup.title + ' (лимит ' + this.selNeed + ')',
                        id: 'lvlupfset_' + this.crStep
                    }]
                });

                panel.getLayout().setActiveItem(this.crStep);

                Ext.each(data.setup.items, function(item){
                    Ext.getCmp('lvlupfset_' + this.crStep).add({
                        xtype: 'checkbox',
                        boxLabel: item.text,
                        id: 'lvlupchosen_' + this.crStep + '_' + item.id,
                        listeners: {
                            change: this.onItemCheck,
                            scope: this
                        }
                    });
                }, this);

                break;
            }

            case 'skip':{
                Ext.getCmp('lvlupcard-' + this.crStep).add({
                    id: 'lvluppanel_' + this.crStep,
                    border: false,
                    bodyCls: 'itemdescr-panel',
                    scrollable: true,
                    html: '<h3>' + data.setup.title + '</h3>'
                });

                panel.getLayout().setActiveItem(this.crStep);

                break;
            }

            case 'finish':{
                Ext.getCmp('lvlupcard-' + this.crStep).add({
                    id: 'lvluppanel_' + this.crStep,
                    border: false,
                    bodyCls: 'itemdescr-panel',
                    scrollable: true,
                    html: '<h3>Поздравляем, ваш персонаж создан! Нажмите "Далее" для завершения.</h3>'
                });

                panel.getLayout().setActiveItem(this.crStep);

                break;
            }
        }

        this.onSuccess();
    },

    onItemCheck: function(obj, newVal, oldVal){
        var id = obj.getId().split('_')[2];

        if (this.nohelp.indexOf(parseInt(id)) == -1){
            this.getHelp(id);
        } else {
            Ext.getCmp('lvluphelp-' + this.crStep).update('Нету справки по этому объекту.');
        }

        if (newVal){
            if (this.selCount + 1 > this.selNeed){
                obj.setValue(false);
                this.selCount++
                return false;
            } else {
                this.selectedItems.push(id);
                this.selCount++;
            }
        } else {
            Ext.Array.remove(this.selectedItems, id);
            this.selCount--;
        }
    },

    getHelp: function(id){
        var data = {};
        var view = this.getView();
        var rm = view.requestManager;
        var settings = view.viewport.settings;

        data.mode = 'gethelp';
        data.type = this.help;
        data.id = id;
        data.ssid = settings.get('ssid');

        Ext.getCmp('lvluphelp-' + this.crStep).mask(this.loadText);

        rm.go({
            url: 'lvlup.php',
            data: data,
            scope: this,
            success: this.onHelpSuccess,
            failure: this.onFailure
        });
    },

    onHelpSuccess: function(response){
        var data = Ext.JSON.decode(response.responseText);
        var panel = Ext.getCmp('lvluphelp-' + this.crStep);

        switch (this.help){
            case 'powers':{
                var helptxt = data.helpobj[0].descr;
                break;
            }

            case 'feats':{
                var helptxt = data.helpobj[0].descr;
                break;
            }

            case 'spells':{
                var helptxt = data.helpobj[0].lvl + ' уровень, ' + data.helpobj[0].cat + '<br/><br/><b>Время накладывания: </b>' + data.helpobj[0].timing + '<br/><b>Дистанция: </b>' + data.helpobj[0].distance + '<br/><b>Компоненты: </b>' + data.helpobj[0].components + '<br/><b>Длительность: </b>' + data.helpobj[0].duration + '<br/><br/>' + data.helpobj[0].descr;
                break;
            }

            case 'weapons':{
                var helptxt = 'Категория: ' + data.helpobj[0].cat + '<br/>Урон: ' + data.helpobj[0].dice + '<br/>Тип урона: ' + data.helpobj[0].dmg_type + '<br/>Свойства: ' + data.helpobj[0].properties;
                break;
            }

            case 'armor':{
                var helptxt = 'Категория: ' + data.helpobj[0].cat + '<br/>КД: ' + data.helpobj[0].ac + '<br/>Штраф скрытности: ' + data.helpobj[0].stealth;
                break;
            }

            case 'equip':{
                var helptxt = data.helpobj[0].descr;
                break;
            }

            default:{
                var helptxt = 'Нету справки по этому объекту.';
            }
        }

        panel.update(helptxt);

        panel.unmask();
    }
    
});
