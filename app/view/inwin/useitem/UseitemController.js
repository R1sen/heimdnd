Ext.define('DwD2.view.inwin.useitem.UseitemController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.inwin-useitem-useitem',

    useText: 'Используем...',

    onComboChange: function(cmb, newV, oldV, opts){
        if (newV){
            this.lookupReference('give_to_combo').enable();
        } else {
            this.lookupReference('give_to_combo').disable();
        }
    },

    onUseItem: function(){
        var form = this.getView();

        if (form.isValid()){
            var data = form.getValues();
            var rm = form.requestManager;
            var settings = form.viewport.settings;

            data.ssid = settings.get('ssid');
            data.iid = form.useItem;

            form.up('window').mask(this.useText);

            rm.go({
                url: 'charaction.php',
                data: data,
                scope: this,
                success: this.onUseSuccess,
                failure: this.onUseFailure
            });
        }
    },

    onUseFailure: function(){
        var window = this.getView().up('window');

        window.unmask();
    },

    onUseSuccess: function(response){
        var window = this.getView().up('window');

        window.unmask();

        Ext.getCmp('charlist-tabpanel').getController().getCharsheet();

        window.close();
    }
});
