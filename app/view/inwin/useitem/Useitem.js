
Ext.define('DwD2.view.inwin.useitem.Useitem',{
    extend: 'Ext.form.Panel',

    requires: [
        'DwD2.view.inwin.useitem.UseitemController',
        'DwD2.view.inwin.useitem.UseitemModel'
    ],

    controller: 'inwin-useitem-useitem',
    viewModel: {
        type: 'inwin-useitem-useitem'
    },

    frame: true,
    xtype: 'useitem',
    layout: 'anchor',
    defaults: {
        anchor: '100%'
    },
    bodyPadding: 10,

    items: [{
        xtype: 'radio',
        boxLabel: 'Использовать',
        name: 'mode',
        inputValue: 'takeon',
        checked: true
    },{
        xtype: 'radio',
        boxLabel: 'Передать',
        name: 'mode',
        inputValue: 'giveto',
        listeners: {
            change: 'onComboChange',
            scope: 'controller'
        }
    },{
        xtype: 'combo',
        disabled: true,
        hideLabel: true,
        editable: false,
        emptyText: 'Передать кому..',
        queryMode: 'local',
        store: 'charsInLocRaw',
        displayField: 'name',
        name: 'give_to',
        valueField: 'id',
        triggerAction: 'all',
        reference: 'give_to_combo'
    },{
        xtype: 'radio',
        boxLabel: 'Выбросить',
        name: 'mode',
        inputValue: 'throwaway'
    }],

    buttons: [{
        text: 'Выбрать',
        handler: 'onUseItem'
    }]
});
