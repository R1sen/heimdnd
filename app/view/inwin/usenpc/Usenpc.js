
Ext.define('DwD2.view.inwin.usenpc.Usenpc',{
    extend: 'Ext.form.Panel',

    requires: [
        'DwD2.view.inwin.usenpc.UsenpcController',
        'DwD2.view.inwin.usenpc.UsenpcModel'
    ],

    controller: 'inwin-usenpc-usenpc',
    viewModel: {
        type: 'inwin-usenpc-usenpc'
    },

    frame: true,
    xtype: 'usenpc',
    layout: 'anchor',
    defaults: {
        anchor: '100%',
        labelWidth: 50
    },
    bodyPadding: 10,

    items: [{
        xtype: 'combobox',
        fieldLabel: 'Куда',
        editable: false,
        emptyText: 'Выберите локацию..',
        queryMode: 'local',
        store: 'locsStore',
        displayField: 'name',
        name: 'lid',
        valueField: 'id',
        triggerAction: 'all',
        reference: 'hand_combo'
    },{
        xtype: 'textfield',
        name: 'name',
        reference: 'name_textfield',
        fieldLabel: 'Имя',
        allowBlank: false,
        regex: /^[0-9А-Яа-я\s\.Ёё\-]{1,19}$/,
        regexText: 'Допускается только длина строки меньше 20 символов.',
        enableKeyEvents: true,
        bind: {
            value: '{mob}'
        },
        listeners: {
            specialKey: 'onSpecialKey'
        }
    },{
        xtype: 'textfield',
        name: 'hp',
        reference: 'hp_textfield',
        fieldLabel: 'ХП',
        bind: {
            value: '{hp}'
        },
        regex: /^\d{1,3}$|^\d{1,2}(d|к|д)\d{1,3}\s((\+|\-)\s\d{1,2})?$/,
        regexText: 'Допускается только целое число, не большее 999 или формула кубика (например: 1d10 + 5).',
        allowBlank: false,
        enableKeyEvents: true,
        listeners: {
            specialKey: 'onSpecialKey'
        }
    }],

    buttons: [{
        text: 'Заселить',
        formBind: true,
        disabled: true,
        handler: 'onSettle'
    }],

    onRender: function(){
        this.callParent();

        this.getController().setInits();
    }
});
