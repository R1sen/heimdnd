Ext.define('DwD2.view.inwin.usenpc.UsenpcController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.inwin-usenpc-usenpc',

    settleText: 'Заселяем...',

    setInits: function(){
        var view = this.getView();

        this.getViewModel().setData({hp: view.useHp, mob: view.useName});
    },

    onSpecialKey: function(field, e) {
        if (e.getKey() === e.ENTER) {
            this.onSettle();
        }
    },

    onSettle: function(){
        var form = this.getView();

        if (form.isValid()){
            var data = form.getValues();
            var rm = form.requestManager;
            var settings = form.viewport.settings;

            data.ssid = settings.get('ssid');
            data.mode = 'settle';
            data.nid = form.useId;

            form.up('window').mask(this.settleText);

            rm.go({
                url: 'dm.php',
                data: data,
                scope: this,
                success: this.onSettleSuccess,
                failure: this.onFailure
            });
        }
    },

    onFailure: function(){
        var window = this.getView().up('window');

        window.unmask();
    },

    onSettleSuccess: function(response){
        var window = this.getView().up('window');

        window.unmask();
        window.close();
    }
    
});
