Ext.define('DwD2.view.inwin.echo.EchoController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.inwin-echo-echo',

    getEchoText: 'Забираем эхо...',
    rotate: false,
    autoRun: false,

    startEchoTask: function(){
        if (!this.echoTask){
            this.echoTask = {
                run: this.getEcho,
                interval: 20000, //20 seconds
                scope: this
            };
        }

        Ext.TaskManager.start(this.echoTask);
    },

    stopEchoTask: function(){
        Ext.TaskManager.stop(this.echoTask);
    },

    onAutoRefresh: function(box, checked){
        if (checked){
            this.startEchoTask();
            this.autoRun = true;
        } else {
            this.stopEchoTask();
            this.autoRun = false;
        }
    },

    onDestroy: function(){
        if (this.autoRun){
            this.stopEchoTask();
            this.autoRun = false;
        }
    },

    getEcho: function(field, value){
        var view = this.getView();
        var rm = view.requestManager;
        var settings = view.viewport.settings;
        var data = {
            ssid: settings.get('ssid')
        };

        if (!value){
			var dt = this.lookupReference('echo_date').getValue();
			var dt2 = new Date(dt.getFullYear(), dt.getMonth(), dt.getDate());
            data.date = Ext.Date.format(dt2, 'timestamp');
        } else {
            data.date = value.getTime() / 1000;
		}

        data.lid = this.lookupReference('echo_locs_list').getValue();

        view.up('window').mask(this.getEchoText);

        rm.go({
            url: 'echo.php',
            data: data,
            scope: this,
            success: this.onGetSuccess,
            failure: this.onFailure
        });
    },

    getLocs: function(){
        var view = this.getView();
        var rm = view.requestManager;
        var settings = view.viewport.settings;
        var data = {
            ssid: settings.get('ssid'),
            mode: 'getlocs'
        };

        if (settings.get('dm') == 1){
            rm.go({
                url: 'dm.php',
                data: data,
                scope: this,
                success: this.onGetLocsSuccess,
                failure: this.onFailure
            });
        }
    },

    onGetLocsSuccess: function(response){
        var data = Ext.JSON.decode(response.responseText);
        var combo = this.lookupReference('echo_locs_list');
        var autobox = this.lookupReference('echo_auto_refresh');

        Ext.getStore('locsStore').loadRawData(data);

        combo.enable();
        autobox.enable();
        combo.setValue(data.lid);
    },

    onLocSelect: function(combo, record){
        this.getEcho();
    },

    onFailure: function(){
        var window = this.getView().up('window');

        window.unmask();
    },

    onGetSuccess: function(response){
        this.echo = Ext.JSON.decode(response.responseText);
        var beacon = this.getView();
        var window = beacon.up('window');

        window.unmask();

        if (this.rotate)
            this.onRotate();
        else
            beacon.setData(this.echo);
    },

    setRotate: function(){
        this.rotate = !this.rotate;

        this.onRotate();
    },
    
    onRotate: function(){
        var newmess = new Array();

        for (var i = this.echo.mess.length; i > 0; i--){
            newmess.push(this.echo.mess[i-1]);
        }

        var newecho = {"success": this.echo.success, "mess": newmess};
        var beacon = this.getView();

        Ext.get('echo-panel-innerCt').update('');

        this.echo = newecho;

        beacon.setData(this.echo);

        beacon.getScrollable().scrollBy(null, 100000, {duration: 2000});
    }
    
});
