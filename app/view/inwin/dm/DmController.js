Ext.define('DwD2.view.inwin.dm.DmController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.inwin-dm-dm',

    dmText: 'Загружаем...',
    botText: 'Высказываем...',
    hpText: 'Снимаем...',
    attackText: 'Атакуем...',
    damageText: 'Наносим урон...',
    useSpellText: 'Высвечиваем в чат...',
    inspirationText: 'Вдохновляем...',
    XPText: 'Начисляем...',
    selectedNPC: {},

    onColorSelect: function (obj, color) {
        this.lookupReference('dm_char_color').setValue('#' + color);
    },

    onSpecialKey: function (field, e) {
        if (e.getKey() === e.ENTER && e.ctrlKey) {
            this.sendBotMessage();
        }
    },

    sendBotMessage: function () {
        var view = this.getView();
        var rm = view.requestManager;
        var settings = view.viewport.settings;
        var data = {
            ssid: settings.get('ssid'),
            mode: 'bot',
            bot_name: this.lookupReference('bot_name').getValue(),
            bot_color: this.lookupReference('dm_char_color').getValue(),
            bot_loc: this.lookupReference('dm_locs_list').getValue(),
            bot_text: this.lookupReference('bot_to_send').getValue()
        };

        view.up('window').mask(this.botText);

        rm.go({
            url: 'dm.php',
            data: data,
            scope: this,
            success: this.onBotSuccess,
            failure: this.onFailure
        });
    },

    sendShoutMessage: function () {
        var view = this.getView();
        var rm = view.requestManager;
        var settings = view.viewport.settings;
        var data = {
            ssid: settings.get('ssid'),
            mode: 'shout',
            shout_name: this.lookupReference('shout_name').getValue(),
            shout_color: this.lookupReference('dm_char_color').getValue(),
            shout_text: this.lookupReference('shout_to_send').getValue()
        };

        view.up('window').mask(this.botText);

        rm.go({
            url: 'dm.php',
            data: data,
            scope: this,
            success: this.onShoutSuccess,
            failure: this.onFailure
        });
    },

    makeHP: function () {
        var view = this.getView();
        var rm = view.requestManager;
        var settings = view.viewport.settings;
        var data = {
            ssid: settings.get('ssid'),
            mode: 'hp',
            hp_mod: this.lookupReference('hp_mod').getValue(),
            hp_combo: this.lookupReference('hp_combo').getValue(),
            hp_reason: this.lookupReference('hp_reason').getValue(),
            temp_hp_mod: this.lookupReference('temp_hp_mod').getValue()
        };

        view.up('window').mask(this.hpText);

        rm.go({
            url: 'dm.php',
            data: data,
            scope: this,
            success: this.onSuccess,
            failure: this.onFailure
        });
    },

    onAttack: function () {
        var view = this.getView();
        var rm = view.requestManager;
        var settings = view.viewport.settings;
        var data = {
            ssid: settings.get('ssid'),
            mode: 'attack',
            attack_mod: this.lookupReference('attack_mod').getValue(),
            attack_combo: this.lookupReference('attack_combo').getValue(),
            attack_name: this.lookupReference('attack_name').getValue(),
            attack_diff: this.lookupReference('attack_diff').getValue(),
            adv: this.lookupReference('attack_advantage').getValue(),
            disadv: this.lookupReference('attack_disadvantage').getValue()
        };

        view.up('window').mask(this.attackText);

        rm.go({
            url: 'dm.php',
            data: data,
            scope: this,
            success: this.onSuccess,
            failure: this.onFailure
        });
    },

    onDamage: function () {
        var view = this.getView();
        var rm = view.requestManager;
        var settings = view.viewport.settings;
        var data = {
            ssid: settings.get('ssid'),
            mode: 'damage',
            damage_combo: this.lookupReference('damage_combo').getValue(),
            damage_name: this.lookupReference('damage_name').getValue(),
            damage_formula: this.lookupReference('damage_formula').getValue()
        };

        view.up('window').mask(this.damageText);

        rm.go({
            url: 'dm.php',
            data: data,
            scope: this,
            success: this.onSuccess,
            failure: this.onFailure
        });
    },

    makeInspiration: function () {
        var view = this.getView();
        var rm = view.requestManager;
        var settings = view.viewport.settings;
        var data = {
            ssid: settings.get('ssid'),
            mode: 'inspiration',
            inspiration_combo: this.lookupReference('inspiration_combo').getValue()
        };

        view.up('window').mask(this.inspirationText);

        rm.go({
            url: 'dm.php',
            data: data,
            scope: this,
            success: this.onSuccess,
            failure: this.onFailure
        });
    },

    makeXP: function () {
        var view = this.getView();
        var rm = view.requestManager;
        var settings = view.viewport.settings;
        var data = {
            ssid: settings.get('ssid'),
            mode: 'xp',
            xp_combo: this.lookupReference('xp_combo').getValue(),
            xp_reason: this.lookupReference('xp_reason').getValue(),
            xp_mod: this.lookupReference('xp_mod').getValue()
        };

        view.up('window').mask(this.XPText);

        rm.go({
            url: 'dm.php',
            data: data,
            scope: this,
            success: this.onSuccess,
            failure: this.onFailure
        });
    },

    getDm: function () {
        var view = this.getView();
        var rm = view.requestManager;
        var settings = view.viewport.settings;
        var data = {
            ssid: settings.get('ssid'),
            mode: 'getdm'
        };

        view.up('window').mask(this.dmText);

        rm.go({
            url: 'dm.php',
            data: data,
            scope: this,
            success: this.onGetDm,
            failure: this.onFailure
        });
    },

    onGetDm: function (response) {
        var data = Ext.JSON.decode(response.responseText);
        var combo = this.lookupReference('dm_locs_list');
        var npc_loclist = this.lookupReference('npc_loclist');
        var allchars_panel = this.lookupReference('dm-allchars');

        Ext.getStore('locsStore').loadRawData(data);
        Ext.getStore('charsInLocRaw').loadRawData(data);

        combo.setValue(data.lid);

        Ext.getStore('npcTree').setRoot(data.npc);
        Ext.getStore('allSpellsTree').setRoot(data.spells);

        npc_loclist.removeAll();

        if (data.npc_loclist) {
            Ext.each(data.npc_loclist, function (item) {
                this.addMMs(item);
            }, this);
        }

        allchars_panel.setData(data);

        this.onSuccess();
    },

    addMMs: function (item) {
        var npc_loclist = this.lookupReference('npc_loclist');
        var mlist = '';

        Ext.each(item.mms, function (mm) {
            mlist += mm.mname + ' <a onclick="Ext.getCmp(\'dm-tabpanel\').fireEvent(\'unsettle\', ' + mm.id + ')">[x]</a><br/>';
        }, this);

        npc_loclist.add({
            xtype: 'fieldset',
            width: 300,
            title: item.lname,
            html: mlist
        })
    },

    onSpellSelect: function (obj, rec, index, opt) {
        var val = rec.getData();
        var panel = this.lookupReference('spelldescr-panel');

        if (!val.leaf) {
            return;
        }

        this.lookupReference('usespell_button_dm').enable();

        var view = this.getView();
        var rm = view.requestManager;
        var settings = view.viewport.settings;
        var data = {
            ssid: settings.get('ssid'),
            sid: val.sid,
            mode: 'getspell'
        };

        panel.mask(this.dmText);

        rm.go({
            url: 'dm.php',
            data: data,
            scope: this,
            success: this.onGetSpell,
            failure: this.onFailure
        });
    },

    onGetSpell: function (response) {
        var panel = this.lookupReference('spelldescr-panel');
        var data = Ext.JSON.decode(response.responseText);

        var attack_regex = /(спасброс(?:ок|ке|ки))\s(Силы|Телосложения|Ловкости|Интеллекта|Мудрости|Харизмы)|(рукопашную)\s(атаку\sзаклинанием)|(дальнобойную)\s(атаку\sзаклинанием)/gi;
        var damage_regex = /(колющий|дробящий|рубящий\s)?(урон\s)?(?:(огнём|звуком|излучением|кислотой|некротической\sэнергией|психической\sэнергией|силовым\sполем|холодом|электричеством|ядом|излучением\sи\sогнём)\s)?((?:\d{1,2})?[dкд]{1}\d{1,2})/gi;

        /*var descr = data.spell.descr.replace(attack_regex, "<a onclick=\"Ext.getCmp('charlist-tabpanel').fireEvent('spellattack', '$&', " + data.spell.id + ")\">$&</a>");
         descr = descr.replace(damage_regex, "<a onclick=\"Ext.getCmp('charlist-tabpanel').fireEvent('spelldamage', '$&', " + data.spell.id + ")\">$&</a>");*/

        var txt = '<b>' + data.spell.name + '</b><br/><i>' + data.spell.lvl + ' уровень, ' + data.spell.cat + '</i><br/><br/><b>Время накладывания:</b> ' + data.spell.timing + '<br/><b>Дистанция:</b> ' + data.spell.distance + '<br/><span data-qtip="В - вербальный, С - соматический, М - материальный"><b>Компоненты:</b> ' + data.spell.components + '</span><br/><b>Длительность:</b> ' + data.spell.duration + '<br/><br/>' + data.spell.descr;

        panel.update(txt);
        panel.unmask();
    },

    useSpell_dm: function () {

        var rec = this.lookupReference('spell-dm-tree').getSelection();

        var folder = rec[0].get('cls');

        if (folder == 'folder') {
            return;
        }

        var view = this.getView();
        var rm = view.requestManager;
        var settings = view.viewport.settings;
        var data = {
            ssid: settings.get('ssid'),
            spell_id: rec[0].get('sid'),
            mode: 'usespell'
        }

        view.up('window').mask(this.useSpellText);

        rm.go({
            url: 'dm.php',
            data: data,
            scope: this,
            success: this.onSuccess,
            failure: this.onFailure
        });
    },

    onNPCSelect: function (obj, rec, index, opt) {
        var val = rec.getData();
        var panel = this.lookupReference('npcdescr-panel');

        if (!val.leaf) {
            this.selectedNPC.id = 0;
            this.lookupReference('usenpc_button').disable();
            return;
        } else {
            this.selectedNPC.id = val.id;
            this.lookupReference('usenpc_button').enable();
        }

        var view = this.getView();
        var rm = view.requestManager;
        var settings = view.viewport.settings;
        var data = {
            ssid: settings.get('ssid'),
            nid: val.id,
            mode: 'getnpc'
        };

        panel.mask(this.dmText);

        rm.go({
            url: 'dm.php',
            data: data,
            scope: this,
            success: this.onGetNPC,
            failure: this.onFailure
        });
    },

    onGetNPC: function (response) {
        var panel = this.lookupReference('npcdescr-panel');
        var data = Ext.JSON.decode(response.responseText);

        panel.unmask();

        var attack_regex = /\+\d{1,2}\sк\sатаке/gi;
        var saves_regex = /(Сил|Ловк|Тел|Инт|Мудр|Хар)\s(\+\d{1,2})/gi;
        var skills_regex = /(Атлетика|Внимательность|Выживание|Медицина|Проницательность|Уход\sза\sживотными|Акробатика|Ловкость\sрук|Скрытность|Анализ|История|Магия|Природа|Религия|Выступление|Запугивание|Обман|Убеждение|Аркана)\s(\+\d{1,2})/gi;
        var damage_regex = /\d{1,2}\s\(\d{1,2}[dкд]{1}\d{1,2}(\s?[\+\-]{1}\s?\d{1,2})?(\sплюс\s\d{1,2}[dкд]{1}\d{1,2})?\)(?:(?:\s(рубящего|колющего|дробящего|физического))?\sурона(?:\s(огнем|звуком|излучением|кислотой|некротической\sэнергией|психической\sэнергией|силовым\sполем|холодом|электричеством|ядом|излучением\sи\sогнем))?)?/gi;

        var actions = data.npc.actions.replace(attack_regex, "<a onclick=\"Ext.getCmp('dm-tabpanel').fireEvent('npcattack', '$&', " + data.npc.id + ")\">$&</a>");
        actions = actions.replace(damage_regex, "<a onclick=\"Ext.getCmp('dm-tabpanel').fireEvent('npcdamage', '$&', " + data.npc.id + ")\">$&</a>");

        var traits = data.npc.traits.replace(damage_regex, "<a onclick=\"Ext.getCmp('dm-tabpanel').fireEvent('npcdamage', '$&', " + data.npc.id + ")\">$&</a>");
        traits = traits.replace(attack_regex, "<a onclick=\"Ext.getCmp('dm-tabpanel').fireEvent('npcattack', '$&', " + data.npc.id + ")\">$&</a>");

        var legendary = data.npc.legendary.replace(damage_regex, "<a onclick=\"Ext.getCmp('dm-tabpanel').fireEvent('npcdamage', '$&', " + data.npc.id + ")\">$&</a>");

        var str_check = '<a onclick="Ext.getCmp(\'dm-tabpanel\').fireEvent(\'npccheck\', \'Сила\', \'' + this.getMod(data.npc.str) + '\')">' + this.getMod(data.npc.str) + '</a>';
        var dex_check = '<a onclick="Ext.getCmp(\'dm-tabpanel\').fireEvent(\'npccheck\', \'Ловкость\', \'' + this.getMod(data.npc.dex) + '\')">' + this.getMod(data.npc.dex) + '</a>';
        var con_check = '<a onclick="Ext.getCmp(\'dm-tabpanel\').fireEvent(\'npccheck\', \'Телосложение\', \'' + this.getMod(data.npc.con) + '\')">' + this.getMod(data.npc.con) + '</a>';
        var intl_check = '<a onclick="Ext.getCmp(\'dm-tabpanel\').fireEvent(\'npccheck\', \'Интеллект\', \'' + this.getMod(data.npc.intl) + '\')">' + this.getMod(data.npc.intl) + '</a>';
        var wis_check = '<a onclick="Ext.getCmp(\'dm-tabpanel\').fireEvent(\'npccheck\', \'Мудрость\', \'' + this.getMod(data.npc.wis) + '\')">' + this.getMod(data.npc.wis) + '</a>';
        var cha_check = '<a onclick="Ext.getCmp(\'dm-tabpanel\').fireEvent(\'npccheck\', \'Харизма\', \'' + this.getMod(data.npc.cha) + '\')">' + this.getMod(data.npc.cha) + '</a>';

        var saves = data.npc.saves.replace(saves_regex, "<a onclick=\"Ext.getCmp('dm-tabpanel').fireEvent('npccheck', '$1', '$2')\">$&</a>");

        var skills = data.npc.skills.replace(skills_regex, "<a onclick=\"Ext.getCmp('dm-tabpanel').fireEvent('npccheck', '$1', '$2')\">$&</a>");

        var txt = '<h3>' + data.npc.name + '</h3><i>' + data.npc.type + '</i><br/><hr/><b>КД:</b> ' + data.npc.ac + '<br/><b>ХП:</b> ' + data.npc.hp + '<br/><b>Скорость:</b> ' + data.npc.speed + '<br/><hr/><table cellspacing="0" cellpadding="0" class="chartable"><tr><td><b>СИЛ</b></td><td><b>ЛОВ</b></td><td><b>ТЕЛ</b></td><td><b>ИНТ</b></td><td><b>МУД</b></td><td><b>ХАР</b></td></tr><tr><td>' + data.npc.str + ' (' + str_check + ')</td><td>' + data.npc.dex + ' (' + dex_check + ')</td><td>' + data.npc.con + ' (' + con_check + ')</td><td>' + data.npc.intl + ' (' + intl_check + ')</td><td>' + data.npc.wis + ' (' + wis_check + ')</td><td>' + data.npc.cha + ' (' + cha_check + ')</td></tr></table><b>Спасброски:</b> ' + saves + '<br/><hr/><b>Навыки:</b> ' + skills + '<br/><b>Сопротивления:</b> ' + data.npc.resist + '<br/><b>Уязвимости:</b> ' + data.npc.vulnerab + '<br/><b>Иммунитет к урону:</b> ' + data.npc.dmg_immune + '<br/><b>Иммунитет к состояниям:</b> ' + data.npc.cond_immune + '<br/><b>Чувства:</b> ' + data.npc.senses + '<br/><b>Языки:</b> ' + data.npc.lang + '<br/><b>Уровень угрозы:</b> ' + data.npc.challenge + ' (' + data.npc.xp + ' опыта)<br/><hr/><b>Особенности:</b><br/>' + this.checkEmpty(traits) + '<h3>Действия</h3>' + this.checkEmpty(actions) + '<h3>Легендарные действия</h3>' + this.checkEmpty(legendary) + '<h3>Реакции</h3>' + this.checkEmpty(data.npc.reactions);

        panel.update(txt);

        this.selectedNPC.hp = data.npc.hp;
        this.selectedNPC.mob = data.npc.name;
    },

    getMod: function (val) {
        var mod = Math.floor((val - 10) / 2);

        if (mod >= 0) {
            mod = '+' + mod;
        }

        return mod;
    },

    useNPC: function () {
        this.fireViewEvent('usenpc', this.selectedNPC);
    },

    checkEmpty: function (val) {
        if (val == '') return '-';
        else return val;
    },

    onFailure: function () {
        var window = this.getView().up('window');

        window.unmask();
    },

    onBotSuccess: function () {
        this.lookupReference('bot_to_send').setValue('');

        this.onSuccess();
    },

    onShoutSuccess: function () {
        this.lookupReference('shout_to_send').setValue('');

        this.onSuccess();
    },

    onSuccess: function () {
        var window = this.getView().up('window');

        window.unmask();
    }

});
