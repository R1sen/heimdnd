
Ext.define('DwD2.view.inwin.dm.Dm',{
    extend: 'Ext.tab.Panel',

    requires: [
        'DwD2.view.inwin.dm.DmController',
        'DwD2.view.inwin.dm.DmModel'
    ],

    controller: 'inwin-dm-dm',
    viewModel: {
        type: 'inwin-dm-dm'
    },

    id: 'dm-tabpanel',
    xtype: 'dm',

    items: [{
        scrollable: true,
        title: 'Общие',
        tooltip: 'Общие функции мастера',
        frame: true,
        items: [{
            xtype: 'fieldset',
            title: 'Цвет сообщений',
            collapsible: true,
            collapsed: true,
            listeners: {
                expand: function(){
                    if (!this.cpRendered) {
                        var cp = new Ext.ColorPalette({
                            cls: 'custom-color-palette',
                            listeners: {
                                select: Ext.getCmp('dm-tabpanel').getController().onColorSelect,
                                scope: Ext.getCmp('dm-tabpanel').getController()
                            }
                        });

                        cp.colors = Ext.getCmp('main-viewport').safeColors;
                        cp.render('dm-colorpalette-div');
                        this.cpRendered = true;
                    }
                },
                beforedestroy: function(){
                    this.cpRendered = false;
                },
                scope: this
            },
            items: [{
                fieldLabel: 'Палитра',
                xtype: 'field',
                submitValue: false,
                fieldSubTpl: '<div id="dm-colorpalette-div"></div>'
            },{
                fieldLabel: 'HEX-цвет',
                name: 'dm_color',
                xtype: 'textfield',
                width: 200,
                allowEmpty: false,
                regex: /^#[A-Fa-f0-9]{6}$/,
                regexText: 'Неверный формат HEX-цвета',
                reference: 'dm_char_color',
                value: '#ADB801'
            }]
        },{
            xtype: 'fieldset',
            title: 'Бот-строка',
            collapsible: true,
            collapsed: true,
            items: [{
                fieldLabel: 'Имя',
                xtype: 'textfield',
                value: 'ДМ',
                reference: 'bot_name',
                width: 300
            },{
                xtype: 'combo',
                editable: false,
                width: 300,
                emptyText: 'В локацию..',
                queryMode: 'local',
                store: 'locsStore',
                displayField: 'name',
                reference: 'dm_locs_list',
                valueField: 'id',
                fieldLabel: 'Локация',
                triggerAction: 'all'
            },{
                xtype: 'textarea',
                hideLabel: true,
                allowBlank: false,
                height: 100,
                anchor: '99%',
                reference: 'bot_to_send',
                listeners: {
                    specialKey: 'onSpecialKey'
                }
            },{
                xtype: 'button',
                hideLabel: true,
                text: 'Высказать',
                handler: 'sendBotMessage'
            }]
        },{
            xtype: 'fieldset',
            title: 'По всем локациям',
            collapsible: true,
            collapsed: true,
            items: [{
                fieldLabel: 'Имя',
                xtype: 'textfield',
                value: 'ДМ',
                reference: 'shout_name',
                width: 300
            },{
                xtype: 'textarea',
                hideLabel: true,
                allowBlank: false,
                anchor: '99%',
                reference: 'shout_to_send'
            },{
                xtype: 'button',
                hideLabel: true,
                text: 'По всем локациям',
                handler: 'sendShoutMessage'
            }]
        },{
            xtype: 'fieldset',
            title: 'Атака',
            collapsible: true,
            collapsed: true,
            items: [{
                fieldLabel: 'Кто атакует',
                xtype: 'textfield',
                reference: 'attack_name',
                width: 300
            },{
                fieldLabel: 'Модификатор',
                xtype: 'textfield',
                value: 0,
                reference: 'attack_mod',
                regex: /^(\+|\-)?\d{1,2}$/,
                regexText: 'Неверное число',
                width: 300
            },{
                fieldLabel: 'Сложность',
                xtype: 'textfield',
                value: 0,
                reference: 'attack_diff',
                regex: /^\d{1,2}$/,
                regexText: 'Неверное число',
                width: 300
            },{
                xtype: 'checkboxfield',
                reference: 'attack_advantage',
                fieldLabel: 'Преимущество',
                inputValue: '1'
            },{
                xtype: 'checkboxfield',
                reference: 'attack_disadvantage',
                fieldLabel: 'Помеха',
                inputValue: '1'
            },{
                xtype: 'combobox',
                fieldLabel: 'Кого',
                editable: false,
                emptyText: 'Выберите цель..',
                queryMode: 'local',
                store: 'allTargets',
                displayField: 'name',
                valueField: 'id',
                triggerAction: 'all',
                reference: 'attack_combo',
                width: 300
            },{
                xtype: 'button',
                hideLabel: true,
                text: 'Атаковать',
                handler: 'onAttack'
            }]
        },{
            xtype: 'fieldset',
            title: 'Урон',
            collapsible: true,
            collapsed: true,
            items: [{
                fieldLabel: 'Кто наносит',
                xtype: 'textfield',
                reference: 'damage_name',
                width: 300
            },{
                fieldLabel: 'Формула',
                xtype: 'textfield',
                value: 0,
                reference: 'damage_formula',
                regex: /^\d{1,2}(d|к|д)\d{1,3}((\+|\-)\d{1,2})?$/,
                regexText: 'Неверная формула кубика',
                width: 300
            },{
                xtype: 'combobox',
                fieldLabel: 'Кому',
                editable: false,
                emptyText: 'Выберите цель..',
                queryMode: 'local',
                store: 'allTargets',
                displayField: 'name',
                valueField: 'id',
                triggerAction: 'all',
                reference: 'damage_combo',
                width: 300
            },{
                xtype: 'button',
                hideLabel: true,
                text: 'Нанести урон',
                handler: 'onDamage'
            }]
        },{
            xtype: 'fieldset',
            title: 'Операции с ХП',
            collapsible: true,
            collapsed: true,
            items: [{
                fieldLabel: 'Снять ХП',
                xtype: 'textfield',
                value: 0,
                reference: 'hp_mod',
                regex: /^(\+|\-)?\d{1,2}$/,
                regexText: 'Неверное число',
                width: 300
            },{
                fieldLabel: '+ Вр. ХП',
                xtype: 'textfield',
                value: 0,
                reference: 'temp_hp_mod',
                regex: /^(\+|\-)?\d{1,2}$/,
                regexText: 'Неверное число',
                width: 300
            },{
                fieldLabel: 'Причина',
                xtype: 'textfield',
                reference: 'hp_reason',
                width: 300
            },{
                xtype: 'combobox',
                fieldLabel: 'У кого',
                editable: false,
                emptyText: 'Выберите цель..',
                queryMode: 'local',
                store: 'allTargets',
                displayField: 'name',
                valueField: 'id',
                triggerAction: 'all',
                reference: 'hp_combo',
                width: 300
            },{
                xtype: 'button',
                hideLabel: true,
                text: 'Снять',
                handler: 'makeHP'
            }]
        },{
            xtype: 'fieldset',
            title: 'Удаление монстров из локаций',
            collapsible: true,
            collapsed: true,
            reference: 'npc_loclist'
        },{
            xtype: 'fieldset',
            title: 'Начисление опыта',
            collapsible: true,
            collapsed: true,
            items: [{
                fieldLabel: 'Сколько',
                xtype: 'textfield',
                value: 0,
                reference: 'xp_mod',
                regex: /^(\-)?\d{1,6}$/,
                regexText: 'Неверное число',
                width: 300
            },{
                fieldLabel: 'Причина',
                xtype: 'textfield',
                reference: 'xp_reason',
                width: 300
            },{
                xtype: 'combobox',
                fieldLabel: 'Кому',
                editable: true,
                emptyText: 'Выберите цель..',
                queryMode: 'local',
                store: 'charsInLocRaw',
                displayField: 'name',
                valueField: 'id',
                triggerAction: 'all',
                reference: 'xp_combo',
                typeAhead: true,
                width: 300
            },{
                xtype: 'button',
                hideLabel: true,
                text: 'Начислить',
                handler: 'makeXP'
            }]
        },{
            xtype: 'fieldset',
            title: 'Выдача вдохновения',
            collapsible: true,
            collapsed: true,
            items: [{
                xtype: 'combobox',
                fieldLabel: 'Кому',
                editable: false,
                emptyText: 'Выберите цель..',
                queryMode: 'local',
                store: 'charsInLocRaw',
                displayField: 'name',
                valueField: 'id',
                triggerAction: 'all',
                reference: 'inspiration_combo',
                width: 300
            },{
                xtype: 'button',
                hideLabel: true,
                text: 'Вдохновить',
                handler: 'makeInspiration'
            }]
        }]
    },{
        title: 'NPC',
        tooltip: 'Работа с монстрами',
        reference: 'dm-npcpanel',

        layout: 'border',
        items: [{
            region: 'west',
            width: 275,
            xtype: 'treepanel',
            store: 'npcTree',
            rootVisible: false,
            useArrows: true,
            frame: true,
            title: 'Монстрятник',
            bufferedRenderer: false,
            animate: true,
            listeners: {
                select: 'onNPCSelect',
                scope: 'controller'
            }
        },{
            scrollable: true,
            region: 'center',
            reference: 'npcdescr-panel',
            bodyCls: 'itemdescr-panel',
            title: 'Описание монстра',
            tbar: [{
                text: 'Заселить',
                tooltip: 'Заселить монстра в локацию',
                disabled: true,
                reference: 'usenpc_button',
                handler: 'useNPC'
            }]
        }]
    },{
        title: 'Заклинания',
        tooltip: 'Работа с заклинаниями',
        reference: 'dm-spellspanel',

        layout: 'border',
        items: [{
            region: 'west',
            width: 275,
            xtype: 'treepanel',
            store: 'allSpellsTree',
            reference: 'spell-dm-tree',
            rootVisible: false,
            useArrows: true,
            frame: true,
            title: 'Заклинания',
            bufferedRenderer: false,
            animate: true,
            listeners: {
                select: 'onSpellSelect',
                scope: 'controller'
            }
        },{
            scrollable: true,
            region: 'center',
            reference: 'spelldescr-panel',
            bodyCls: 'itemdescr-panel',
            title: 'Описание заклинания',
            tbar: [{
                text: 'Высветить',
                tooltip: 'Высветить в чат заклинание',
                disabled: true,
                reference: 'usespell_button_dm',
                handler: 'useSpell_dm'
            }]
        }]
    },{
        title: 'Персонажи',
        tooltip: 'Список всех персонажей в игре',
        reference: 'dm-allchars',
        scrollable: true,
        bodyPadding: 10,
        frame: true,
        tpl: new Ext.XTemplate(
            '<tpl for="allchars">',
            '<a onclick="Ext.getCmp(\'main-viewport\').fireEvent(\'showinfo\', {id})"><b>{name}</b></a><br/>',
            '</tpl>'
        )
    }],

    onRender: function(){
        this.callParent();

        this.getController().getDm();
    }
});
