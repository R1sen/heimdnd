
Ext.define('DwD2.view.inwin.makeattack.Makeattack',{
    extend: 'Ext.form.Panel',

    requires: [
        'DwD2.view.inwin.makeattack.MakeattackController',
        'DwD2.view.inwin.makeattack.MakeattackModel'
    ],

    controller: 'inwin-makeattack-makeattack',
    viewModel: {
        type: 'inwin-makeattack-makeattack'
    },

    frame: true,
    xtype: 'makeattack',
    layout: 'anchor',
    defaults: {
        anchor: '100%',
        labelWidth: 140
    },
    bodyPadding: 10,

    items: [{
        xtype: 'combobox',
        fieldLabel: 'Рука',
        editable: false,
        emptyText: 'Выберите руку..',
        queryMode: 'local',
        store: 'handSelect',
        displayField: 'name',
        name: 'hand',
        valueField: 'id',
        triggerAction: 'all',
        reference: 'hand_combo'
    },{
        xtype: 'textfield',
        name: 'mod',
        reference: 'mod_textfield',
        fieldLabel: 'Модификатор',
        regex: /^[\+\-]?\d{1,2}$/,
        regexText: 'Допускается только целое число с плюсом или минусом, не большее 99.',
        allowBlank: false,
        enableKeyEvents: true,
        value: '0',
        listeners: {
            specialKey: 'onSpecialKey'
        }
    },{
        xtype: 'textfield',
        name: 'diff',
        reference: 'diff_textfield',
        fieldLabel: 'Сложность',
        regex: /^\d{1,2}$/,
        regexText: 'Допускается только целочисленное значение не большее 99.',
        allowBlank: false,
        enableKeyEvents: true,
        value: '0',
        listeners: {
            specialKey: 'onSpecialKey'
        }
    },{
        xtype: 'combobox',
        fieldLabel: 'Цель',
        editable: false,
        emptyText: 'Выберите цель..',
        queryMode: 'local',
        store: 'allTargets',
        displayField: 'name',
        name: 'target',
        valueField: 'id',
        triggerAction: 'all',
        reference: 'target_combo'
    },{
        xtype: 'checkboxfield',
        name: 'adv',
        reference: 'advantage_box',
        fieldLabel: 'Преимущество',
        inputValue: '1'
    },{
        xtype: 'checkboxfield',
        name: 'disadv',
        reference: 'disadvantage_box',
        fieldLabel: 'Помеха',
        inputValue: '1'
    },{
        xtype: 'checkboxfield',
        reference: 'improvised_box',
        fieldLabel: 'Импровизированная',
        inputValue: '1',
        name: 'improv',
        listeners: {
            change: 'onImprovChange',
            scope: 'controller'
        }
    },{
        xtype: 'combobox',
        hideLabel: true,
        //labelWidth: 45,
        disabled: true,
        editable: false,
        emptyText: 'Выберите тип..',
        queryMode: 'local',
        store: 'improvisedStore',
        displayField: 'name',
        name: 'improv_type',
        valueField: 'id',
        triggerAction: 'all',
        reference: 'improvised_combo'
    }],

    buttons: [{
        text: 'Атаковать',
        formBind: true,
        disabled: true,
        handler: 'onMakeAttack'
    }]
});
