Ext.define('DwD2.view.inwin.makeattack.MakeattackController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.inwin-makeattack-makeattack',

    attackText: 'Атакуем...',

    onMakeAttack: function(){
        var form = this.getView();

        if (form.isValid()){
            var data = form.getValues();
            var rm = form.requestManager;
            var settings = form.viewport.settings;

            data.ssid = settings.get('ssid');
            data.mode = 'attack';

            form.up('window').mask(this.attackText);

            rm.go({
                url: 'charattack.php',
                data: data,
                scope: this,
                success: this.onAttackSuccess,
                failure: this.onAttackFailure
            });
        }
    },

    onImprovChange: function(obj, newValue){
        if (newValue == 1){
            this.lookupReference('improvised_combo').enable();
        } else {
            this.lookupReference('improvised_combo').disable();
        }
    },

    onAttackFailure: function(){
        var window = this.getView().up('window');

        window.unmask();
    },

    onAttackSuccess: function(response){
        var window = this.getView().up('window');

        window.unmask();
        window.close();
    },

    onSpecialKey: function(field, e) {
        if (e.getKey() === e.ENTER) {
            this.onMakeAttack();
        }
    }
    
});
