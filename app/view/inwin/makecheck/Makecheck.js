
Ext.define('DwD2.view.inwin.makecheck.Makecheck',{
    extend: 'Ext.form.Panel',

    requires: [
        'DwD2.view.inwin.makecheck.MakecheckController',
        'DwD2.view.inwin.makecheck.MakecheckModel'
    ],

    controller: 'inwin-makecheck-makecheck',
    viewModel: {
        type: 'inwin-makecheck-makecheck'
    },
    
    frame: true,
    xtype: 'makecheck',
    layout: 'anchor',
    defaults: {
        anchor: '100%'
    },
    bodyPadding: 10,

    items: [{
        xtype: 'textfield',
        name: 'mod',
        reference: 'mod_textfield',
        fieldLabel: 'Модификатор',
        regex: /^[\+\-]?\d{1,2}$/,
        regexText: 'Допускается только целое число с плюсом или минусом, не большее 99.',
        allowBlank: false,
        enableKeyEvents: true,
        value: '0',
        listeners: {
            specialKey: 'onSpecialKey'
        }
    },{
        xtype: 'textfield',
        name: 'diff',
        reference: 'diff_textfield',
        fieldLabel: 'Сложность',
        regex: /^\d{1,2}$/,
        regexText: 'Допускается только целочисленное значение не большее 99.',
        allowBlank: false,
        enableKeyEvents: true,
        value: '0',
        listeners: {
            specialKey: 'onSpecialKey'
        }
    },{
        xtype: 'checkboxfield',
        name: 'adv',
        reference: 'advantage_box',
        fieldLabel: 'Преимущество',
        inputValue: '1'
    },{
        xtype: 'checkboxfield',
        name: 'disadv',
        reference: 'disadvantage_box',
        fieldLabel: 'Помеха',
        inputValue: '1'
    }],

    buttons: [{
        text: 'Проверить',
        formBind: true,
        disabled: true,
        handler: 'onMakeCheck'
    }]
});
