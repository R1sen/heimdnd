Ext.define('DwD2.view.inwin.makecheck.MakecheckController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.inwin-makecheck-makecheck',

    checkText: 'Проверяем...',

    onMakeCheck: function(){
        var form = this.getView();

        if (form.isValid()){
            var data = form.getValues();
            var rm = form.requestManager;
            var settings = form.viewport.settings;

            data.ssid = settings.get('ssid');
            data.check = form.makeCheck;

            form.up('window').mask(this.checkText);

            rm.go({
                url: 'makecheck.php',
                data: data,
                scope: this,
                success: this.onCheckSuccess,
                failure: this.onCheckFailure
            });
        }
    },

    onCheckFailure: function(){
        var window = this.getView().up('window');

        window.unmask();
    },

    onCheckSuccess: function(response){
        var window = this.getView().up('window');

        window.unmask();
        window.close();
    },

    onSpecialKey: function(field, e) {
        if (e.getKey() === e.ENTER) {
            this.onMakeCheck();
        }
    }
    
});
