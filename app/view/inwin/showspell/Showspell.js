
Ext.define('DwD2.view.inwin.showspell.Showspell',{
    extend: 'Ext.panel.Panel',

    requires: [
        'DwD2.view.inwin.showspell.ShowspellController',
        'DwD2.view.inwin.showspell.ShowspellModel'
    ],

    controller: 'inwin-showspell-showspell',
    viewModel: {
        type: 'inwin-showspell-showspell'
    },

    bodyCls: 'itemdescr-panel',
    xtype: 'showspell',
    scrollable: true,
    frame: true,

    onRender: function(){
        this.callParent();

        this.getController().getSpell(this.spellId);
    }
});
