Ext.define('DwD2.view.inwin.showspell.ShowspellController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.inwin-showspell-showspell',

    getSpellText: 'Загружаем...',

    getSpell: function(id){
        var view = this.getView();
        var rm = view.requestManager;
        var settings = view.viewport.settings;
        var data = {
            ssid: settings.get('ssid')
        };

        data.mode = 'getspell';
        data.sid = id;

        view.up('window').mask(this.getSpellText);

        rm.go({
            url: 'charaction.php',
            data: data,
            scope: this,
            success: this.onGetSuccess,
            failure: this.onFailure
        });
    },

    onGetSuccess: function(response){
        var data = Ext.JSON.decode(response.responseText);
        var view = this.getView();
        var window = view.up('window');

        window.unmask();

        var txt = '<b>' + data.spell.name + '</b><br/><i>' + data.spell.lvl + ' уровень, ' + data.spell.cat + '</i><br/><br/><b>Время накладывания:</b> ' + data.spell.timing + '<br/><b>Дистанция:</b> ' + data.spell.distance + '<br/><span data-qtip="В - вербальный, С - соматический, М - материальный"><b>Компоненты:</b> ' + data.spell.components + '</span><br/><b>Длительность:</b> ' + data.spell.duration + '<br/><br/>' + data.spell.descr;

        view.update(txt);
    },

    onFailure: function(){
        var window = this.getView().up('window');

        window.unmask();
    }
    
});
