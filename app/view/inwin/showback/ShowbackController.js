Ext.define('DwD2.view.inwin.showback.ShowbackController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.inwin-showback-showback',

    getBackText: 'Загружаем...',

    getBack: function(id){
        var view = this.getView();
        var rm = view.requestManager;
        var settings = view.viewport.settings;
        var data = {
            ssid: settings.get('ssid')
        };

        data.mode = 'getback';
        data.back_id = id;

        view.up('window').mask(this.getBackText);

        rm.go({
            url: 'charaction.php',
            data: data,
            scope: this,
            success: this.onGetSuccess,
            failure: this.onFailure
        });
    },

    onGetSuccess: function(response){
        var data = Ext.JSON.decode(response.responseText);
        var view = this.getView();
        var window = view.up('window');

        window.unmask();

        var txt = '<b>' + data.back.title + '</b><br/><br/>' + data.back.descr;

        view.update(txt);
    },

    onFailure: function(){
        var window = this.getView().up('window');

        window.unmask();
    }
    
});
