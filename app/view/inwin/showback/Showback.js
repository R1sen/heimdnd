
Ext.define('DwD2.view.inwin.showback.Showback',{
    extend: 'Ext.panel.Panel',

    requires: [
        'DwD2.view.inwin.showback.ShowbackController',
        'DwD2.view.inwin.showback.ShowbackModel'
    ],

    controller: 'inwin-showback-showback',
    viewModel: {
        type: 'inwin-showback-showback'
    },

    bodyCls: 'itemdescr-panel',
    xtype: 'showback',
    scrollable: true,
    frame: true,

    onRender: function(){
        this.callParent();

        this.getController().getBack(this.backId);
    }
});
