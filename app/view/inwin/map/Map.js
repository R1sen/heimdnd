Ext.define('DwD2.view.inwin.map.Map', {
    extend: 'Ext.tab.Panel',

    requires: [
        'DwD2.view.inwin.map.MapController',
        'DwD2.view.inwin.map.MapModel'
    ],

    controller: 'inwin-map-map',
    viewModel: {
        type: 'inwin-map-map'
    },

    xtype: 'lenzmap',
    id: 'map-tabpanel',

    items: [{
        title: 'Карта',
        tabTip: 'Карта локации',
        layout: 'fit',
        items: [{
            reference: 'tactical-map'
        }]

    }, {
        title: 'Редактирование',
        tabTip: 'Редактирование карты',
        reference: 'tab-edit-map',
        frame: true,
        disabled: true,
        items: [{
            border: false,
            xtype: 'fieldset',
            items: [
                {
                    xtype: 'textfield',
                    reference: 'MapName',
                    fieldLabel: 'Название карты',
                    name: 'MapName'
                }
            ]
        }],
        buttons: [
            {
                text: 'Создать',
                formBind: true,
                handler: 'onMapCreate'
            },
            {
                text: 'Сохранить',
                formBind: true,
                handler: 'onMapRename'
            }
        ]
    }],

    tbar: [{
        text: 'Обновить',
        iconCls: 'refresh',
        tooltip: 'Обновить карту',
        handler: 'refreshMap'
    }, {
        xtype: 'combobox',
        hideLabel: true,
        editable: false,
        width: 200,
        emptyText: 'Выберите карту..',
        queryMode: 'local',
        store: 'mapStore',
        displayField: 'name',
        valueField: 'id',
        triggerAction: 'all',
        reference: 'map_combo',
        listeners: {
            select: 'getMap',
            scope: 'controller'
        }
    }, {
        text: 'Расставить',
        iconCls: 'rotate',
        tooltip: 'Расставить фишки на карте',
        disabled: true,
        reference: 'setup_button',
        handler: 'setupMap'
    }, '-', {
        xtype: 'checkbox',
        reference: 'sync_map_t',
        checked: false,
        disabled: true,
        boxLabel: 'Синхронизировать'
    }],

    onRender: function () {
        this.callParent();

        this.getController().getMapList();
    }
});
