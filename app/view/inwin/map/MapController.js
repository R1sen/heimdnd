Ext.define('DwD2.view.inwin.map.MapController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.inwin-map-map',

    getMapText: 'Загружаем...',
    setupText: 'Расставляем...',

    getMapList: function(){
        var view = this.getView();
        var rm = view.requestManager;
        var settings = view.viewport.settings;
        var data = {
            ssid: settings.get('ssid'),
            mode: 'getmaplist'
        };

        view.up('window').mask(this.getMapText);

        rm.go({
            url: 'map.php',
            data: data,
            scope: this,
            success: this.onGetMapList,
            failure: this.onFailure
        });
    },

    onGetMapList: function(response){

        var data = Ext.util.JSON.decode(response.responseText);
        var view = this.getView();
        var settings = view.viewport.settings;

        Ext.getStore('mapStore').loadRawData(data);

        if (settings.get('dm') == 1 || settings.get('minidm') == 1) {
            this.lookupReference('tab-edit-map').enable();
            this.lookupReference('setup_button').enable();
            this.lookupReference('sync_map_t').enable();
        }

        this.onSuccess();
    },

    refreshMap: function(){
        var view = this.getView();
        var rm = view.requestManager;
        var settings = view.viewport.settings;
        var mpid = this.lookupReference('map_combo').getValue();

        if (mpid == '' || mpid == 0) return;

        var data = {
            ssid: settings.get('ssid'),
            mode: 'getmap',
            mpid: mpid
        };

        view.up('window').mask(this.getMapText);

        rm.go({
            url: 'map.php',
            data: data,
            scope: this,
            success: this.onRefreshMap,
            failure: this.onFailure
        });
    },

    setupMap: function(){
        var view = this.getView();
        var rm = view.requestManager;
        var settings = view.viewport.settings;
        var mpid = this.lookupReference('map_combo').getValue();

        if (mpid == '' || mpid == 0) return;

        var data = {
            ssid: settings.get('ssid'),
            mpid: mpid
        };

        view.up('window').mask(this.setupText);

        rm.go({
            url: 'map_load.php',
            data: data,
            scope: this,
            success: this.onRefreshMap,
            failure: this.onFailure
        });
    },

    onRefreshMap: function(response){
        var data = Ext.util.JSON.decode(response.responseText);

        this.conf = data.map.confstring;

        this.onSuccess();

        send_to_as(this.conf);
    },

    setMap: function(conf){
        var view = this.getView();
        var rm = view.requestManager;
        var settings = view.viewport.settings;
        var mpid = this.lookupReference('map_combo').getValue();
        var sync = this.lookupReference('sync_map_t').getValue();

        if (mpid == '' || mpid == 0) return;

        var data = {
            ssid: settings.get('ssid'),
            mode: 'setmap',
            mpid: mpid,
			conf: conf,
            sync: sync
        };

        view.up('window').mask(this.getMapText);

        rm.go({
            url: 'map.php',
            data: data,
            scope: this,
            success: this.onRefreshMap,
            failure: this.onFailure
        });
    },

    getMap: function(combo, record){
        var view = this.getView();
        var rm = view.requestManager;
        var settings = view.viewport.settings;
        var data = {
            ssid: settings.get('ssid'),
            mode: 'getmap',
            mpid: record.get('id')
        };

        view.up('window').mask(this.getMapText);

        rm.go({
            url: 'map.php',
            data: data,
            scope: this,
            success: this.onGetMap,
            failure: this.onFailure
        });
    },

    onGetMap: function(response){
        var data = Ext.util.JSON.decode(response.responseText);

        this.conf = data.map.confstring;

        this.onSuccess();

        var flashthing = '<object id="december" width="100%" height="100%" classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab"><param name="movie" value="http://rpgamer.ru/media/view/main.swf" /><param name="allowScriptAccess" value="always" /><embed src=\'http://pyromancers.com/media/view/main.swf?use_bd=0&data=' + this.conf + '\' quality="high" bgcolor="#000000" width="100%" height="100%" name="december" align="middle" play="true" loop="false" quality="high" allowScriptAccess="always" type="application/x-shockwave-flash" pluginspage="http://www.adobe.com/go/getflashplayer"></embed></object>';

        this.lookupReference('MapName').setValue(data.map.name);

        this.lookupReference('tactical-map').update(flashthing);
    },

    onMapRename: function(){
        var view = this.getView();
        var rm = view.requestManager;
        var settings = view.viewport.settings;
        var mpid = this.lookupReference('map_combo').getValue();
        var map_name = this.lookupReference('MapName').getValue();

        if (mpid == '' || mpid == 0) return;

        var data = {
            ssid: settings.get('ssid'),
            map_name: map_name,
            mpid: mpid,
            mode: 'rename'
        };

        rm.go({
            url: 'map.php',
            data: data,
            scope: this,
            success: function(fp, o) {
                //this.getMapList;
                Ext.getCmp('main-viewport').fireEvent('map');
            },
            failure: this.onFailure
        });
    },

    onMapCreate: function(){
        var view = this.getView();
        var rm = view.requestManager;
        var settings = view.viewport.settings;
        var map_name = this.lookupReference('MapName').getValue();

        var data = {
            ssid: settings.get('ssid'),
            map_name: map_name,
            mode: 'create'
        };

        rm.go({
            url: 'map.php',
            data: data,
            scope: this,
            success: function() {
                //this.getMapList;
                Ext.getCmp('main-viewport').fireEvent('map');
            },
            failure: this.onFailure
        });
    },

    onFailure: function(){
        var window = this.getView().up('window');

        window.unmask();
    },

    onSuccess: function(){
        var window = this.getView().up('window');

        window.unmask();
    }
    
});
