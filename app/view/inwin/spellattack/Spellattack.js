
Ext.define('DwD2.view.inwin.spellattack.Spellattack',{
    extend: 'Ext.form.Panel',

    requires: [
        'DwD2.view.inwin.spellattack.SpellattackController',
        'DwD2.view.inwin.spellattack.SpellattackModel'
    ],

    controller: 'inwin-spellattack-spellattack',
    viewModel: {
        type: 'inwin-spellattack-spellattack'
    },

    frame: true,
    xtype: 'spellattack',
    layout: 'anchor',
    defaults: {
        anchor: '100%',
        labelWidth: 140
    },
    bodyPadding: 10,

    items: [{
        xtype: 'textfield',
        name: 'mod',
        reference: 'mod_textfield',
        fieldLabel: 'Модификатор',
        regex: /^[\+\-]?\d{1,2}$/,
        regexText: 'Допускается только целое число с плюсом или минусом, не большее 99.',
        allowBlank: false,
        enableKeyEvents: true,
        value: '0',
        listeners: {
            specialKey: 'onSpecialKey'
        }
    },{
        xtype: 'textfield',
        name: 'diff',
        reference: 'diff_textfield',
        fieldLabel: 'Сложность',
        regex: /^\d{1,2}$/,
        regexText: 'Допускается только целочисленное значение не большее 99.',
        allowBlank: false,
        enableKeyEvents: true,
        value: '0',
        listeners: {
            specialKey: 'onSpecialKey'
        }
    },{
        xtype: 'combobox',
        fieldLabel: 'Цель',
        //labelWidth: 45,
        editable: false,
        emptyText: 'Выберите цель..',
        queryMode: 'local',
        store: 'allTargets',
        displayField: 'name',
        name: 'target',
        valueField: 'id',
        triggerAction: 'all',
        reference: 'target_combo'
    },{
        xtype: 'checkboxfield',
        name: 'adv',
        reference: 'advantage_box',
        fieldLabel: 'Преимущество',
        inputValue: '1'
    },{
        xtype: 'checkboxfield',
        name: 'disadv',
        reference: 'disadvantage_box',
        fieldLabel: 'Помеха',
        inputValue: '1'
    }],

    buttons: [{
        text: 'Атаковать',
        formBind: true,
        disabled: true,
        handler: 'onMakeAttack'
    }]
});
