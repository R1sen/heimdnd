Ext.define('DwD2.view.inwin.spellattack.SpellattackController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.inwin-spellattack-spellattack',

    attackText: 'Атакуем...',

    onMakeAttack: function(){
        var form = this.getView();

        if (form.isValid()){
            var data = form.getValues();
            var rm = form.requestManager;
            var settings = form.viewport.settings;

            data.ssid = settings.get('ssid');
            data.mode = 'attack';
            data.sid = form.spellId;
            data.str = form.spellString;
            data.clid = form.spellClid;

            form.up('window').mask(this.attackText);

            rm.go({
                url: 'spellattack.php',
                data: data,
                scope: this,
                success: this.onAttackSuccess,
                failure: this.onAttackFailure
            });
        }
    },

    onAttackFailure: function(){
        var window = this.getView().up('window');

        window.unmask();
    },

    onAttackSuccess: function(response){
        var window = this.getView().up('window');

        window.unmask();
        window.close();
    },

    onSpecialKey: function(field, e) {
        if (e.getKey() === e.ENTER) {
            this.onMakeAttack();
        }
    }
    
});
