Ext.define('DwD2.view.inwin.BasewinController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.inwin-basewin',

    registerWindow: function(){
        Ext.getCmp('main-viewport').fireEvent('openwin', this.getView());
    },

    onMinimize: function(){
        Ext.getCmp('main-viewport').fireEvent('minimizewin', this.getView());
    },

    onClose: function(){
        Ext.getCmp('main-viewport').fireEvent('closewin', this.getView());
    },

    checkMove: function(c, x, y){
        if (x <= 0){
            c.setPosition(1, y);
        }

        if (y <= 0){
            c.setPosition(x, 1);
        }

        var size = Ext.getCmp('main-viewport').getSize();

        if (x > size.width - 100){
            c.setPosition(size.width - 100, y);
        }

        if (y > size.height - 100){
            c.setPosition(x, size.height - 100);
        }
    },

    checkResize: function(){
        this.fireViewEvent('move');
    }
});
