
Ext.define('DwD2.view.inwin.charinfo.Charinfo',{
    extend: 'Ext.tab.Panel',

    requires: [
        'DwD2.view.inwin.charinfo.CharinfoController',
        'DwD2.view.inwin.charinfo.CharinfoModel'
    ],

    controller: 'inwin-charinfo-charinfo',
    viewModel: {
        type: 'inwin-charinfo-charinfo'
    },

    id: 'charinfo-tabpanel',
    xtype: 'charinfo',
    border: false,

    items: [{
        title: 'Описание',
        tooltip: 'Общее описание персонажа',
        layout: 'border',
        border: false,
        items: [{
            region: 'west',
            xtype: 'fieldset',
            bind: {
                title: '{info.name}'
            },
            width: 142,
            height: 229,
            items: [{
                xtype: 'image',
                alt: 'Картинка персонажа',
                bind: {
                    src: 'resources/data/faces/b/{info.b_icon}'
                }
            }]
        },{
            region: 'center',
            //frame: true,
            border: false,
            bodyCls: 'panel-filling',
            layout: {
                type: 'table',
                columns: 5
            },
            defaults: {
                bodyCls: 'item-filling',
                border: false,
                listeners: {
                    contextmenu: {
                        fn: 'lockInfo',
                        element: 'body'
                    },
                    mouseenter: {
                        fn: 'getMore',
                        element: 'body'
                    },
                    /*click: {
                        fn: 'takeOff',
                        element: 'body'
                    },*/
                    scope: 'controller'
                }

            },

            items: [{
                //panel is for underlying element events
                items: [{
                    xtype: 'puppetimage',
                    alt: 'Глаза',
                    bind: {
                        src: 'resources/data/items/{info.eyes_object.icon}',
                        alt: 'info-eyes-{info.eyes}'
                    }
                }]
            },{
                //panel is for underlying element events
                items: [{
                    xtype: 'puppetimage',
                    alt: 'Голова',
                    bind: {
                        src: 'resources/data/items/{info.head_object.icon}',
                        alt: 'info-head-{info.head}'
                    }
                }]
            },{
                //panel is for underlying element events
                items: [{
                    xtype: 'puppetimage',
                    alt: 'Шея',
                    bind: {
                        src: 'resources/data/items/{info.neck_object.icon}',
                        alt: 'info-neck-{info.neck}'
                    }
                }]
            },{
                //panel is for underlying element events
                items: [{
                    xtype: 'puppetimage',
                    alt: 'Кольцо 1',
                    bind: {
                        src: 'resources/data/items/{info.ring_1_object.icon}',
                        alt: 'info-ring_1-{info.ring_1}'
                    }
                }]
            },{
                //panel is for underlying element events
                items: [{
                    xtype: 'puppetimage',
                    alt: 'Кольцо 2',
                    bind: {
                        src: 'resources/data/items/{info.ring_2_object.icon}',
                        alt: 'info-ring_2-{info.ring_2}'
                    }
                }]
            },{
                //panel is for underlying element events
                items: [{
                    xtype: 'puppetimage',
                    alt: 'Плечи',
                    bind: {
                        src: 'resources/data/items/{info.coat_object.icon}',
                        alt: 'info-coat-{info.coat}'
                    }
                }]
            },{
                colspan: 2,
                rowspan: 3,
                xtype: 'image',
                alt: 'Аватар',
                height: 150,
                width: 100,
                bind: {
                    src: 'resources/data/faces/p/{info.picture}'
                }
            },{
                //panel is for underlying element events
                items: [{
                    xtype: 'puppetimage',
                    alt: 'Предплечья',
                    bind: {
                        src: 'resources/data/items/{info.arms_object.icon}',
                        alt: 'info-arms-{info.arms}'
                    }
                }]
            },{
                //panel is for underlying element events
                items: [{
                    xtype: 'puppetimage',
                    alt: 'Ладони',
                    bind: {
                        src: 'resources/data/items/{info.hands_object.icon}',
                        alt: 'info-hands-{info.hands}'
                    }
                }]
            },{
                //panel is for underlying element events
                items: [{
                    xtype: 'puppetimage',
                    alt: 'Броня',
                    bind: {
                        src: 'resources/data/items/{info.armor_object.icon}',
                        alt: 'info-armor-{info.armor}'
                    }
                }]
            },{
                //panel is for underlying element events
                items: [{
                    xtype: 'puppetimage',
                    alt: 'Основная рука',
                    bind: {
                        src: 'resources/data/items/{info.mainhand_object.icon}',
                        alt: 'info-mainhand-{info.mainhand}'
                    }
                }]
            },{
                //panel is for underlying element events
                items: [{
                    xtype: 'puppetimage',
                    alt: 'П1',
                    bind: {
                        src: 'resources/data/items/{info.wondrous_1_object.icon}',
                        alt: 'info-wondrous_1-{info.wondrous_1}'
                    }
                }]
            },{
                //panel is for underlying element events
                items: [{
                    xtype: 'puppetimage',
                    alt: 'Торс',
                    bind: {
                        src: 'resources/data/items/{info.torso_object.icon}',
                        alt: 'info-torso-{info.torso}'
                    }
                }]
            },{
                //panel is for underlying element events
                reference: 'offhand-puppetimage',
                items: [{
                    xtype: 'puppetimage',
                    alt: 'Вторая рука',
                    bind: {
                        src: 'resources/data/items/{info.offhand_object.icon}',
                        alt: 'info-offhand-{info.offhand}'
                    }
                }]
            },{
                //panel is for underlying element events
                items: [{
                    xtype: 'puppetimage',
                    alt: 'П2',
                    bind: {
                        src: 'resources/data/items/{info.wondrous_2_object.icon}',
                        alt: 'info-wondrous_2-{info.wondrous_2}'
                    }
                }]
            },{
                //panel is for underlying element events
                items: [{
                    xtype: 'puppetimage',
                    alt: 'Пояс',
                    bind: {
                        src: 'resources/data/items/{info.waist_object.icon}',
                        alt: 'info-waist-{info.waist}'
                    }
                }]
            },{
                //panel is for underlying element events
                items: [{
                    xtype: 'puppetimage',
                    alt: 'Ноги',
                    bind: {
                        src: 'resources/data/items/{info.feet_object.icon}',
                        alt: 'info-feet-{info.feet}'
                    }
                }]
            },{
                //panel is for underlying element events
                items: [{
                    xtype: 'puppetimage',
                    alt: 'П3',
                    bind: {
                        src: 'resources/data/items/{info.wondrous_3_object.icon}',
                        alt: 'info-wondrous_3-{info.wondrous_3}'
                    }
                }]
            },{
                //panel is for underlying element events
                items: [{
                    xtype: 'puppetimage',
                    alt: 'П4',
                    bind: {
                        src: 'resources/data/items/{info.wondrous_4_object.icon}',
                        alt: 'info-wondrous_4-{info.wondrous_4}'
                    }
                }]
            },{
                //panel is for underlying element events
                items: [{
                    xtype: 'puppetimage',
                    alt: 'П5',
                    bind: {
                        src: 'resources/data/items/{info.wondrous_5_object.icon}',
                        alt: 'info-wondrous_5-{info.wondrous_5}'
                    }
                }]
            }]
        },{
            region: 'east',
            width: 380,
            title: 'Подробное описание предметов',
            layout: 'border',
            defaults: {
                bodyCls: 'panel-filling',
                border: false
            },
            items: [{
                region: 'west',
                width: 180,
                items: [{
                    reference: 'item_more_pic',
                    xtype: 'image',
                    width: 180,
                    height: 180,
                    alt: 'Увеличенное изображение',
                    src: 'resources/data/items/empty.png'
                }]
            },{
                bodyCls: 'itemdescr-panel',
                region: 'center',
                reference: 'item_more_desc',
                scrollable: true
            }]
        },{
            region: 'south',
            height: 195,
            layout: 'border',
            border: false,

            defaults: {
                bodyCls: 'panel-filling'
            },
            items: [{
                region: 'west',
                bodyCls: 'itemdescr-panel',
                scrollable: true,
                width: 300,
                title: 'Записи ДМов',
                bind: {
                    html: '{info.dm_notes}'
                }
            },{
                region: 'center',
                bodyCls: 'itemdescr-panel',
                scrollable: true,
                title: 'Информация от игрока',
                bind: {
                    html: '{info.notes}'
                }
            }]
        }]
    },{
        scrollable: true,
        bind: {
            disabled: '{!info.dm}'
        },
        title: 'Информация',
        tooltip: 'Общая информация о персонаже',
        frame: true,
        layout: {
            type: 'table',
            columns: 3,
            tableAttrs: {
                cls: 'main-tables'
            }
        },
        defaults: {
            layout: 'fit',
            cellCls: 'top-align'
        },
        items: [{
            xtype: 'fieldset',
            title: 'Данные',
            rowspan: 2,
            height: 229,
            bind: {
                html: '<table cellspacing="0" cellpadding="0" class="chartable">' +
                '<tr>' +
                '<td><b>Имя:</b></td><td>{info.name}</td>' +
                '</tr>' +
                '<tr>' +
                '<td><b>Класс:</b></td><td>{info.class}</td>' +
                '</tr>' +
                '<tr>' +
                '<td><b>Раса:</b></td><td>{info.race_name}</td>' +
                '</tr>' +
                '<tr>' +
                '<td><b>Размер:</b></td><td>{info.size}</td>' +
                '</tr>' +
                '<tr>' +
                '<td><b>Предыстория:</b></td><td>{info.back_name}</td>' +
                '</tr>' +
                '<tr>' +
                '<td><b>Мировоззрение:</b></td><td>{info.align}</td>' +
                '</tr>' +
                '<tr>' +
                '<td><b>Уровень:</b></td><td>{info.lvl}</td>' +
                '</tr>' +
                '<tr>' +
                '<td><b>Опыт:</b></td><td>{info.exp}</td>' +
                '</tr>' +
                '<tr>' +
                '<td><b>След. уровень:</b></td><td>{info.next_lvl}</td>' +
                '</tr>' +
                '<tr>' +
                '<td><b>Вдохновение:</b></td><td>{info.inspiration}</td>' +
                '</tr>' +
                '</table>'
            }
        },{
            xtype: 'fieldset',
            title: 'Бонус мастерства',
            bind: {
                html: '<table cellspacing="0" cellpadding="0" class="chartable">' +
                '<tr>' +
                '<td><b>Бонус мастерства:</b></td><td>{info.prof_bonus}</td>' +
                '</tr>' +
                '</table>'
            }
        },{
            xtype: 'fieldset',
            title: 'Аватар',
            width: 142,
            height: 229,
            rowspan: 2,
            items: [{
                xtype: 'image',
                alt: 'Картинка персонажа',
                bind: {
                    src: 'resources/data/faces/b/{info.b_icon}'
                }
            }]
        },{
            xtype: 'fieldset',
            title: 'Здоровье и броня',
            height: 176,
            bind: {
                html: '<table cellspacing="0" cellpadding="0" class="chartable">' +
                '<tr>' +
                '<td><b>КД:</b></td><td>{info.ac}</td>' +
                '</tr>' +
                '<tr>' +
                '<td><b>ХП:</b></td><td>{info.hp}/{info.total_hp}</td>' +
                '</tr>' +
                '<tr>' +
                '<td><b>Временные ХП:</b></td><td>{info.temp_hp}</td>' +
                '</tr>' +
                '<tr>' +
                '<td><b>Кости хитов доступно:</b></td><td>{info.hit_dice}</td>' +
                '</tr>' +
                '<tr>' +
                '<td><b>Кости хитов всего:</b></td><td>{info.hit_dice_total}</td>' +
                '</tr>' +
                '<tr>' +
                '<td><b>Спасы от смерти:</b></td><td>{info.death_saves_true} успех/{info.death_saves_false} провал</td>' +
                '</tr>' +
                '</table>'
            }
        },{
            xtype: 'fieldset',
            title: 'Скорость и инициатива',
            rowspan: 2,
            bind: {
                html: '<table cellspacing="0" cellpadding="0" class="chartable">' +
                '<tr>' +
                '<td><b>Инициатива:</b></td><td>{info.initiative}</td>' +
                '</tr>' +
                '<tr>' +
                '<td><b>Скорость:</b></td><td>{info.speed} фут ({info.speed_cell} кл.)</td>' +
                '</tr>' +
                '<tr>' +
                '<td><b>Полёт:</b></td><td>{info.fly} фут ({info.fly_cell} кл.)</td>' +
                '</tr>' +
                '<tr>' +
                '<td><b>Плавание:</b></td><td>{info.swim} фут ({info.swim_cell} кл.)</td>' +
                '</tr>' +
                '<tr>' +
                '<td><b>Прыжок в длину:</b></td><td>{info.long_jump} фут ({info.long_jump_cell} кл.)</td>' +
                '</tr>' +
                '<tr>' +
                '<td><b>Прыжок в высоту:</b></td><td>{info.high_jump} фут ({info.high_jump_cell} кл.)</td>' +
                '</tr>' +
                '<tr>' +
                '<td><b>Ползание:</b></td><td>{info.crawl} фут ({info.crawl_cell} кл.)</td>' +
                '</tr>' +
                '<tr>' +
                '<td><b>Вскарабкивание:</b></td><td>{info.climb} фут ({info.climb_cell} кл.)</td>' +
                '</tr>' +
                '</table>'
            }
        },{
            xtype: 'fieldset',
            title: 'Пассивные навыки',
            colspan: 2,
            bind: {
                html: '<table cellspacing="0" cellpadding="0" class="chartable">' +
                '<tr>' +
                '<td><b>Пассивная внимательность:</b></td><td>{info.passive_perception}</td>' +
                '</tr>' +
                '<tr>' +
                '<td><b>Пассивный анализ:</b></td><td>{info.passive_investigation}</td>' +
                '</tr>' +
                '</table>'
            }
        },{
            xtype: 'fieldset',
            title: 'Зрение и чувства',
            colspan: 2,
            height: 110,
            bind: {
                html: '<table cellspacing="0" cellpadding="0" class="chartable">' +
                '<tr>' +
                '<td><b>Зрение и чувства:</b></td><td>{info.vis_spec_sen}</td>' +
                '</tr>' +
                '</table>'
            }
        }]
    },{
        scrollable: true,
        bind: {
            disabled: '{!info.dm}'
        },
        title: 'Характеристики',
        tooltip: 'Базовые характеристики и навыки',
        frame: true,
        layout: 'fit',
        items: [{
            xtype: 'fieldset',
            title: 'Характеристики, спасброски и навыки',
            autoScroll: true,
            bind: {
                html: '<table cellspacing="0" cellpadding="0" class="chartable">' +
                '<tr>' +
                '<td width="60%">' +
                '<table cellspacing="0" cellpadding="0" class="chartable-attr">' +
                '<tr>' +
                '<td colspan="2" class="centr"><b>Сила</b></td>' +
                '</tr>' +
                '<tr>' +
                '<td class="stat-cell-size"><img class="stat-size" src="resources/server/get_attr_img.php?attr=str&attr_val={info.str}" alt="Сила"/></td><td style="padding-left: 5px; width=100%;">{info.str_save_prof} Спасброски: {info.str_save}<br/>{info.athletics_prof} Атлетика: {info.athletics}<br/>Переносимый груз: {info.weight} фунт ({info.weight_kg} кг)<br/>Максимальный груз: {info.weight_max} фунт ({info.weight_max_kg} кг)<br/>Толкать, тянуть, поднимать: {info.push_drag_lift} фунт ({info.push_drag_lift_kg} кг)</td>' +
                '</tr>' +
                '</table>' +
                '</td><td>' +
                '<table cellspacing="0" cellpadding="0" class="chartable-attr">' +
                '<tr>' +
                '<td colspan="2" class="centr"><b>Ловкость</b></td>' +
                '</tr>' +
                '<tr>' +
                '<td class="stat-cell-size"><img class="stat-size" src="resources/server/get_attr_img.php?attr=dex&attr_val={info.dex}" alt="Ловкость"/></td><td style="padding-left: 5px; width=100%;">{info.dex_save_prof} Спасброски: {info.dex_save}<br/>{info.acrobatics_prof} Акробатика: {info.acrobatics}<br/>{info.sleight_prof} Ловкость рук: {info.sleight}<br/>{info.stealth_prof} Скрытность: {info.stealth}</td>' +
                '</tr>' +
                '</table>' +
                '</td>' +
                '</tr>' +
                '<tr>' +
                '<td>' +
                '<table cellspacing="0" cellpadding="0" class="chartable-attr">' +
                '<tr>' +
                '<td colspan="2" class="centr"><b>Телосложение</b></td>' +
                '</tr>' +
                '<tr>' +
                '<td class="stat-cell-size"><img class="stat-size" src="resources/server/get_attr_img.php?attr=cons&attr_val={info.con}" alt="Телосложение"/></td><td style="padding-left: 5px; width=100%;">{info.con_save_prof} Спасброски: {info.con_save}<br/>Задержка дыхания: {info.breath}<br/>Нехватка воздуха: {info.suffocating} раунда</td>' +
                '</tr>' +
                '</table>' +
                '</td>' +
                '<td>' +
                '<table cellspacing="0" cellpadding="0" class="chartable-attr">' +
                '<tr>' +
                '<td colspan="2" class="centr"><b>Интеллект</b></td>' +
                '</tr>' +
                '<tr>' +
                '<td class="stat-cell-size"><img class="stat-size" src="resources/server/get_attr_img.php?attr=intl&attr_val={info.intl}" alt="Интеллект"/></td><td style="padding-left: 5px; width=100%;">{info.intl_save_prof} Спасброски: {info.intl_save}<br/>{info.investigation_prof} Анализ: {info.investigation}<br/>{info.history_prof} История: {info.history}<br/>{info.arcana_prof} Магия: {info.arcana}<br/>{info.nature_prof} Природа: {info.nature}<br/>{info.religion_prof} Религия: {info.religion}</td>' +
                '</tr>' +
                '</table>' +
                '</td>' +
                '</tr>' +
                '<tr>' +
                '<td>' +
                '<table cellspacing="0" cellpadding="0" class="chartable-attr">' +
                '<tr>' +
                '<td colspan="2" class="centr"><b>Мудрость</b></td>' +
                '</tr>' +
                '<tr>' +
                '<td class="stat-cell-size"><img class="stat-size" src="resources/server/get_attr_img.php?attr=wis&attr_val={info.wis}" alt="Мудрость"/></td><td style="padding-left: 5px; width=100%;">{info.wis_save_prof} Спасброски: {info.wis_save}<br/>{info.perception_prof} Внимательность: {info.perception}<br/>{info.survival_prof} Выживание: {info.survival}<br/>{info.medicine_prof} Медицина: {info.medicine}<br/>{info.insight_prof} Проницательность: {info.insight}<br/>{info.animal_prof} Уход за животными: {info.animal}</a></td>' +
                '</tr>' +
                '</table>' +
                '</td><td>' +
                '<table cellspacing="0" cellpadding="0" class="chartable-attr">' +
                '<tr>' +
                '<td colspan="2" class="centr"><b>Харизма</b></td>' +
                '</tr>' +
                '<tr>' +
                '<td class="stat-cell-size"><img class="stat-size" src="resources/server/get_attr_img.php?attr=cha&attr_val={info.cha}" alt="Харизма"/></td><td style="padding-left: 5px; width=100%;">{info.cha_save_prof} Спасброски: {info.cha_save}<br/>{info.performance_prof} Выступление: {info.performance}<br/>{info.intimidation_prof} Запугивание: {info.intimidation}<br/>{info.deception_prof} Обман: {info.deception}<br/>{info.persuasion_prof} Убеждение: {info.persuasion}</a></td>' +
                '</tr>' +
                '</table>' +
                '</td>' +
                '</tr>' +
                '</table>'
            }
        }]
    },{
        title: 'Инвентарь',
        tooltip: 'Предметы на персонаже',
        layout: 'border',
        bind: {
            disabled: '{!info.dm}'
        },
        items: [{
            region: 'center',
            title: 'Подробное описание предметов и действия с листом персонажа',
            layout: 'border',
            defaults: {
                bodyCls: 'panel-filling',
                border: false
            },
            items: [{
                region: 'west',
                width: 180,
                items: [{
                    reference: 'item_dminfo_more_pic',
                    xtype: 'image',
                    width: 180,
                    height: 180,
                    alt: 'Увеличенное изображение',
                    src: 'resources/data/items/empty.png'
                }]
            },{
                bodyCls: 'itemdescr-panel',
                region: 'center',
                reference: 'item_dminfo_more_desc',
                scrollable: true
            },{
                region: 'north',
                tbar: [{
                    tooltip: 'Платина',
                    iconCls: 'platinum',
                    bind: {
                        text: '{info.platinum}'
                    }
                }, {
                    tooltip: 'Золото',
                    iconCls: 'gold',
                    bind: {
                        text: '{info.gold}'
                    }
                }, {
                    tooltip: 'Серебро',
                    iconCls: 'silver',
                    bind: {
                        text: '{info.silver}'
                    }
                }, {
                    tooltip: 'Медь',
                    iconCls: 'copper',
                    bind: {
                        text: '{info.copper}'
                    }
                }]
            }]
        },{
            region: 'south',
            frame: true,
            height: 170,
            title: 'Инвентарь',
            reference: 'inventory-panel',
            scrollable: true,
            items: [{
                bodyCls: 'panel-filling',
                border: false,
                reference: 'inventory-items',
                layout: {
                    type: 'table',
                    columns: 13,
                    tableAttrs: {
                        style: 'table-layout: fixed'
                    }
                },
                defaults: {
                    bodyCls: 'item-filling-inv',
                    listeners: {
                        contextmenu: {
                            fn: 'lockInfo',
                            element: 'body'
                        },
                        mouseenter: {
                            fn: 'getMore',
                            element: 'body'
                        },
                        singletap: {
                            fn: 'getMore',
                            element: 'body'
                        },
                        /*click: {
                            fn: 'takeOn',
                            element: 'body'
                        },*/
                        scope: 'controller'
                    }
                }
            }]
        }/*,{
            bodyCls: 'itemdescr-panel',
            region: 'north',
            reference: 'item_money',
            height: 23
        }*/]
    },{
        title: 'Таланты',
        tooltip: 'Информация о талантах',
        bind: {
            disabled: '{!info.dm}'
        },
        reference: 'char-powerspanel',

        layout: 'border',
        items: [{
            region: 'west',
            width: 350,
            xtype: 'treepanel',
            store: 'infoPowersTree',
            rootVisible: false,
            useArrows: true,
            frame: true,
            title: 'Информация о талантах',
            bufferedRenderer: false,
            animate: true,
            listeners: {
                select: 'onPowerSelect',
                scope: 'controller'
            }
        },{
            scrollable: true,
            region: 'center',
            reference: 'powerdescr-panel',
            bodyCls: 'itemdescr-panel',
            title: 'Описание таланта'
        }]
    },{
        title: 'Заклинания',
        tooltip: 'Информация о заклинаниях',
        reference: 'char-spellspanel',
        bind: {
            disabled: '{!info.dm}'
        },
        layout: 'border',
        items: [{
            region: 'west',
            width: 350,
            xtype: 'treepanel',
            store: 'infoSpellsTree',
            rootVisible: false,
            useArrows: true,
            frame: true,
            title: 'Доступные заклинания',
            bufferedRenderer: false,
            animate: true,
            reference: 'spells_treepanel',
            listeners: {
                select: 'onSpellSelect',
                beforecheckchange: 'onBeforeCheckChange',
                scope: 'controller'
            }
        },{
            region: 'center',
            scrollable: true,
            reference: 'spelldescr-panel',
            bodyCls: 'itemdescr-panel',
            title: 'Описание заклинания'
        },{
            region: 'north',
            xtype: 'fieldset',
            title: 'Информация о заклинателе',
            bind: {
                html: '<table cellspacing="0" cellpadding="0" class="chartable">' +
                '<tr>' +
                '<td class="centr"><b>Класс</b></td><td class="centr"><b>Атрибуты</b></td><td class="centr"><b>Спасброски</b></td><td class="centr"><b>Уровень</b></td><td class="centr"><b>Подготов/Знание</b></td>' +
                '</tr>' +
                '<tr>' +
                '<td class="centr">{info.spellcaster_class}</td><td class="centr">{info.spellcaster_attrs}</td><td class="centr">{info.spellcaster_saves}</td><td class="centr">{info.spellcaster_lvl}</td><td class="centr">{info.spellcaster_prepares}</td>' +
                '</tr>' +
                '<tr>' +
                '<td class="centr"><b>Ячеек доступно:</b></td><td colspan="4">' +
                '<table cellspacing="0" cellpadding="0" class="chartable"><tr>' +
                '<td><img src="resources/server/get_cell_img.php?cell=1&val={info.slot_1}&warslot={info.warslot_1}"/></td><td><img src="resources/server/get_cell_img.php?cell=2&val={info.slot_2}&warslot={info.warslot_2}"/></td><td><img src="resources/server/get_cell_img.php?cell=3&val={info.slot_3}&warslot={info.warslot_3}"/></td><td><img src="resources/server/get_cell_img.php?cell=4&val={info.slot_4}&warslot={info.warslot_4}"/></td><td><img src="resources/server/get_cell_img.php?cell=5&val={info.slot_5}&warslot={info.warslot_5}"/></td><td><img src="resources/server/get_cell_img.php?cell=6&val={info.slot_6}"/></td><td><img src="resources/server/get_cell_img.php?cell=7&val={info.slot_7}"/></td><td><img src="resources/server/get_cell_img.php?cell=8&val={info.slot_8}"/></td><td><img src="resources/server/get_cell_img.php?cell=9&val={info.slot_9}"/></td>' +
                '</tr>' +
                '</table>' +
                '</td>' +
                '</tr>' +
                '</table>'
            }
        }]
    },{
        title: 'Черты',
        tooltip: 'Информация о чертах',
        bind: {
            disabled: '{!info.dm}'
        },
        reference: 'char-featspanel',
        scrollable: true,
        frame: true
    },{
        scrollable: true,
        title: 'Инфо',
        tooltip: 'Настройки и описание персонажа',
        bind: {
            disabled: '{!info.dm}'
        },
        frame: true,
        reference: 'char-infopanel',
        xtype: 'form',
        items: [{
            fieldLabel: 'Алиас',
            name: 'char_alias',
            xtype: 'textfield',
            allowEmpty: false,
            formBind: true,
            reference: 'info_char_alias',
            bind: {
                value: '{info.alias}'
            }
        },{
            fieldLabel: 'Картинка в рост (100х150)',
            name: 'picture',
            xtype: 'fileuploadfield',
            anchor: '50%',
            emptyText: 'Выберите PNG-изображение..',
            buttonText: 'Открыть...',
            reference: 'char_picture'
        },{
            fieldLabel: 'ДМ-инфа',
            name: 'char_dminfo',
            reference: 'char_dminfo',
            xtype: 'htmleditor',
            width: 710,
            height: 380,
            bind: {
                value: '{info.dm_notes}'
            }
        },{
            fieldLabel: 'ДМ-заметки (скрытые)',
            name: 'char_dmhidden',
            reference: 'char_dmhidden',
            xtype: 'htmleditor',
            width: 710,
            height: 380,
            bind: {
                value: '{info.dm_hidden}'
            }
        }],
        buttons: [{
            text: 'Сохранить',
            formBind: true,
            handler: 'onInfoSave'
        }]
    }],

    onRender: function(){
        this.callParent();

        this.getController().getCharinfo();
    }
});
