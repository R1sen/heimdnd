Ext.define('DwD2.view.inwin.charinfo.CharinfoController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.inwin-charinfo-charinfo',

    fixedInfo: false,
    getCharinfoText: 'Загрузка...',
    saveInfoText: 'Сохраняем...',

    lockInfo: function (e, t, o) {
        e.preventDefault();

        this.fixedInfo = !this.fixedInfo;
    },

    getItem: function (id) {
        var target = false;

        Ext.each(this.info.get('items'), function (item) {
            if (item.id == id) {
                target = item;
                return false;
            }
        });

        return target;
    },

    getMore: function (e, t, o) {
        if (this.fixedInfo) return;

        if (Ext.get(t.id).component.items) {
            var what = Ext.get(t.id).component.items.items[0].alt;
        } else {
            var what = t.alt;
        }

        var spl = what.split('-');

        var txt = '';
        var node = false;
        var base = '';
        var enchant_info = '';
        var enchant_rarity = {
            common: 'Обычное',
            uncommon: 'Необычное',
            rare: 'Редкое',
            'very rare': 'Очень редкое',
            legendary: 'Легендарное'
        };


        if (spl[2] == 0) {
            return;
        }

        if (spl[1] == 'item') {
            node = this.getItem(spl[2]);
            base = node.base;
        } else {
            base = spl[1];
            node = this.info.get(base + '_object');
        }

        /*switch (base){
         case 'armor':
         if (!node) node = this.char.get('armor_object');
         break;

         case 'mainhand':

         default: return;
         }*/

        if (node.enchant_id != 0)
            enchant_info = '<br><br><i>Зачарование</i><br><b>' + node.enchant_object.name + '</b>(' + enchant_rarity[node.enchant_object.rarity] + ')<br><b>Описание:</b>' + node.enchant_object.descr;

        if (this.info.get('dm') == 1)
            this.lookupReference('item_more_desc').update('<b>' + node.name + '</b><br/><br/>' + txt + node.descr + enchant_info);
        else
            this.lookupReference('item_more_desc').update('<b>' + node.name + '</b><br/><br/>' + txt + node.descr);

        this.lookupReference('item_more_pic').setSrc('resources/data/items/' + node.icon);
        //this.lookupReference('item_more_desc').update('<b>' + node.name + '</b><br/><br/>' + txt + node.descr);

        this.lookupReference('item_dminfo_more_pic').setSrc('resources/data/items/' + node.icon);
        this.lookupReference('item_dminfo_more_desc').update('<b>' + node.name + '</b><br/><br/>' + txt + node.descr + enchant_info);
    },

    addItem: function (item) {
        var items_table = this.lookupReference('inventory-items');

        items_table.add({
            //panel is for underlying element events
            items: [{
                xtype: 'image',
                width: 50,
                height: 50,
                alt: 'char-item-' + item.id,
                src: 'resources/data/items/' + item.icon
            }]
        });
    },

    getCharinfo: function () {
        var view = this.getView();
        var rm = view.requestManager;
        var settings = view.viewport.settings;
        var data = {
            ssid: settings.get('ssid'),
            cid: view.charId,
            mode: 'char'
        };

        view.up('window').mask(this.getCharinfoText);

        rm.go({
            url: 'getinfo.php',
            data: data,
            scope: this,
            success: this.onGetCharinfo,
            failure: this.onFailure
        });
    },

    onGetCharinfo: function (response) {
        if (this.info) this.info.erase();

        var charModel = Ext.create('DwD2.model.CharsheetInfo');

        var charSet = charModel.getProxy().getReader().read(response, {
            recordCreator: this.getSession().recordCreator
        });

        var chara = charSet.getRecords()[0];
        this.info = chara;

        charSet.destroy();

        this.getViewModel().linkTo('info', this.info);

        this.getView().up('window').setTitle('Описание персонажа - ' + this.info.get('name'));

        var settings = this.getView().viewport.settings;
        if (settings.get('dm') == 1) this.info.set('dm', 1);

        if ((this.info.get('mainhand') == this.info.get('offhand')) && this.info.get('mainhand') != 0) {
            this.lookupReference('offhand-puppetimage').disable();
        } else {
            this.lookupReference('offhand-puppetimage').enable();
        }

        if (this.info.get('dm') == 1) {
            Ext.each(this.info.get('items'), function (item) {
                this.addItem(item);
            }, this);

            Ext.getStore('infoPowersTree').setRoot(this.info.get('powers_tree'));
            Ext.getStore('infoSpellsTree').setRoot(this.info.get('spells_tree'));

            this.loadFeats();

            //var moneys = '<b>Монеты:</b> |<span style="color: steelBlue  ">' + this.info.get('platinum') + ' платины</span>|<span style="color: DarkGoldenRod ">' + this.info.get('gold') + ' золота</span>|<span style="color: silver ">' + this.info.get('silver') + ' серебра</span>|<span style="color: chocolate  ">' + this.info.get('copper') + ' медных</span>|';

            //this.lookupReference('item_money').update(moneys);

        }

        this.onSuccess();
    },

    onPowerSelect: function (obj, rec, index, opt) {
        var val = rec.getData();

        if (val.parentId == 'root') {
            return;
        }

        var power = this.info.get('powers')[this.getPowerIndex(val.text)];

        var txt = '<b>' + power.title + '</b><br/><br/>' + power.descr;

        this.lookupReference('powerdescr-panel').update(txt);
    },

    getPowerIndex: function (txt) {
        var ret = false;

        Ext.each(this.info.get('powers'), function (power, index) {
            if (power.title == txt) {
                ret = index;
                return false;
            }
        });

        return ret;
    },

    onBeforeCheckChange: function (record, checkedState, e) {
        return false;
    },

    onSpellSelect: function (obj, rec, index, opt) {
        var val = rec.getData();

        if (!val.leaf) {
            return;
        }

        var spell = this.info.get('spells')[this.getSpellIndex(val.text)];

        var txt = '<b>' + spell.name + '</b><br/><i>' + spell.lvl + ' уровень, ' + spell.cat + '</i><br/><br/><b>Время накладывания:</b> ' + spell.timing + '<br/><b>Дистанция:</b> ' + spell.distance + '<br/><span data-qtip="В - вербальный, С - соматический, М - материальный"><b>Компоненты:</b> ' + spell.components + '</span><br/><b>Длительность:</b> ' + spell.duration + '<br/><br/>' + spell.descr;

        this.lookupReference('spelldescr-panel').update(txt);
    },

    getSpellIndex: function (txt) {
        var ret = false;

        Ext.each(this.info.get('spells'), function (spell, index) {
            if (spell.name == txt) {
                ret = index;
                return false;
            }
        });

        return ret;
    },

    loadFeats: function () {
        var panel = this.lookupReference('char-featspanel');
        var feats = this.info.get('feats');

        if (feats.length == 0) {
            panel.update('У вас нету черт для отображения.');
        } else {
            Ext.each(feats, function (feat) {
                panel.add({
                    xtype: 'fieldset',
                    collapsible: true,
                    collapsed: true,
                    title: feat.title,
                    toggleOnTitleClick: false,
                    items: [{
                        bodyCls: 'itemdescr-panel',
                        border: false,
                        html: feat.descr
                    }]
                });
            }, this);
        }
    },

    onInfoSave: function () {
        var form = this.lookupReference('char-infopanel');
        var view = this.getView();
        var settings = view.viewport.settings;

        if (form.isValid()) {
            view.up('window').mask(this.saveInfoText);

            form.submit({
                url: 'resources/server/dm.php',
                params: {
                    ssid: settings.get('ssid'),
                    mode: 'saveinfo',
                    cid: view.charId
                },
                scope: this,
                success: this.onSuccess,
                failure: this.onError
            });
        }
    },

    onSuccess: function () {
        var window = this.getView().up('window');

        window.unmask();
    },

    onFailure: function () {
        var window = this.getView().up('window');

        window.unmask();
    }

});
