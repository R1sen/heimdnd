Ext.define('DwD2.view.inwin.usespell.UsespellController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.inwin-usespell-usespell',

    useText: 'Используем...',

    onUseSpell: function(){
        var form = this.getView();

        if (form.isValid()){
            var data = form.getValues();
            var rm = form.requestManager;
            var settings = form.viewport.settings;

            data.ssid = settings.get('ssid');
            data.sid = form.useSpell;
            data.mode = 'usespell';

            form.up('window').mask(this.useText);

            this.slot = data.slot;

            rm.go({
                url: 'charaction.php',
                data: data,
                scope: this,
                success: this.onUseSuccess,
                failure: this.onUseFailure
            });
        }
    },

    onUseFailure: function(){
        var window = this.getView().up('window');

        window.unmask();
    },

    onUseSuccess: function(response){
        var window = this.getView().up('window');

        window.unmask();

        if (this.slot) Ext.getCmp('charlist-tabpanel').getController().calcSlots(this.slot);

        window.close();
    }
    
});
