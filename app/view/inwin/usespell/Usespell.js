
Ext.define('DwD2.view.inwin.usespell.Usespell',{
    extend: 'Ext.form.Panel',

    requires: [
        'DwD2.view.inwin.usespell.UsespellController',
        'DwD2.view.inwin.usespell.UsespellModel'
    ],

    controller: 'inwin-usespell-usespell',
    viewModel: {
        type: 'inwin-usespell-usespell'
    },

    frame: true,
    xtype: 'usespell',
    layout: 'anchor',
    defaults: {
        anchor: '100%'
    },
    bodyPadding: 10,

    items: [{
        xtype: 'combobox',
        fieldLabel: 'Цель',
        labelWidth: 45,
        editable: false,
        emptyText: 'Выберите цель..',
        queryMode: 'local',
        store: 'allTargets',
        displayField: 'name',
        name: 'target',
        valueField: 'id',
        triggerAction: 'all',
        reference: 'target_combo'
    },{
        xtype: 'combobox',
        fieldLabel: 'Ячейка',
        labelWidth: 45,
        editable: false,
        emptyText: 'Выберите ячейку..',
        queryMode: 'local',
        store: 'actualSlots',
        displayField: 'name',
        name: 'slot',
        valueField: 'id',
        triggerAction: 'all',
        reference: 'slot_combo'
    }],

    buttons: [{
        text: 'Использовать',
        handler: 'onUseSpell'
    }],

    onRender: function(){
        this.callParent();

        if (this.useSpell == -1) this.getController().lookupReference('target_combo').disable();
    }
});
