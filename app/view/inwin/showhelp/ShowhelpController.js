Ext.define('DwD2.view.inwin.showhelp.ShowhelpController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.inwin-showhelp-showhelp',

    getHelpText: 'Загружаем...',

    getHelp: function(help){
        var view = this.getView();

        view.up('window').mask(this.getHelpText);

        Ext.Ajax.request({
            url: 'resources/data/help/stats/' + translit(help) + '.html',
            scope: this,
            callback: function(opts, success, response) {
                var details = this.getView();
                details.update(response.responseText);
                this.onFailure();
            }
        });
    },

    onFailure: function(){
        var window = this.getView().up('window');

        window.unmask();
    }
    
});
