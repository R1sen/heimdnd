
Ext.define('DwD2.view.inwin.showhelp.Showhelp',{
    extend: 'Ext.panel.Panel',

    requires: [
        'DwD2.view.inwin.showhelp.ShowhelpController',
        'DwD2.view.inwin.showhelp.ShowhelpModel'
    ],

    controller: 'inwin-showhelp-showhelp',
    viewModel: {
        type: 'inwin-showhelp-showhelp'
    },

    bodyCls: 'itemdescr-panel',
    xtype: 'showhelp',
    scrollable: true,
    frame: true,

    onRender: function(){
        this.callParent();

        this.getController().getHelp(this.help);
    }
});
