
Ext.define('DwD2.view.inwin.dices.Dices',{
    extend: 'Ext.form.Panel',

    requires: [
        'DwD2.view.inwin.dices.DicesController',
        'DwD2.view.inwin.dices.DicesModel'
    ],

    controller: 'inwin-dices-dices',
    viewModel: {
        type: 'inwin-dices-dices'
    },

    frame: true,
    xtype: 'dices',
    bodyPadding: 10,
    defaults: {
        anchor: '100%',
        labelWidth: 110
    },

    items: [{
        xtype: 'textfield',
        name: 'dice',
        reference: 'dice_field',
        fieldLabel: 'Формула кубиков',
        regex: /^\d{1,2}(d|к|д)\d{1,3}((\+|\-)\d{1,2})?$/,
        regexText: 'Неверная формула кубиков (пример: 3d10+2 или 3к10+2 или 3д10+2)',
        allowBlank: false,
        enableKeyEvents: true,
        listeners: {
            specialKey: 'onSpecialKey'
        }
    },{
        xtype: 'textfield',
        name: 'reason',
        reference: 'reason_field',
        fieldLabel: 'Причина',
        allowBlank: true,
        enableKeyEvents: true,
        listeners: {
            specialKey: 'onSpecialKey'
        }
    },{
        xtype: 'checkboxfield',
        name: 'summary',
        reference: 'summary_box',
        fieldLabel: 'Только сумма',
        inputValue: '1'
    },{
        xtype: 'checkboxfield',
        name: 'dm_only',
        reference: 'dm_only_box',
        fieldLabel: 'Только для ДМа',
        inputValue: '1'
    }],

    buttons: [{
        text: 'Бросить',
        formBind: true,
        disabled: true,
        handler: 'onThrowDice'
    }]
});
