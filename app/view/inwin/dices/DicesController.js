Ext.define('DwD2.view.inwin.dices.DicesController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.inwin-dices-dices',

    dicesText: 'Атакуем...',

    onThrowDice: function(){
        var form = this.getView();

        if (form.isValid()){
            var data = form.getValues();
            var rm = form.requestManager;
            var settings = form.viewport.settings;

            data.ssid = settings.get('ssid');

            form.up('window').mask(this.dicesText);

            rm.go({
                url: 'dice.php',
                data: data,
                scope: this,
                success: this.onDiceSuccess,
                failure: this.onDiceFailure
            });
        }
    },

    onDiceFailure: function(){
        var window = this.getView().up('window');

        window.unmask();
    },

    onDiceSuccess: function(response){
        var window = this.getView().up('window');

        window.unmask();
    },

    onSpecialKey: function(field, e) {
        if (e.getKey() === e.ENTER) {
            this.onThrowDice();
        }
    }
    
});
