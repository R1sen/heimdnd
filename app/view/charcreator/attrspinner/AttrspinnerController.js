Ext.define('DwD2.view.charcreator.attrspinner.AttrspinnerController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.charcreator-attrspinner-attrspinner',

    getChange: function(val, dir){
        switch (dir){
            case 'up':{
                if (val >= 9 && val <= 13){
                    return 1;
                } else if (val >= 14 && val <= 15){
                    return 2;
                }

                break;
            }

            case 'down':{
                if (val <= 15 && val >= 13){
                    return 2;
                } else if (val <= 12 && val >= 8){
                    return 1;
                }

                break;
            }

            default:{
                return 0;
            }
        }
    }
    
});
