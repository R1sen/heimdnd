
Ext.define('DwD2.view.charcreator.attrspinner.Attrspinner',{
    extend: 'Ext.form.field.Spinner',

    requires: [
        'DwD2.view.charcreator.attrspinner.AttrspinnerController',
        'DwD2.view.charcreator.attrspinner.AttrspinnerModel'
    ],

    controller: 'charcreator-attrspinner-attrspinner',
    viewModel: {
        type: 'charcreator-attrspinner-attrspinner'
    },

    editable: false,
    value: 8,
    xtype: 'attrspinner',

    onSpinUp: function() {
        var val = parseInt(this.getValue());
        var pool = Ext.getCmp('charcreator-cardpanel').getController().attrpoints;

        if (val == 15) return;

        val++;

        var change = this.getController().getChange(val, 'up');

        if ((pool - change) < 0) return;

        pool -= change;

        if (pool == 0){
            Ext.get('attr-points').update('0');
        } else {
            Ext.get('attr-points').update(pool);
        }

        Ext.getCmp('charcreator-cardpanel').getController().setPool(pool);

        this.setValue(val);
    },

    onSpinDown: function() {
        var val = parseInt(this.getValue());
        var pool = Ext.getCmp('charcreator-cardpanel').getController().attrpoints;

        if (val == 8) return;

        val--;

        var change = this.getController().getChange(val, 'down');

        if ((pool + change) > 27) return;

        pool += change;

        if (pool == 0){
            Ext.get('attr-points').update('0');
        } else {
            Ext.get('attr-points').update(pool);
        }
        
        Ext.getCmp('charcreator-cardpanel').getController().setPool(pool);

        this.setValue(val);
    }
});
