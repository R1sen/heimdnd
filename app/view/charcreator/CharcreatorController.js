Ext.define('DwD2.view.charcreator.CharcreatorController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.charcreator-charcreator',

    nextText: 'Готовим шаг...',
    loadText: 'Загружаем...',
    cancelText: 'Удаляем...',

    attrpoints: 27,

    selCount: 0,
    selNeed: 0,
    crStep: 0,
    crCid: 0,
    currentType: '',
    selectedItems: new Array(),

    setPool: function(val){
        this.attrpoints = val;
    },

    nextStep: function(){
        if (this.crStep == 0){
            var form = this.lookupReference('creator_mainform');

            if (form.isValid()) {
                var data = form.getValues();
                var view = this.getView();
                var rm = view.requestManager;

                data.mode = 'first';
                data.username = view.user.get('name');
                data.password = view.user.get('pass');

                form.up('window').mask(this.nextText);

                rm.go({
                    url: 'creator.php',
                    data: data,
                    scope: this,
                    success: this.onNextSuccess,
                    failure: this.onFailure
                });
            }
        } else {
            if (this.currentType != 'finish' && this.currentType != 'skip' && (this.selCount == 0 || this.selCount != this.selNeed)){
                Ext.Msg.show({
                    title: 'Ошибка',
                    msg: 'Выбраны не все требуемые опции!',
                    modal: true,
                    icon: Ext.Msg.ERROR,
                    buttons: Ext.Msg.OK
                });

                return;
            }

            var view = this.getView();
            var rm = view.requestManager;
            var data = {};

            data.mode = 'next';
            data.username = view.user.get('name');
            data.password = view.user.get('pass');
            data.cid = this.crCid;

            view.up('window').mask(this.nextText);

            switch (this.currentType){
                case 'selectitem':{
                    data.selected = this.selectedItems.join(';');

                    break;
                }

                case 'finish':{
                    Ext.getCmp('main-viewport').fireEvent('charcreated');
                    view.up('window').close();

                    return;
                    break;
                }

                case 'skip':{
                    break;
                }
            }

            rm.go({
                url: 'creator.php',
                data: data,
                scope: this,
                success: this.onNextSuccess,
                failure: this.onFailure
            });
        }
    },

    onAlignmentSelect: function(combo, record){
        Ext.Ajax.request({
            url: 'resources/data/help/alignment.html',
            scope: this,
            callback: function(opts, success, response) {
                var details = this.lookupReference('details_panel');
                details.update(response.responseText);
            }
        });
    },

    onComboSelect: function(combo, record){
        var subj = record.get('name');
        var type = combo.getName().split('_')[1];

        Ext.Ajax.request({
            url: 'resources/data/help/' + type + '/' + translit(subj) + '.html',
            scope: this,
            callback: function(opts, success, response) {
                var details = this.lookupReference('details_panel');
                details.update(response.responseText);
            }
        });
    },

    loadAttrHelp: function(){
        Ext.Ajax.request({
            url: 'resources/data/help/stats.html',
            scope: this,
            callback: function(opts, success, response) {
                var details = this.lookupReference('details_panel');
                details.update(response.responseText);
            }
        });
    },

    onAttrChange: function(obj){
        var subj = obj.getFieldLabel();

        Ext.Ajax.request({
            url: 'resources/data/help/stats/' + translit(subj) + '.html',
            scope: this,
            callback: function(opts, success, response) {
                var details = this.lookupReference('details_panel');
                details.update(response.responseText);
            }
        });
    },

    onFailure: function(){
        var window = this.getView().up('window');

        window.unmask();
    },

    onSuccess: function(response){
        var window = this.getView().up('window');

        window.unmask();
    },

    onNextSuccess: function(response){
        var data = Ext.JSON.decode(response.responseText);
        var panel = Ext.getCmp('charcreator-cardpanel');

        this.lookupReference('cancel_button').enable();

        this.crStep++;

        panel.add({
            layout: 'border',
            border: false,
            items: [{
                region: 'center',
                id: 'setupcard-' + this.crStep,
                border: false,
                bodyCls: 'panel-filling',
                scrollable: true,
                bodyPadding: 10
            },{
                region: 'east',
                width: 300,
                id: 'setuphelp-' + this.crStep,
                border: false,
                bodyCls: 'panel-filling',
                scrollable: true,
                bodyPadding: 10
            }]
        });

        this.currentType = data.setup.type;
        this.selNeed = data.setup.need;
        this.crCid = data.setup.cid;
        this.help = data.setup.help;
        this.selCount = 0;
        this.selectedItems = new Array();

        if (data.setup.nohelp){
            this.nohelp = data.setup.nohelp;
        } else {
            this.nohelp = new Array();
        }

        switch (this.currentType){
            case 'selectitem':{
                Ext.getCmp('setupcard-' + this.crStep).add({
                    xtype: 'form',
                    id: 'form_' + this.crStep,
                    border: false,
                    bodyCls: 'panel-filling',
                    scrollable: true,
                    items: [{
                        xtype: 'fieldset',
                        bodyCls: 'itemdescr-panel',
                        width: 440,
                        title: data.setup.title + ' (лимит ' + this.selNeed + ')',
                        id: 'fset_' + this.crStep
                    }]
                });

                panel.getLayout().setActiveItem(this.crStep);

                Ext.each(data.setup.items, function(item){
                    Ext.getCmp('fset_' + this.crStep).add({
                        xtype: 'checkbox',
                        boxLabel: item.text,
                        id: 'chosen_' + this.crStep + '_' + item.id,
                        listeners: {
                            change: this.onItemCheck,
                            scope: this
                        }
                    });
                }, this);

                break;
            }

            case 'skip':{
                Ext.getCmp('setupcard-' + this.crStep).add({
                    id: 'panel_' + this.crStep,
                    border: false,
                    bodyCls: 'itemdescr-panel',
                    scrollable: true,
                    html: '<h3>' + data.setup.title + '</h3>'
                });

                panel.getLayout().setActiveItem(this.crStep);

                break;
            }

            case 'finish':{
                Ext.getCmp('setupcard-' + this.crStep).add({
                    id: 'panel_' + this.crStep,
                    border: false,
                    bodyCls: 'itemdescr-panel',
                    scrollable: true,
                    html: '<h3>Поздравляем, ваш персонаж создан! Нажмите "Далее" для завершения.</h3>'
                });

                panel.getLayout().setActiveItem(this.crStep);

                this.lookupReference('cancel_button').disable();

                break;
            }
        }
        
        this.onSuccess();
    },

    onItemCheck: function(obj, newVal, oldVal){
        var id = obj.getId().split('_')[2];

        if (this.nohelp.indexOf(parseInt(id)) == -1){
            this.getHelp(id);
        } else {
            Ext.getCmp('setuphelp-' + this.crStep).update('Нету справки по этому объекту.');
        }

        if (newVal){
            if (this.selCount + 1 > this.selNeed){
                obj.setValue(false);
                this.selCount++
                return false;
            } else {
                this.selectedItems.push(id);
                this.selCount++;
            }
        } else {
            Ext.Array.remove(this.selectedItems, id);
            this.selCount--;
        }
    },

    getHelp: function(id){
        var data = {};
        var view = this.getView();
        var rm = view.requestManager;

        data.mode = 'gethelp';
        data.type = this.help;
        data.id = id;
        data.username = view.user.get('name');
        data.password = view.user.get('pass');
        data.cid = this.crCid;

        Ext.getCmp('setuphelp-' + this.crStep).mask(this.loadText);

        rm.go({
            url: 'creator.php',
            data: data,
            scope: this,
            success: this.onHelpSuccess,
            failure: this.onFailure
        });
    },

    onHelpSuccess: function(response){
        var data = Ext.JSON.decode(response.responseText);
        var panel = Ext.getCmp('setuphelp-' + this.crStep);

        switch (this.help){
            case 'powers':{
                var helptxt = data.helpobj[0].descr;
                break;
            }

            case 'feats':{
                var helptxt = data.helpobj[0].descr;
                break;
            }

            case 'spells':{
                var helptxt = data.helpobj[0].lvl + ' уровень, ' + data.helpobj[0].cat + '<br/><br/><b>Время накладывания: </b>' + data.helpobj[0].timing + '<br/><b>Дистанция: </b>' + data.helpobj[0].distance + '<br/><b>Компоненты: </b>' + data.helpobj[0].components + '<br/><b>Длительность: </b>' + data.helpobj[0].duration + '<br/><br/>' + data.helpobj[0].descr;
                break;
            }

            case 'weapons':{
                var helptxt = 'Категория: ' + data.helpobj[0].cat + '<br/>Урон: ' + data.helpobj[0].dice + '<br/>Тип урона: ' + data.helpobj[0].dmg_type + '<br/>Свойства: ' + data.helpobj[0].properties;
                break;
            }

            case 'armor':{
                var helptxt = 'Категория: ' + data.helpobj[0].cat + '<br/>КД: ' + data.helpobj[0].ac + '<br/>Штраф скрытности: ' + data.helpobj[0].stealth;
                break;
            }

            case 'equip':{
                var helptxt = data.helpobj[0].descr;
                break;
            }

            default:{
                var helptxt = 'Нету справки по этому объекту.';
            }
        }

        panel.update(helptxt);

        panel.unmask();
    },

    getStores: function(){
        var view = this.getView();
        var rm = view.requestManager;
        var settings = view.viewport.settings;
        var data = {
            mode: 'getstores'
        };

        view.up('window').mask(this.loadText);

        rm.go({
            url: 'creator.php',
            data: data,
            scope: this,
            success: this.onGetStores,
            failure: this.onFailure
        });
    },

    onGetStores: function(response){
        var data = Ext.JSON.decode(response.responseText);

        Ext.getStore('classesStore').loadRawData(data);
        Ext.getStore('racesStore').loadRawData(data);
        Ext.getStore('backsStore').loadRawData(data);

        this.onSuccess();
    },

    cancelCreator: function(){
        var data = {};
        var view = this.getView();
        var rm = view.requestManager;

        data.mode = 'cancel';
        data.username = view.user.get('name');
        data.password = view.user.get('pass');
        data.cid = this.crCid;

        view.up('window').mask(this.cancelText);

        rm.go({
            url: 'creator.php',
            data: data,
            scope: this,
            success: this.onCancelSuccess,
            failure: this.onFailure
        });
    },

    onCancelSuccess: function(){
        this.getView().up('window').close();
    }
    
});
