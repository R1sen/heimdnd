
Ext.define('DwD2.view.charcreator.Charcreator',{
    extend: 'Ext.panel.Panel',

    requires: [
        'DwD2.view.charcreator.CharcreatorController',
        'DwD2.view.charcreator.CharcreatorModel',
        'DwD2.view.charcreator.attrspinner.Attrspinner'
    ],

    controller: 'charcreator-charcreator',
    viewModel: {
        type: 'charcreator-charcreator'
    },

    bbar: [{
        text: 'Отменить создание',
        disabled: true,
        reference: 'cancel_button',
        handler: 'cancelCreator'
    }, '->', {
        text: 'Далее &raquo;',
        handler: 'nextStep'
    }],

    layout: 'card',
    id: 'charcreator-cardpanel',
    border: false,
    xtype: 'charcreator',

    items: [{
        layout: 'border',
        border: false,
        items: [{
            border: false,
            bodyCls: 'panel-filling',
            region: 'center',
            xtype: 'form',
            reference: 'creator_mainform',
            bodyPadding: 10,
            defaults: {
                anchor: '90%'
            },
            items: [{
                xtype: 'textfield',
                fieldLabel: 'Имя персонажа',
                name: 'char_name',
                reference: 'char_name',
                maxLength: 19,
                regex: /^[А-Яа-я\sёЁ]{1,19}$/,
                regexText: 'Допускаются только буквы русского алфавита и пробел, длиной не более 19 символов.',
                allowBlank: false
            },{
                xtype: 'radiogroup',
                fieldLabel: 'Пол',
                reference: 'char_gender',
                items: [{
                    xtype: 'radio',
                    boxLabel: 'Мужской',
                    name: 'char_gender',
                    inputValue: 'male',
                    labelWidth: 20,
                    checked: true
                },{
                    xtype: 'radio',
                    boxLabel: 'Женский',
                    name: 'char_gender',
                    labelWidth: 20,
                    inputValue: 'female'
                }]
            },{
                xtype: 'combobox',
                fieldLabel: 'Раса',
                editable: false,
                emptyText: 'Выберите расу..',
                queryMode: 'local',
                store: 'racesStore',
                displayField: 'name',
                valueField: 'id',
                name: 'char_race',
                triggerAction: 'all',
                allowBlank: false,
                listeners: {
                    select: 'onComboSelect',
                    scope: 'controller'
                },
                reference: 'char_race'
            },{
                xtype: 'combobox',
                fieldLabel: 'Класс',
                editable: false,
                emptyText: 'Выберите класс..',
                queryMode: 'local',
                store: 'classesStore',
                displayField: 'name',
                valueField: 'id',
                name: 'char_class',
                triggerAction: 'all',
                allowBlank: false,
                listeners: {
                    select: 'onComboSelect',
                    scope: 'controller'
                },
                reference: 'char_class'
            },{
                xtype: 'combobox',
                fieldLabel: 'Предыстория',
                editable: false,
                emptyText: 'Выберите предысторию..',
                queryMode: 'local',
                store: 'backsStore',
                displayField: 'name',
                valueField: 'id',
                name: 'char_back',
                triggerAction: 'all',
                allowBlank: false,
                listeners: {
                    select: 'onComboSelect',
                    scope: 'controller'
                },
                reference: 'char_back'
            },{
                xtype: 'combobox',
                fieldLabel: 'Мировоззрение',
                editable: false,
                emptyText: 'Выберите мировоззрение..',
                queryMode: 'local',
                store: 'alignmentList',
                displayField: 'name',
                valueField: 'name',
                name: 'char_alignment',
                triggerAction: 'all',
                allowBlank: false,
                listeners: {
                    select: 'onAlignmentSelect',
                    scope: 'controller'
                },
                reference: 'char_alignment'
            },{
                xtype: 'label',
                html: '<br/><h4><a onclick="Ext.getCmp(\'charcreator-cardpanel\').getController().loadAttrHelp()">Установка атрибутов характеристик</a></h4>(Расстановка без учёта рассовых бонусов)'
            },{
                fieldLabel: 'Очков',
                reference: 'attr_points',
                xtype: 'field',
                submitValue: false,
                fieldSubTpl: '<div id="attr-points">27</div>'
            },{
                xtype: 'attrspinner',
                fieldLabel: 'Сила',
                reference: 'char_str',
                name: 'char_str',
                listeners: {
                    change: 'onAttrChange',
                    scope: 'controller'
                }
            },{
                xtype: 'attrspinner',
                fieldLabel: 'Ловкость',
                reference: 'char_dex',
                name: 'char_dex',
                listeners: {
                    change: 'onAttrChange',
                    scope: 'controller'
                }
            },{
                xtype: 'attrspinner',
                fieldLabel: 'Телосложение',
                reference: 'char_con',
                name: 'char_con',
                listeners: {
                    change: 'onAttrChange',
                    scope: 'controller'
                }
            },{
                xtype: 'attrspinner',
                fieldLabel: 'Интеллект',
                reference: 'char_intl',
                name: 'char_intl',
                listeners: {
                    change: 'onAttrChange',
                    scope: 'controller'
                }
            },{
                xtype: 'attrspinner',
                fieldLabel: 'Мудрость',
                reference: 'char_wis',
                name: 'char_wis',
                listeners: {
                    change: 'onAttrChange',
                    scope: 'controller'
                }
            },{
                xtype: 'attrspinner',
                fieldLabel: 'Харизма',
                reference: 'char_cha',
                name: 'char_cha',
                listeners: {
                    change: 'onAttrChange',
                    scope: 'controller'
                }
            }]
        },{
            region: 'east',
            reference: 'details_panel',
            border: false,
            scrollable: true,
            bodyPadding: 5,
            bodyCls: 'itemdescr-panel',
            width: 440
        }]
    }],

    onRender: function(){
        this.callParent();

        this.getController().getStores();
    }
});
