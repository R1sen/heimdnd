Ext.define('DwD2.view.switchloc.SwitchlocController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.switchloc-switchloc',

	clearList: function (){
		this.lookupReference('switchloc-fs').removeAll();
	},

	getExits: function(){
		var view = this.getView();
		var rm = view.requestManager;
		var settings = view.viewport.settings;
		var data = {
			ssid: settings.get('ssid'),
			mode: 0
		};

		rm.go({
			url: 'switchloc.php',
			data: data,
			scope: this,
			success: this.onGetExitsSuccess,
			failure: this.onGetExitsFailure
		});	
	},

	onGetExitsSuccess: function(response){
		var data = Ext.JSON.decode(response.responseText);
		var fset = this.lookupReference('switchloc-fs');

		this.clearList();

		Ext.each(data.exits, function(exit){
			fset.add({
				xtype: 'displayfield',
				value: exit.name + ':'
			},{
				xtype: 'button',
				iconCls: 'loctype' + exit.loctype,
				id: 'goto-' + exit.lid,
				listeners: {
					click: 'onGoToClick',
					scope: this
				},
				tooltip: 'Перейти - ' + exit.name
			});
		}, this);
	},

	onGetExitsFailure: function(){
		return;
	},

	onGoToClick: function(btn){
		var lid = btn.getId().substr(5);

		btn.disable();

		this.fireViewEvent('exitloc', lid);
	}
});
