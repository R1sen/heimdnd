
Ext.define("DwD2.view.switchloc.Switchloc",{
    extend: "Ext.window.Window",

    controller: "switchloc-switchloc",
    viewModel: {
        type: "switchloc-switchloc"
    },

	requires: [
		'DwD2.view.switchloc.SwitchlocController',
		'DwD2.view.switchloc.SwitchlocModel'
	],

	title: 'Переход в другую локацию',
	alias: 'widget.switchloc',
	resizable: false,
	draggable: false,
	maximizable: true,
	autoShow: true,
	width: 450,
	height: 400,
	layout: 'fit',
	
	items: [{
		layout: {
			// layout-specific configs go here
			type: 'accordion',
			animate: true
		},

		items: [{
			title: 'Переход',
			scrollable: true,
			layout: 'form',
			//bodyStyle: 'padding:15px;background-color:#DFE8F6;font:12px verdana,tahoma,arial,sans-serif;',
			bodyCls: 'switchloc-form',
			items: [{
				title: 'Список локаций доступных для перехода',
				xtype: 'fieldset',
				layout: {
					type: 'table',
					columns: 2,
					tableAttrs: {
						style: {
							width: '100%'
						}
					}
				},
				labelWidth: 250,
				columns: 1,
				reference: 'switchloc-fs'
			}]
		},{
			title: 'Карта города',
			autoScroll: true,
			items: [{
				xtype: 'image',
				alt: 'Карта города',
				src: 'resources/data/images/map.jpg'
			}]
		},{
			title: 'Карта страны',
			autoScroll: true,
			items: [{
				xtype: 'image',
				alt: 'Карта местности',
				src: 'resources/data/images/bigmap.jpg'
			}]
		}]
	}],

	onRender: function(){
		this.callParent();

		this.getController().getExits();
	}
});
