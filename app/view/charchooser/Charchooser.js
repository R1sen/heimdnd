
Ext.define("DwD2.view.charchooser.Charchooser",{
    extend: "Ext.window.Window",

    controller: "charchooser-charchooser",
    viewModel: {
        type: "charchooser-charchooser"
    },

	requires: [
		'DwD2.view.charchooser.CharchooserController',
		'DwD2.view.charchooser.CharchooserModel'
	],

    title: Ext.get('page-title').dom.innerHTML,
	bodyPadding: 10,
    closable: false,
	autoShow: true,
	alias: 'widget.charchooser',
	resizable: false,
	draggable: false,
	width: 400,
	height: 300,
	scrollable: true,

    items: [{
		xtype: 'form',
		reference: 'charchooser-form',
		scrollable: true,
		border: false,
		bodyCls: 'panel-filling',
		
		items: [{
			xtype: 'fieldset',
			reference: 'charchooser-fieldset',
			title: 'Выбор персонажа',
			columns: 1,
			defaultType: 'radio',

			items: [{
				boxLabel: 'Создать нового',
				name: 'cid',
				inputValue: 0,
				checked: true
			}]
		}]		
	}],

	buttons: [{
		text: 'Назад',
		listeners: {
			click: 'onBackClick'
		}
	},{
		text: 'Выбрать',
		listeners: {
			click: 'onSubmitClick'
		}
	}],

	onRender: function(){
		this.callParent();

		if (this.user.get('chars')){
			this.getController().populateList(this.user);
		}
	}
});
