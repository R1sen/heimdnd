Ext.define('DwD2.view.charchooser.CharchooserController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.charchooser-charchooser',

	onBackClick: function(){
		this.fireViewEvent('back');
	},

	onSubmitClick: function(){
		var form = this.lookupReference('charchooser-form');
        
        if (form.isValid()) {
			var data = form.getValues();
			var view = this.getView();
			var rm = view.requestManager;

			if (data.cid == 0){
				Ext.getCmp('main-viewport').fireEvent('createchar', view.user);
				return;
			}

			view.hide();

			data.username = view.user.get('name');
			data.password = view.user.get('pass');

			rm.go({
				url: 'login.php',
				data: data,
				scope: this,
				success: this.onChooseSuccess,
				failure: this.onChooseFailure
			});			
        }
	},

	onChooseSuccess: function(response) {
		var obj = Ext.JSON.decode(response.responseText);
		var view = this.getView();
		var user = view.user;

		var settingsModel = Ext.create('DwD2.model.Settings');

		var settingsSet = settingsModel.getProxy().getReader().read(response, {
                recordCreator: this.getSession().recordCreator
        });

		this.fireViewEvent('choose', user, settingsSet.getRecords()[0]);

		settingsSet.destroy();
		view.destroy();
    },

	onChooseFailure: function() {
		return;
	},

	populateList: function(user) {
		var fset = this.getView().lookupReference('charchooser-fieldset');

		Ext.each(user.get('chars'), function(character){
			fset.add({
				boxLabel: character.name, 
				name: 'cid', 
				inputValue: character.cid
			});
		});
	}
});
