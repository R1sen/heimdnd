
Ext.define('DwD2.RequestManager', {
	mixins: ['Ext.mixin.Observable'],

    constructor: function (config) {
        // The Observable constructor copies all of the properties of `config` on
        // to `this` using Ext.apply. Further, the `listeners` property is
        // processed to add listeners.
        //
        this.mixins.observable.constructor.call(this, config);
    },

	go: function(options) {
        Ext.Ajax.request({
            url: 'resources/server/' + options.url,
            method: 'POST',
            params: options.data,
            scope: this,
            callback: this.onReturn,
            original: options
        });
    },

	onReturn: function(options, success, response) {
        var options = options.original;

		if (response.responseText == ""){
			if (options.inmess) Ext.callback(options.failure, options.scope, [response]);
			return;
		}

		if (response.responseText.charAt(0) != "{"){
			this.onFailure(options, response);
			return;
		}
                
        if (success) {
			var obj = Ext.JSON.decode(response.responseText);

			if (obj.success){
				Ext.callback(options.success, options.scope, [response]);
				return;
			}
        }

		this.onFailure(options, response);
    },

	onFailure: function(options, response) {
		if (response.responseText.charAt(0) != "{"){
			this.fireEvent('error', 'Ошибка ответа от сервера.<br/>' + response.responseText, options.failure, options.scope);
		} else {
			var obj = Ext.JSON.decode(response.responseText);
			
			this.fireEvent('error', obj.error, options.failure, options.scope);
		}
    }
});
