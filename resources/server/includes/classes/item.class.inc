<?php

if (!defined("_IN_DWD_")): die; endif;

class Item
{
    
    public $id = 0;
    public $icon = '';
    public $descr = '';
    public $name = '';
    public $base = '';

    public $base_id = 0;

    public $enchant_id = 0;
    public $weight = 0;
    public $cost = 0;
    public $container = 0;

    public $base_object;
    public $enchant_object;
    public $enchant_effects;

    function __construct($item_type = null, $triggers = false, $id = 0)
    {

        if ($this->id == 0 && $item_type == null) return;

        if ($this->base_id == 0 && $item_type != 'unarmed' && $item_type != "improvised melee" && $item_type != "improvised ranged"){

            $this->base_object = (object)['weight' => $this->weight, 'cost' => $this->cost];
            //return;
        }

        if ($item_type == 'unarmed') {
            $this->base_object = (object)['name' => 'Безоружный удар', 'cat' => 'simple melee', 'dice' => '1d1', 'dmg_type' => 'bludgeoning', 'properties' => []];
            $this->name = "Безоружный удар";
        } elseif ($item_type == "improvised melee"){
            $this->base_object = (object)['name' => 'Импровизированное оружие (ближний бой)', 'cat' => 'simple melee', 'dice' => '1d4', 'dmg_type' => 'variant', 'properties' => []];
            $this->name = "Импровизированное оружие (ближний бой)";
        } elseif ($item_type == "improvised ranged"){
            $this->base_object = (object)['name' => 'Импровизированное оружие (дальний бой)', 'cat' => 'simple ranged', 'dice' => '1d4', 'dmg_type' => 'variant', 'properties' => ["Метательное (дис. 20/60)"]];
            $this->name = "Импровизированное оружие (дальний бой)";
        } else {

            $this->get_base();

            if ($this->enchant_id != 0 && $triggers == true) {

                $this->get_enchant_info($this->enchant_id);

            }
        }

    }

    function get_base()
    {
        global $dbh, $errors;

        switch ($this->base) {
            case 'armor':
                $sth = $dbh->prepare('SELECT * FROM armor WHERE armor.id = ?');
                $sth->execute([$this->base_id]);

                $this->base_object = $sth->fetch(PDO::FETCH_OBJ);
                break;


            case 'weapon':
                $sth = $dbh->prepare('SELECT * FROM weapons WHERE weapons.id = ?');
                $sth->execute([$this->base_id]);

                $this->base_object = $sth->fetch(PDO::FETCH_OBJ);

                $this->base_object->properties = explode(', ', mb_strtolower($this->base_object->properties));

                break;


            case 'equipment':
                $sth = $dbh->prepare('SELECT * FROM equip WHERE equip.id = ?');
                $sth->execute([$this->base_id]);

                $this->base_object = $sth->fetch(PDO::FETCH_OBJ);

                break;
        }
    }

    function get_enchant_info($enchant_id)
    {
        global $dbh;

        $sth = $dbh->prepare('SELECT * FROM enchants WHERE enchants.id = ?');
        $sth->execute([$enchant_id]);

        $this->enchant_object = $sth->fetch(PDO::FETCH_OBJ);



        $sth = $dbh->prepare('SELECT events.effect, events.when_active FROM events WHERE events.type_id = ? AND events.type = ?');
        $sth->execute([$enchant_id, 'enchant']);

        $this->enchant_effects = $sth->fetchall(PDO::FETCH_ASSOC);

    }

    function get_item_plug($type, $sex)
    {

        global $dbh, $errors, $pics;

        $this->base = $type;

        switch ($this->base) {

            case 'head':

                $this->icon = $sex.'_'.$pics['head'];
                $this->descr = 'Пусто';
                $this->name = 'Голова';
                break;


            case 'eyes':

                $this->icon = $sex.'_'.$pics['eyes'];
                $this->descr = 'Пусто';
                $this->name = 'Глаза';
                break;


            case 'neck':

                $this->icon = $sex.'_'.$pics['neck'];
                $this->descr = 'Пусто';
                $this->name = 'Шея';
                break;

            case 'coat':

                $this->icon = $sex.'_'.$pics['coat'];
                $this->descr = 'Пусто';
                $this->name = 'Плечи';
                break;

            case 'armor':

                $this->icon = $sex.'_'.$pics['armor'];
                $this->descr = 'Пусто';
                $this->name = 'Броня';
                break;

            case 'torso':

                $this->icon = $sex.'_'.$pics['torso'];
                $this->descr = 'Пусто';
                $this->name = 'Торс';
                break;

            case 'hands':

                $this->icon = $sex.'_'.$pics['hands'];
                $this->descr = 'Пусто';
                $this->name = 'Ладони';
                break;

            case 'arms':

                $this->icon = $sex.'_'.$pics['arms'];
                $this->descr = 'Пусто';
                $this->name = 'Предплечья';
                break;

            case 'waist':

                $this->icon = $sex.'_'.$pics['waist'];
                $this->descr = 'Пусто';
                $this->name = 'Пояс';
                break;

            case 'feet':

                $this->icon = $sex.'_'.$pics['feet'];
                $this->descr = 'Пусто';
                $this->name = 'Ноги';
                break;

            case 'ring':

                $this->icon = $sex.'_'.$pics['ring'];
                $this->descr = 'Пусто';
                $this->name = 'Кольцо';
                break;

            case 'wondrous':

                $this->icon = $sex.'_'.$pics['wondrous'];
                $this->descr = 'Пусто';
                $this->name = 'Чудесный предмет';
                break;

            case 'mainhand':

                $this->icon = $sex.'_'.$pics['mainhand'];
                $this->descr = 'Пусто';
                $this->name = 'Основная рука';
                break;

            case 'offhand':

                $this->icon = $sex.'_'.$pics['offhand'];
                $this->descr = 'Пусто';
                $this->name = 'Вторая рука';
                break;


        }
    }
    
    function change_owner($new){
        global $dbh, $errors;
        
        $sth = $dbh->prepare('UPDATE char_items SET char_id  = ? WHERE item_id = ?');
        $sth->execute([$new->id, $this->id]);
    }

    function throw_away($lid){
        global $dbh, $errors;

        $sth = $dbh->prepare('DELETE FROM char_items WHERE item_id = ?');
        $sth->execute([$this->id]);
    }
    
    function create_and_give($cid){
        global $dbh, $errors;

        if (!file_exists("../data/items/" . translit($this->icon))) $this->random_icon();

        $sth = $dbh->prepare('INSERT INTO items (name, descr, icon, base, base_id, enchant_id, container, weight, cost) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)');
        $sth->execute([$this->name, $this->descr, translit($this->icon), $this->base, $this->base_id, $this->enchant_id, $this->container, $this->weight, $this->cost]);
        $num = $sth->rowCount();

        if ($num == 0){
            send_error($errors["e_98"]);
        }

        $this->id = $dbh->lastInsertId();

        $sth = $dbh->prepare('INSERT INTO char_items (char_id, item_id) VALUES (?, ?)');
        $sth->execute([$cid, $this->id]);
        $num = $sth->rowCount();

        if ($num == 0){
            send_error($errors["e_99"]);
        }
    }

    function random_icon(){
        $limit = 1;
        $ex = false;

        while (file_exists("../data/items/" . translit($this->name) . $limit . ".png")){
            $limit++;
            $ex = true;
        }

        if (!$ex) while (file_exists(iconv('utf-8', 'cp1251', "../data/items/" . translit($this->name) . $limit . ".png"))){
            $limit++;
            $ex = true;
        }

        if (!$ex){
            $this->icon = 'empty.png';
            return;
        }

        $this->icon = translit($this->name) . mt_rand(1, $limit - 1) . ".png";
    }

/*
    function get_enchant_info($enchant_id, $when_array)
    {
        global $dbh;

        $sth = $dbh->prepare('SELECT * FROM enchants WHERE enchants.id = ?');
        $sth->execute([$enchant_id]);

        $this->enchant_object = $sth->fetch(PDO::FETCH_OBJ);

        $in = str_repeat('?,', count($when_array) - 1) . '?';


        $sth = $dbh->prepare('SELECT * FROM events WHERE events.type_id = ? AND events.type = ? AND events.when_active IN (' . $in . ')');
        $sth->execute(array_merge([$enchant_id, 'enchant'], $when_array));

        $this->enchant_effects = $sth->fetchall(PDO::FETCH_ASSOC);

    }
*/
}

?>