<?php

if (!defined("_IN_DWD_")): die; endif;

class Message{

	public $id = 0;
	public $cid = 0;
	public $private_cid = 0;
	public $mtime = 0;
	public $type = 0;
	public $lid = 0;
	public $body = "";

	public $mess = array();

	function __construct($cid = 0, $body = "", $lid = 0, $type = 0, $private_cid = 0){
		if ($cid === 0) return;

		$this->cid = $cid;
		$this->mtime = time();
		$this->body = $body;
		$this->lid = $lid;
		$this->type = $type;
		$this->private_cid = $private_cid;
	}

	function post_msg(){
		if (($this->cid == 0) || ($this->body == "")){ 
			return 0;
		} else {
			global $errors, $dbh;

			//"INSERT INTO mess (cid, body, mtime, type, lid, private_cid) VALUES (".$this->cid.", '".$this->body."', ".$this->mtime.", ".$this->type.", ".$this->lid.", ".$this->private_cid.")");
			$sth = $dbh->prepare("INSERT INTO mess (cid, body, mtime, type, lid, private_cid) VALUES (?, ?, ?, ?, ?, ?)");
			$sth->execute(array($this->cid, $this->body, $this->mtime, $this->type, $this->lid, $this->private_cid));
			$num = $sth->rowCount();

			if ($num != 1){
				send_error($errors["e_14"]);
			}

			return 1;
		}
	}

	//дальше новое, не проверено!!!!

	/*function get_exp_mess(){
		global $e_119, $errors, $dbh;
		$out = array();

		//$query = "SELECT mess.body, mess.mid, mess.mtime, chars.name FROM mess, chars WHERE mess.cid = chars.cid AND (mess.type = 28 OR mess.type = 31) ORDER BY mess.mtime ASC";
		$sth = $dbh->query("SELECT mess.body, mess.id, mess.mtime, chars.name FROM mess, chars WHERE mess.cid = chars.id AND (mess.type = 28 OR mess.type = 31) ORDER BY mess.mtime ASC");
		
		if ($sth->rowCount() == 0){
			
			return false;
		}

		while ($exp = $sth->fetch(PDO::FETCH_ASSOC)){
			$body = preg_split("/;/", $exp["body"]);

			if ($body[2] == 'Социальная игра') continue;

			//$out[] = array("mid" => $exp["mid"], "mtime" => $exp["mtime"], "dm" => $exp["name"], "xp" => $body[0], "char" => $body[1], "reason" => $body[2]);
			$out[] = array("mid" => $exp["id"], "mtime" => $exp["mtime"], "dm" => $exp["name"], "xp" => $body[0], "char" => $body[1], "reason" => $body[2]);
		}

		return $out;
	}*/
}
