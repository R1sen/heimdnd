<?php

/**
 * Created by PhpStorm.
 * User: gg_truschalov
 * Date: 26.09.2016
 * Time: 11:36
 */

if (!defined("_IN_DWD_")): die; endif;

Class Map
{

    function __construct()
    {


    }

    function update_conf($conf)
    {
        global $errors, $dbh;

        if (json_decode(stripslashes($conf)) != null) {
            $sth = $dbh->prepare('UPDATE maps SET confstring = ? WHERE id = ?');
            $sth->execute([$conf, $this->id]);

            $this->confstring = $conf;
            $this->get_active();
        } else {
            send_error($errors["e_71"]);
        }
    }

    function get_active()
    {
        $conf = json_decode($this->confstring, true);

        usort($conf["CharProto"], "cmp");
        //var_dump($conf["CharProto"]);
        if (isset($conf["CharProto"][$conf["Scena"][0]["active"]]["name"]))
            $this->active = $conf["CharProto"][$conf["Scena"][0]["active"]]["name"];
        else $this->active = '';

    }

    function sync_tokens($conf, $char)
    {
        global $host;

        $confstr = json_decode($conf, true);

        foreach ($confstr["CharProto"] as $key => $chr) {
            $npc_regex = "/(\W*)\s?(\d{1,2})?$/";

            preg_match($npc_regex, $chr["name"], $res);

            // print_r($res);

            if (!isset($res[2])) {
                $token = $char->get_token($chr["name"]);

                if ($token != false) {
                    $confstr["CharProto"][$key]["pic"] = $host . "resources/data/faces/t/" . $token;
                } else {
                    $npc = new Npc('empty');
                    $token = $npc->get_token($res[1]);

                    if ($token != false) {
                        $confstr["CharProto"][$key]["pic"] = $host . "resources/data/faces/npc/nt/" . preg_replace("/ /", "_", trim(translit($token))) . ".png";
                    }
                }

            } else {
                $npc = new Npc('empty');
                $token = $npc->get_token($res[1]);

                if ($token != false) {
                    $confstr["CharProto"][$key]["pic"] = $host . "resources/data/faces/npc/nt/" . preg_replace("/ /", "_", trim(translit($token))) . ".png";
                }
            }
        }

        return json_encode($confstr);
    }

    function update()
    {
        global $errors, $dbh;

        $sth = $dbh->prepare('UPDATE maps SET name = ?, description = ? WHERE id = ?');
        $sth->execute([$this->name, $this->description, $this->id]);

        return true;
    }

    function delete()
    {
        global $dbh;

        $sth = $dbh->prepare('DELETE FROM maps WHERE id = ?');
        $sth->execute([$this->id]);
    }

}

function cmp($a, $b)
{
    if ($a["initiative"] == $b["initiative"])
        return 0;

    return $a["initiative"] > $b["initiative"] ? -1 : 1;
}

?>