<?php

if (!defined("_IN_DWD_")): die; endif;

class User{

	public $id = 0;
	public $name = "";
	public $pass = "";
	public $regdate = 0;
	public $email = "";

	//dynamic set
	public $chars = array();

	function __construct($mod, $name, $pass)
	{
		global $errors, $dbh_forum, $forum_prefix;

		if($mod == 0) //TODO: Коннект к форуму и запрос данных?
		{
			//TODO: 
			//SELECT user_password, user_regdate, user_email FROM ".$forum_prefix."users WHERE username = '$name'
			conGo_forum();

			$sth = $dbh_forum->prepare("SELECT user_password, user_regdate, user_email FROM ".$forum_prefix."users WHERE username = ?");
			$sth->execute([$name]);
			$count = $sth->rowCount();

			if ($count != 1){
				send_error($errors["e_5"]);
			}

			$user = $sth->fetch(PDO::FETCH_ASSOC);

			if (!phpbb_check_hash($pass, $user["user_password"]) && !check($pass, $user["user_password"])){
				send_error($errors["e_1"]);
			} else {
				$this->name = $name;
				$this->regdate = $user["user_regdate"];
				$this->email = $user["user_email"];
				$this->pass = $user["user_password"];

				conGo_out_forum();

				$this->id = $this->create_new();

				return;
			}
		}
		else
		{
			if (!phpbb_check_hash($pass, $this->pass) && !check($pass, $this->pass)){
				send_error($errors["e_1"]);
			}
			
			/*TODO: Проверка на забаненного
			if(this->banned == 1)
			{
				//TODO Лог ошибки
				$dbh = null;
				die;
			}*/

			$this->pass = $pass;
		}
	
	}

	function get_chars(){
		global $dbh;

		$chars = array();
	
		$sth = $dbh->prepare("SELECT name, id FROM chars WHERE uid = ?");
		$sth->execute(array($this->id));
		$chars_count = $sth->rowCount(); 

		if ($chars_count == 0){
			return false;
		}

		while ($char = $sth->fetch(PDO::FETCH_ASSOC))
			//$chars[] = array("name" => $char["name"], "cid" => $char["id"], "xp" => $char["xp"], "inactive" => $char["inactive"]);
			$chars[] = array("name" => $char["name"], "cid" => $char["id"]);

		$this->chars = $chars;
	}

	function create_new(){
		global $errors, $dbh;

		$sth = $dbh->prepare("INSERT INTO users (name, pass, email, regdate) VALUES (?, ?, ?, ?)");
		$sth->execute(array($this->name, $this->pass, $this->email, time()));
		$new_count = $sth->rowCount();
				
		if ($new_count != 1){
			send_error($errors["e_2"]);
		}

		//МОЖНО ПРОЩЕ!

		return $dbh->lastInsertId();

		/*$sth = $dbh->prepare("SELECT id FROM users WHERE name = ? AND pass = ?");
		$sth->execute(array($this->name, $this->pass));
		$num = $sth->rowCount();

		if ($num == 0){
			send_error(array($errors["e_3"], mysql_error()));
		}

		$user = $sth->fetch(PDO::FETCH_ASSOC)

		return $user["uid"];*/
	}
 
}