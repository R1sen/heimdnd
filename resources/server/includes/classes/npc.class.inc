<?php

if (!defined("_IN_DWD_")): die; endif;

class Npc{

    public $id = 0;
    public $name;
    public $ac = 0;
    public $init = 0;

    public $npc_tree = [];

    public $size = 1;

    public $saves = null;

    public $str_save = 0;
    public $con_save = 0;
    public $dex_save = 0;
    public $intl_save = 0;
    public $wis_save = 0;
    public $cha_save = 0;

    public $dmg_immune;
    public $vulnerab;
    public $resist;

    public $penalty_weight;
    public $penalty_armor;

    public $mmdesc = '';

    function __construct($plug = null, $diff = 0)
    {

        if ($plug == 'empty') {

            $this->name = 'неопределённую цель';
            $this->ac = $diff;

        } elseif ($plug == 'emptydmg'){
            
            $this->name = 'неопределённой цели';
            $this->ac = $diff;

        } else {

            if ($this->saves != '-' && $this->saves != '') {

                global $errors;

                $this->saves_array = explode(', ', $this->saves);

                $regex = '/(сил|тел|ловк|инт|мудр|хар)\s\+(.*)/ui';

                foreach ($this->saves_array as $save) {

                    if (!preg_match($regex, $save, $res)) {
                        send_error($errors['e_63']);
                    }

                    switch ($res[1]) {

                        case 'Сил':
                        case 'сил':

                            $this->str_save += $res[2];

                            break;

                        case 'Тел':
                        case 'тел':

                            $this->con_save += $res[2];

                            break;

                        case 'Ловк':
                        case 'ловк':

                            $this->dex_save += $res[2];

                            break;

                        case 'Инт':
                        case 'инт':

                            $this->intl_save += $res[2];

                            break;

                        case 'Мудр':
                        case 'мудр':

                            $this->wis_save += $res[2];

                            break;

                        case 'Хар':
                        case 'хар':

                            $this->cha_save += $res[2];

                            break;

                        default: send_error($errors["e_66"]);
                    }

                }

            }

            $this->fill_std_saves();
            $this->get_size();

        }

    }

    function get_size(){

        global $errors;

        $regex = '/(Кро|Мал|Сред|Больш|Огром|Гром)/ui';

        if (!preg_match($regex, $this->type, $res))
            send_error($errors['e_74']);

        switch ($res[0]) {

            case 'Кро':
            case 'Мал':
            case 'Сред':

                $this->size = 1;

                break;

            case 'Больш':

                $this->size = 2;

                break;

            case 'Огром':

                $this->size = 3;

                break;

            case 'Гром':

                $this->size = 4;

                break;

            default: send_error($errors["e_75"]);
        }

    }


    function fill_std_saves(){
        $eng = new Engine();

        if ($this->str_save == 0){
            $this->str_save += $eng->get_mod($this->str);
        }

        if ($this->dex_save == 0){
            $this->dex_save += $eng->get_mod($this->dex);
        }

        if ($this->con_save == 0){
            $this->con_save += $eng->get_mod($this->con);
        }

        if ($this->intl_save == 0){
            $this->intl_save += $eng->get_mod($this->intl);
        }

        if ($this->wis_save == 0){
            $this->wis_save += $eng->get_mod($this->wis);
        }

        if ($this->cha_save == 0){
            $this->cha_save += $eng->get_mod($this->cha);
        }

    }


    function arrange_npc(){
        global $dbh, $errors;

        $sth = $dbh->prepare('SELECT id, name, cat FROM mm ORDER BY name ASC');
        $sth->execute([]);
        $num = $sth->rowCount();

        if (!$num){
            send_error($errors["e_53"]);
        }

        $npc_tree = ["text" => "NPC", "cls" => "folder", "expanded" => true, "children" => []];
        $npc_monsters = ["text" => "Монстры", "cls" => "folder", "children" => []];
        $npc_critters = ["text" => "Животные", "cls" => "folder", "children" => []];
        $npc_humanoids = ["text" => "Гуманоиды", "cls" => "folder", "children" => []];
        $npc_monsters_cats = [];
        $npc_critters_cats = [];

        while ($npc = $sth->fetch(PDO::FETCH_ASSOC)){
            if ($npc["cat"] == 'Гуманоид'){
                $npc_humanoids["children"][] = ["text" => $npc["name"], "leaf" => true, "id" => $npc["id"]];
            } elseif (mb_strstr($npc["cat"], 'Животные: ')){
                $npc_critters_cats[mb_substr($npc["cat"], 10)][] = ["name" => $npc["name"], "id" => $npc["id"]];
            } else {
                $npc_monsters_cats[$npc["cat"]][] = ["name" => $npc["name"], "id" => $npc["id"]];
            }
        }

        foreach ($npc_critters_cats as $cat => $npc){
            $list = [];

            foreach ($npc as $one){
                $list[] = ["text" => $one["name"], "leaf" => true, "id" => $one["id"]];
            }

            $npc_critters["children"][] = ["text" => $cat, "cls" => "folder", "children" => $list];
        }

        foreach ($npc_monsters_cats as $cat => $npc){
            $list = [];

            foreach ($npc as $one){
                $list[] = ["text" => $one["name"], "leaf" => true, "id" => $one["id"]];
            }

            $npc_monsters["children"][] = ["text" => $cat, "cls" => "folder", "children" => $list];
        }

        $npc_tree["children"][] = $npc_monsters;
        $npc_tree["children"][] = $npc_critters;
        $npc_tree["children"][] = $npc_humanoids;

        $this->npc_tree = $npc_tree;
    }

    function settle($name, $lid, $hp, $owner){
        global $errors, $dbh;

        if (!is_numeric($hp)){
            $eng = new Engine();

            $hp = preg_replace("/к/", "d", $hp);
            $hp = preg_replace("/д/", "d", $hp);

            $regex = "/^(\d{1,2})d(\d{1,3})\s((\+|\-)\s(\d{1,2}))?$/";

            if (!preg_match($regex, $hp, $res)){
                send_error($errors["e_55"]);
            }

            $td = $res[1]."d".$res[2];
            $throw = $eng->dice($td);
            $check = $throw["int"];

            if ($res[4] == '+'){
                $check += $res[5];
            } elseif ($res[4] == '-'){
                $check -= $res[5];
            }
        } else {
            $check = $hp;
        }

        $sth = $dbh->prepare("INSERT INTO mm_locs (name, lid, nid, hp, total_hp, owner) VALUES (?, ?, ?, ?, ?, ?)");
        $sth->execute([$name, $lid, $this->id, $check, $check, $owner]);
        $num = $sth->rowCount();

        if ($num != 1){
            send_error($errors["e_56"]);
        }
    }

    function save_hp(){
        global $dbh;

        $sth = $dbh->prepare("UPDATE mm_locs SET hp = ?, temp_hp = ? WHERE id = ?");
        $sth->execute([$this->hp, $this->temp_hp, $this->settled_id]);
    }

    function unsettle(){
        global $dbh, $errors;

        $sth = $dbh->prepare("DELETE FROM mm_locs WHERE id = ?");
        $sth->execute([$this->settled_id]);
        $num = $sth->rowCount();

        if ($num != 1){
            send_error($errors["e_61"]);
        }
    }

    function get_token($name){
        global $dbh;

        $sth = $dbh->prepare("SELECT nid FROM mm_locs WHERE name = ?");
        $sth->execute([$name]);
        $num = $sth->rowCount();

        if ($num == 0){
            return false;
        }

        $npc = $sth->fetch(PDO::FETCH_ASSOC);

        $forge = new Constructor();

        $mm = $forge->get_npc_by_id($npc["nid"]);

        return $mm->name;
    }

    function get_mmdesc(){
        global $dbh;

        $sth = $dbh->prepare("SELECT descr FROM mmdesc WHERE mmid = ?");
        $sth->execute([$this->id]);
        $num = $sth->rowCount();

        if ($num != 1){
            return false;
        }

        $this->mmdesc = $sth->fetch(PDO::FETCH_ASSOC)["descr"];
    }

}
?>