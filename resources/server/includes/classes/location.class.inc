<?php

if (!defined("_IN_DWD_")): die; endif;

class Location
{

    public $id = 0;
    public $name = "";
    public $topic = "";
    public $loctype = 0;

    public $exits = array();

    public $users = array();
    public $mess = array();

    function get_last_msgs($lastmid, $cid, $dm = 0)
    {
        global $dbh;

        if (!$lastmid) { //get pack of 20 last messages in channel
            //"SELECT mess.mid, mess.cid, mess.mtime, mess.type, mess.body, mess.private_cid, chars.name, chars.color FROM chars, mess WHERE mess.cid = chars.cid AND mess.lid = $this->lid ORDER BY mess.mid DESC LIMIT 0,20"
            $sth = $dbh->prepare("SELECT mess.id, mess.cid, mess.mtime, mess.type, mess.body, mess.private_cid, chars.name, chars.color FROM chars, mess WHERE mess.cid = chars.id AND mess.lid = ? ORDER BY mess.id DESC LIMIT 0,20");
            $sth->execute(array($this->id));
        } else { //get last new messages, over $lastmid
            //$query = "SELECT mess.mid, mess.cid, mess.mtime, mess.type, mess.body, mess.private_cid, chars.name, chars.color FROM chars, mess WHERE mess.mid > $lastmid AND mess.cid = chars.cid AND mess.lid = $this->lid ORDER BY mess.mid DESC";
            $sth = $dbh->prepare("SELECT mess.id, mess.cid, mess.mtime, mess.type, mess.body, mess.private_cid, chars.name, chars.color FROM chars, mess WHERE mess.id > ? AND mess.cid = chars.id AND mess.lid = ? ORDER BY mess.id DESC");
            $sth->execute(array($lastmid, $this->id));
        }

        $num = $sth->rowCount();
        $temp = array();
        $j = 0;

        if ($num == 0) return 0;
        else
            while ($temp[] = $sth->fetch(PDO::FETCH_ASSOC))
                $j++;

        for ($i = $j - 1; $i >= 0; $i--)
            if (($temp[$i]["type"] == 3 && $temp[$i]["private_cid"] != $cid && $temp[$i]["cid"] != $cid) || ($temp[$i]["type"] == 19 && $temp[$i]["cid"] != $cid && !$dm))
                continue;
            else
                $this->mess[] = $temp[$i];

        return $out = array("num" => $j, "lastmid" => $temp[0]["id"]);
    }

    function get_body($i)
    {

        switch ($this->mess[$i]['type']) {
            case '6': {
                $body = explode(';', $this->mess[$i]['body']);
                return ['check' => $body[0], 'dice' => $body[1], 'diff' => $body[2], 'attr_mod' => $body[3], 'mod' => $body[4], 'penalty' => $body[5], 'total' => $body[6], 'result' => $body[7]];
            }

            case '7': {
                $body = explode(';', $this->mess[$i]['body']);
                return ['dice' => $body[0], 'attr_mod' => $body[1], 'mod' => $body[2], 'total' => $body[3]];
            }

            case '10': {
                $body = explode(';', $this->mess[$i]['body']);
                return ['what' => $body[0], 'whom' => $body[1]];
            }

            case '17':
            case '12': {
                $body = explode(';', $this->mess[$i]['body']);
                return ['id' => $body[0], 'title' => $body[1]];
            }

            case '13': {
                $body = explode(';', $this->mess[$i]['body']);
                return ['dice' => $body[0], 'result' => $body[1]];
            }

            case '15': {
                $body = explode(';', $this->mess[$i]['body']);
                return ['target' => $body[0], 'slot' => $body[1], 'sid' => $body[2], 's_name' => $body[3]];
            }

            case '16': {
                $body = explode(';', $this->mess[$i]['body']);
                return ['target' => $body[0], 'dice' => $body[1], 'diff' => $body[2], 'attr_mod' => $body[3], 'mod' => $body[4], 'penalty' => $body[5], 'weapon' => $body[6], 'total' => $body[7], 'result' => $body[8]];
            }

            case '19':
            case '18': {
                $body = explode(':;:', $this->mess[$i]['body']);
                return ['dice' => $body[0], 'check' => $body[1], 'reason' => $body[2]];
            }

            case '20': {
                $body = explode(';', $this->mess[$i]['body']);
                return ['target' => $body[0], 'dice' => $body[1], 'attr_mod' => $body[2], 'mod' => $body[3], 'trait' => $body[4], 'weapon' => $body[5], 'total' => $body[6], 'type_dmg' => $body[7]];
            }

            case '21':{
                $body = explode(':;:', $this->mess[$i]["body"]);

                return array("text" => $body[0], "name" => $body[1], "color" => $body[2]);
            }

            case '23':{
                $body = explode(';', $this->mess[$i]["body"]);

                return array("target" => $body[1], "t_hp" => $body[0]);
            }

            case '24':{
                $body = explode(':;:', $this->mess[$i]["body"]);

                if ($body[5] == ''){
                    $body[5] = 0;
                }

                return array("hp" => $body[0], "target" => $body[1], "doing" => $body[2], "state" => $body[3], "reason" => $body[4], "t_hp" => $body[5]);
            }

            case '26': {
                $body = explode(':;:', $this->mess[$i]["body"]);

                return array("npc" => $body[0], "target" => $body[1], "diff" => $body[2], "mod" => $body[3], "c_val" => $body[4], "result" => $body[5], "dice" => $body[6]);
            }

            case '27': {
                $body = explode(':;:', $this->mess[$i]['body']);
                return ['dice' => $body[0], 'check' => $body[1], 'target' => $body[2], 'npc' => $body[3]];
            }

            case '30':
			case '29':{

                $body = explode(';', $this->mess[$i]['body']);

                return ['target' => $body[0], 'spell'=> $body[1], 'dice' => $body[2], 'diff' => $body[3], 'attr_mod' => $body[4], 'mod' => $body[5], 'reason' => $body[6],  'total' => $body[7], 'result' => $body[8], 'sid' => $body[9], 'addin' => $body[10]];

            }

            case '31':{

                $body = explode(';', $this->mess[$i]['body']);

                return ['check' => $body[0], 'dice'=> $body[1], 'diff' => $body[2], 'mod' => $body[3], 'total' => $body[4], 'result' => $body[5], 'target' => $body[6]];

            }

            case '32':{

                $body = explode(';', $this->mess[$i]['body']);

                return ['npc' => $body[0], 'target'=> $body[1], 'mod' => $body[2], 'result' => $body[3], 'addon' => $body[4], 'dice' => $body[5]];

            }

            case '33': {

                $body = explode(';', $this->mess[$i]['body']);

                return ['target' => $body[0], 'spell' => $body[1], 'dice' => $body[2], 'factor' => $body[3], 'attr_mod' => $body[4], 'mod' => $body[5], 'trait' => $body[6], 'total' => $body[7], 'type_dmg' => $body[8], 'sid' => $body[9]];

            }

            case '35':{
                $body = explode(';', $this->mess[$i]["body"]);

                return array("map" => $body[0], "active" => $body[1]);
            }

            case '36':{
                $body = explode(';', $this->mess[$i]["body"]);

                return array("dice" => $body[0], "check" => $body[1], "mod" => $body[2], "target" => $body[3], "power" => $body[4], "pid" => $body[5]);
            }

            case '38':{
                $body = explode(';', $this->mess[$i]["body"]);

                return array("spell" => $body[0], "sid" => $body[1]);
            }

            case '40':{
                $body = explode(';', $this->mess[$i]["body"]);

                return array("title" => $body[0], "id" => $body[1]);
            }

            case '45':{
                $body = explode(':;:', $this->mess[$i]["body"]);

                return array("target" => $body[0], "cause" => $body[2], "delta" => $body[3]);
            }

            case '46': {
                $body = explode(';', $this->mess[$i]['body']);
                return ['sid' => $body[0], 'name' => $body[1]];
            }

            default:
                return $this->mess[$i]["body"];
        }
    }

    function get_user_list()
    {
        global $dbh;
        //$query = "SELECT chars.name, chars.cid, chars.s_icon, chars.alias, online.na, chars.inactive FROM online, chars WHERE online.lid = $this->lid AND online.cid = chars.cid ORDER BY chars.name ASC";
        $sth = $dbh->prepare("SELECT chars.name, chars.id, chars.s_icon, chars.alias, online.status FROM online, chars WHERE online.lid = ? AND online.cid = chars.id ORDER BY chars.name ASC");
        $sth->execute(array($this->id));
        $num = $sth->rowCount();

        $names = array();

        if ($num == 0) return 0;
        else
            while ($char = $sth->fetch(PDO::FETCH_ASSOC))
                //$names[] = array("name" => $chars["name"], "id" => $chars["id"], "s_icon" => $chars["s_icon"], "alias" => $chars["alias"], "na" => $chars["na"], "inactive" => $chars["inactive"]);
                $names[] = array("name" => $char["name"], "id" => $char["id"], "s_icon" => $char["s_icon"], "alias" => $char["alias"], "status" => $char["status"]);

        return $names;
    }

    function get_exits()
    {
        global $dbh;

        $exits = array();

        $sth = $dbh->prepare("SELECT locs.id, locs.name, locs.loctype FROM locs, locs_exits WHERE locs_exits.lid_from = ? AND locs_exits.lid_to = locs.id");
        $sth->execute(array($this->id));

        while ($new_loc = $sth->fetch(PDO::FETCH_ASSOC)) {

            $exits[] = array("name" => $new_loc["name"], "lid" => $new_loc["id"], "loctype" => $new_loc["loctype"]);
        }

        return $exits;
    }

    function switch_location($lid, $cid)
    {
        global $dbh, $errors;

        if ($this->check_validity($lid, $cid)) {
            $forge = new Constructor();

            $new_loc = $forge->get_loc_by_id($lid);

            $msg = new Message($cid, $new_loc->name, $this->id, 4);

            if ($msg->post_msg()) {
                $msg = new Message($cid, $this->name, $lid, 5);

                if ($msg->post_msg()) {
                    $sth = $dbh->prepare("UPDATE online SET lid = ? WHERE cid = ?");
                    $sth->execute(array($lid, $cid));

                    $sth = $dbh->prepare("SELECT max(id), max(mtime) FROM mess WHERE cid = ? AND lid = ? AND type = 5");
                    $sth->execute(array($cid, $lid));
                    $num = $sth->rowCount();

                    if ($num != 1) {
                        send_error($errors["e_26"]);
                    }

                    $mess = $sth->fetch(PDO::FETCH_ASSOC);

                    return array("lastmid" => $mess["max(id)"], "mtime" => $mess["max(mtime)"], "body" => $new_loc->name);
                } else {
                    send_error($errors["e_25"]);
                }
            } else {
                send_error($errors["e_24"]);
            }
        } else {
            send_error($errors["e_23"]);
        }
    }

    function check_validity($lid, $cid)
    {
        global $dbh;

        $sth = $dbh->prepare("SELECT lid_to FROM locs_exits WHERE lid_from = $this->id AND lid_to = $lid");
        $sth->execute();
        $num = $sth->rowCount();

        if ($num == 1) {
            $in_array = true;
        } else {
            $in_array = false;
        }

        $sth = $dbh->prepare("SELECT cid FROM online WHERE cid = ? AND lid = $this->id");
        $sth->execute(array($cid));
        $num = $sth->rowCount();

        if ($num == 1)
            $in_place = true;
        else
            $in_place = false;

        return $in_array && $in_place;
    }

    function get_echo($time, $cid, $dm = 0){
        global $dbh;

        $start = $time;
        $end = strtotime("+1 day", $start);
        $temp = [];
        $j = 0;

        $sth = $dbh->prepare("SELECT mess.*, chars.name, chars.color FROM chars, mess WHERE mess.mtime >= ? AND mess.mtime < ? AND mess.cid = chars.id AND mess.lid = ? ORDER BY mess.id ASC");
        $sth->execute([$start, $end, $this->id]);
        $num = $sth->rowCount();

        if ($num == 0) return 0;
        else
            while ($temp[] = $sth->fetch(PDO::FETCH_ASSOC))
                $j++;

        for ($i = $j - 1; $i >= 0; $i--)
            if (($temp[$i]["type"] == 3 && $temp[$i]["private_cid"] != $cid && $temp[$i]["cid"] != $cid) || ($temp[$i]["type"] == 19 && $temp[$i]["cid"] != $cid && !$dm)){
                continue;
            } else {
                $this->mess[] = $temp[$i];
            }

        return $out = array("num" => $j);
    }

    function get_locs(){
        global $dbh, $errors;

        $out = [];

        $sth = $dbh->prepare("SELECT id, name FROM locs ORDER BY name ASC");

        $sth->execute([]);
        $num = $sth->rowCount();

        if ($num == 0){
            send_error($errors["e_48"]);
        }

        while ($loc = $sth->fetch(PDO::FETCH_ASSOC)){
            $out[] = array("id" => $loc["id"], "name" => $loc["name"]);
        }

        return $out;
    }

    function who_where($char){
        global $dbh;

        if ($char->get_dm() == 1)
            $dm = 1;
        else
            $dm = 0;

        $out = array();

        if ($dm == 1)
            $sth = $dbh->prepare("SELECT locs.name as lname, chars.name as cname, online.status as status, online.cip as cip FROM chars, online, locs WHERE online.cid = chars.id AND online.lid = locs.id ORDER BY chars.name ASC");
        else
            $sth = $dbh->prepare("SELECT locs.name as lname, chars.name as cname, online.status as status FROM chars, online, locs WHERE online.cid = chars.id AND online.lid = locs.id ORDER BY chars.name ASC");

        $sth->execute([]);
        $num = $sth->rowCount();

        if ($num == 0){
            return 0;
        } else {
            while ($rec = $sth->fetch(PDO::FETCH_ASSOC)){
                $status = '';

                if ($rec["status"] == 1){
                    $status = ' (N/A)';
                } elseif ($rec["status"] == 2){
                    $status = ' (!)';
                }

                if ($dm == 1)
                    $temp[$rec["lname"]][] = $rec["cname"].$status.' ['.$rec["cip"].']';
                else
                    $temp[$rec["lname"]][] = $rec["cname"].$status;
            }

            foreach ($temp as $key => $value){
                $chars = array();

                foreach ($value as $cname){
                    $chars[] = $cname;
                }

                $out[] = array("lname" => $key, "chars" => $chars);
            }

            return $out;
        }
    }

    function get_npc_list(){
        global $dbh;

        $sth = $dbh->prepare("SELECT mm_locs.*, mm.name as `original` FROM mm_locs, mm WHERE mm_locs.lid = ? AND mm_locs.nid = mm.id ORDER BY mm_locs.name ASC");
        $sth->execute([$this->id]);
        $num = $sth->rowCount();

        if ($num == 0) return false;

        $out = [];

        $out = $sth->fetchAll(PDO::FETCH_ASSOC);

        return $out;
    }

    function get_npc_loclist(){
        global $dbh;

        $out = [];
        $out2 = [];

        $sth = $dbh->prepare("SELECT locs.name as `lname`, mm_locs.name as `mname`, mm.name as `origin_name`, mm_locs.id FROM mm_locs, mm, locs WHERE mm_locs.nid = mm.id AND mm_locs.lid = locs.id ORDER BY locs.name, mm.name ASC");
        $sth->execute([]);
        $num = $sth->rowCount();

        if ($num == 0) return false;

        $list = $sth->fetchAll(PDO::FETCH_ASSOC);

        foreach ($list as $item){
            $out[$item["lname"]][] = ["mname" => $item["mname"].' ('.$item["origin_name"].')', "id" => $item["id"]];
        }

        foreach ($out as $loc => $mms){
            $out2[] = ["lname" => $loc, "mms" => $mms];
        }

        return $out2;
    }

    function get_maps(){
        global $dbh, $errors;

        $sth = $dbh->prepare("SELECT id, name FROM maps WHERE lid = ? ORDER BY name ASC");
        $sth->execute([$this->id]);
        $num = $sth->rowCount();

        if ($num == 0) send_error($errors["e_73"]);

        $out = $sth->fetchAll(PDO::FETCH_ASSOC);

        return $out;
    }
}


//eof
?>