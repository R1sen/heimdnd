<?php

if (!defined('_IN_DWD_')): die; endif;

class Engine
{

    function dice($dice, $type = null, $crit = false)
    {
        $vals = explode('d', $dice);
        $sum = ['int' => 0, 'int_extra' => 0, 'result' => 0, 'str' => ''];
        $first = true;


        if ($crit) {
            $vals[0] *= 2;

            if ($crit >= 2) $vals[0] += $crit - 1;
        }

        for ($i = 1; $i <= trim($vals[0]); $i++) {


            $d = mt_rand(1, $vals[1]);
            $sum["int"] += $d;

            if ($type == 'advantage' || $type == 'disadvantage') {
                $d_extra = mt_rand(1, $vals[1]);
                $sum['int_extra'] += $d_extra;

                switch ($type) {
                    case('advantage'):
                        if ($sum['int'] > $sum['int_extra']) {

                            $sum['result'] = $sum['int'];
                            $sum['str'] = 'с преимуществом <span style="color: orangered">(<b>' . $sum['int'] . '</b> | ' . $sum['int_extra'] . ')</span>';
                            return $sum;

                        } else {

                            $sum['result'] = $sum['int_extra'];
                            $sum['str'] = 'с преимуществом <span style="color: orangered">(<b>' . $sum['int_extra'] . '</b> | ' . $sum['int'] . ')</span>';
                            return $sum;

                        }
                        break;

                    case('disadvantage'):
                        if ($sum['int'] < $sum['int_extra']) {

                            $sum['result'] = $sum['int'];
                            $sum['str'] = 'с помехой <span style="color: orangered">(<b>' . $sum['int'] . '</b> | ' . $sum['int_extra'] . ')</span>';
                            return $sum;

                        } else {

                            $sum['result'] = $sum['int_extra'];
                            $sum['str'] = 'с помехой <span style="color: orangered">(<b>' . $sum['int_extra'] . '</b> | ' . $sum['int'] . ')</span>';
                            return $sum;
                        }
                        break;
                }
            }


            if ($first) {
                $sum['str'] = $d;
                $first = false;
            } else {
                $sum['str'] .= " + " . $d;
            }
        }

        $sum['str'] = '<span style="color: orangered">(' . $sum['str'] . ')</span>';
        $sum['result'] = $sum['int'];

        return $sum;
    }


    function str_result_check($type, $object, $char, $diff, $attr, $mod, $options)
    {

        global $errors;

        $message = '';
        $dice = null;

        if (($options['adv'] == 1 && $options['disadv'] == 1) || ($options['adv'] == null && $options['disadv'] == null))
            $dice = $this->dice('1d20');
        elseif ($options['adv'] == 1)
            $dice = $this->dice('1d20', 'advantage');
        elseif ($options['disadv'] == 1)
            $dice = $this->dice('1d20', 'disadvantage');


        $overall_res = $dice['result'] + $attr + $mod;

        if ($dice['result'] == 20) {
            $result['str'] = 'КРИТИЧЕСКИЙ УСПЕХ!';
        } elseif ($dice['result'] == 1) {
            $result['str'] = 'КРИТИЧЕСКИЙ ПРОВАЛ!';
        } elseif ($overall_res >= $diff) {
            $result['str'] = 'УСПЕХ!';
        } else {
            $result['str'] = 'ПРОВАЛ!';
        }

        switch ($type) {

            case 'characteristic':

                $message = 'проверку характеристики <b>' . $object . '</b>;' . $dice['str'] . ';' . $diff . ';' . $attr . ';' . $mod . ';' . $options['reason'] . ';' . $overall_res . ';' . $result['str'];

                break;

            case 'save':

                $message = 'спасбросок по <b>' . $object . '</b>;' . $dice['str'] . ';' . $diff . ';' . $attr . ';' . $mod . ';' . $options['reason'] . ';' . $overall_res . ';' . $result['str'];

                break;

            case 'ability':

                $message = 'проверку навыка <b>' . $object . '</b>;' . $dice['str'] . ';' . $diff . ';' . $attr . ';' . $mod . ';' . $options['reason'] . ';' . $overall_res . ';' . $result['str'];

                break;

        }

        $post = new Message($char['id'], $message, $char['lid'], 6);
        if ($post->post_msg())
            return ['success' => true];
        else
            send_error($errors["e_20"]);
    }


    /**
     * @param $char
     * @param $options
     * @return array
     */
    //function make_attack($char, $target, $type, $mod = 0, $advantage = false, $hitch = false, $fencing = false, $heavy_trow = false,  $hand = true,
    //                   $improvised = false) {
    function make_attack($char, $options)
    {
        global $errors;

        $attr_mod = 0;

        if ($options['hand'] == 1)
            $weapon_object = $char->offhand_object;
        else
            $weapon_object = $char->mainhand_object;

        if ($weapon_object->id != 0 && $weapon_object->base != 'weapon')
            send_error($errors['e_42']);

        /*if ($options['hand'] == 1 && $char->pair_prof == false && !in_array('лёгкое', $char->mainhand_object->base_object->properties)
            && !in_array('лёгкое', $char->offhand_object->base_object->properties)
        )
            send_error($errors['e_40']);*/

        if ($weapon_object->id == 0 && $options['improv'] == 0) {
            $weapon_object = new Item('unarmed');
        } elseif ($options['improv'] == 1) {
            $weapon_object = new Item('improvised melee');
        } elseif ($options['improv'] == 2) {
            $weapon_object = new Item('improvised ranged');
        }


        eval($char->active_triggers(['onAttack']));

        if (in_array($weapon_object->base_object->name, $char->profs) || $weapon_object->base_object->name == 'Безоружный удар') {
            $attr_mod += $char->prof_bonus;
        }

        $reason = '';

        if ($char->penalty_weight == 'heavy' || $char->penalty_weight == 'overloaded') {

            $options['disadv'] = 1;
            $reason .= 'Сильный перегруз. ';

        } elseif ($char->penalty_armor == true) {

            $options['disadv'] = 1;
            $reason .= 'Ношение брони без владения. ';

        } elseif ($char->size == 'Маленький' && in_array('тяжёлое', $weapon_object->base_object->properties)) {

            $options['disadv'] = 1;
            $reason .= 'Тяжёлое оружие для маленького персонажа. ';

        }

        if ($reason == '') $reason = '-';


        if (($options['adv'] == 1 && $options['disadv'] == 1) || ($options['adv'] == null && $options['disadv'] == null))
            $dice = $this->dice('1d20');
        elseif ($options['adv'] == 1)
            $dice = $this->dice('1d20', 'advantage');
        elseif ($options['disadv'] == 1)
            $dice = $this->dice('1d20', 'disadvantage');

        $melee_mod = $this->get_mod($char->str) + $char->attr_melee;
        $ranged_mod = $this->get_mod($char->dex) + $char->attr_ranged;

        if (in_array('фехтовальное', $weapon_object->base_object->properties)) {
            $attr_mod += ($melee_mod > $ranged_mod) ? $melee_mod : $ranged_mod;
        } elseif ($this->is_in_str($weapon_object->base_object->cat, 'melee')) {
            $attr_mod += $melee_mod;
        } elseif ($this->is_in_str($weapon_object->base_object->cat, 'ranged')) {
            $attr_mod += $ranged_mod;
        }

        $overall_res = $dice['result'] + $attr_mod + $options['mod'];


        if ($dice['result'] >= $char->crit_score) {

            $result = 'КРИТИЧЕСКОЕ ПОПАДАНИЕ - ДВОЙНОЙ УРОН!';

        } elseif ($dice['result'] == 1) {

            $result = 'АВТОМАТИЧЕСКИЙ ПРОМАХ!';

        } else {

            if ($overall_res >= (integer)$options['target']->ac)
                $result = 'ПОПАДАНИЕ!';

            else $result = 'ПРОМАХ!';
        }

        $body = $options['target']->name . ';' . $dice['str'] . ';' . $options['diff'] . ';' . $attr_mod . ';' . $options['mod'] . ';' . $reason . ';' . $weapon_object->name . ';' . $overall_res . ';' . $result;

        $message = new Message($char->id, $body, $char->lid, 16);

        if ($message->post_msg())
            $out = ['success' => true];
        else
            send_error($errors["e_20"]);

        return $out;

    }

    function make_dmg_check($char, $options)
    {
        global $errors, $dmg;

        $attr_mod = 0;
        $trait = '-';

        if ($options['hand'] == 1)
            $weapon_object = $char->offhand_object;
        else
            $weapon_object = $char->mainhand_object;

        if ($weapon_object->id != 0 && $weapon_object->base != 'weapon')
            send_error($errors['e_42']);

        if ($weapon_object->id == 0 && $options['improv'] == 0) {
            $weapon_object = new Item('unarmed');
        } elseif ($options['improv'] == 1) {
            $weapon_object = new Item('improvised melee');
        } elseif ($options['improv'] == 2) {
            $weapon_object = new Item('improvised ranged');
        }

        eval($char->active_triggers(['onDamage']));

        if ($this->is_in_str($weapon_object->base_object->properties, 'универсальное') && (($options['hand'] == 1 && $char->mainhand_object->base == 'mainhand') || ($options['hand'] == 0 && $char->offhand_object->base == 'offhand'))){

            $regex = '/(?<=универсальное\s\()\d{1,2}d\d{1,2}/';
            preg_match($regex, implode ($weapon_object->base_object->properties), $res);

            $weapon_object->base_object->dice = $res[0];

        }

        $dice = $this->dice($weapon_object->base_object->dice, null, $options['crit']);

        if (($options['pair_attack'] == 0 || $char->pair_prof == true)) {
            $melee_mod = $this->get_mod($char->str);
            $ranged_mod = $this->get_mod($char->dex);
        } else {
            $melee_mod = ($this->get_mod($char->str) >= 0) ? 0 : $this->get_mod($char->str);
            $ranged_mod = ($this->get_mod($char->dex) >= 0) ? 0 : $this->get_mod($char->dex);
        }

        if (in_array('фехтовальное', $weapon_object->base_object->properties)) {
            $attr_mod += ($melee_mod > $ranged_mod) ? $melee_mod : $ranged_mod;
        } elseif ($this->is_in_str($weapon_object->base_object->cat, 'melee')) {
            $attr_mod += $melee_mod;
        } elseif ($this->is_in_str($weapon_object->base_object->cat, 'ranged')) {
            $attr_mod += $ranged_mod;
        }



        $attr_mod += $char->attr_dmg;

        $overall_res = $dice['result'] + $options['mod'] + $attr_mod;

        $type_dmg = $dmg[$weapon_object->base_object->dmg_type];

        if (($this->is_in_str($options['target']->dmg_immune, $type_dmg) && !$this->is_in_str($options['target']->dmg_immune, 'немагического'))
            || ($this->is_in_str($options['target']->dmg_immune, $type_dmg) && $this->is_in_str($options['target']->dmg_immune, 'немагического') && $weapon_object->enchant_id == 0)
        ) {

            $trait = 'Иммунитет к урону (' . $type_dmg . ')';
            $overall_res = 0;

        } elseif ($this->is_in_str($options['target']->vulnerab, $type_dmg)) {

            $trait = 'Уязвимость к урону (' . $type_dmg . ')';
            $overall_res *= 2;

        } elseif (($this->is_in_str($options['target']->resist, $type_dmg) && !$this->is_in_str($options['target']->resist, 'немагического'))
            || ($this->is_in_str($options['target']->resist, $type_dmg) && $this->is_in_str($options['target']->resist, 'немагического') && $weapon_object->enchant_id == 0)
            || ($this->is_in_str($options['target']->resist, $type_dmg) && $this->is_in_str($options['target']->resist, 'от магического') && $weapon_object->enchant_id != 0)
        ) {
            $trait = 'Сопротивление к урону (' . $type_dmg . ')';
            $overall_res = round($overall_res / 2, 0, PHP_ROUND_HALF_DOWN);
        }

        $wname = $weapon_object->name;

        if ($options['crit']) $wname .= ' <span style="color: red"><small>крит!</small></span>';

        $body = $options['target']->name . ';' . $dice['str'] . ';' . $attr_mod . ';' . $options['mod'] . ';' . $trait . ';' . $wname . ';' . $overall_res . ';' . $type_dmg;

        $message = new Message($char->id, $body, $char->lid, 20);

        if ($message->post_msg())
            $out = ['success' => true];
        else
            send_error($errors["e_20"]);

        return $out;

    }


    function make_spell_attack($str, $char, $options)
    {
        global $errors;

        $eng = new Engine();

        $regex = '/(спасброс|рукопашную|дальнобойную)(?>ок|ке|ки|)\s(силы|телосложения|ловкости|интеллекта|мудрости|харизмы|атаку заклинанием)/ui';

        if (!preg_match($regex, $str, $res)) {
            send_error($errors['e_62']);
        }

        $options['reason'] = '';

        switch ($res[1]) {

            case 'спасброс':

                if ($options['clid'] != 0) {
                    $options['diff'] = $char->spells_slots[$options['clid']]['save'];
                } elseif ($options['ability'] != null) {
                    $options['diff'] = 8 + $eng->get_mod($char->{$options['ability']}) + $char->prof_bonus;
                } elseif (isset($char->spells_slots[0][$options["spell"]->id]['save'])) {
                    $options['diff'] = $char->spells_slots[0][$options["spell"]->id]['save'];
                } else {
                    $options['diff'] = $char->spells_slots[0]["race_default"]['save'];
                }

                $array_penalty_armor = ['силы', 'ловкости'];
                $array_penalty_weight = ['силы', 'ловкости', 'телосложения'];

                if ((in_array($res[2], $array_penalty_armor) || in_array($res[0], $array_penalty_armor)) && ($options['target']->penalty_weight == 'heavy' || $options['target']->penalty_weight == 'overloaded')) {

                    $options['disadv'] = 1;
                    $options['reason'] .= 'Сильный перегруз. ';

                } elseif ((in_array($res[2], $array_penalty_weight) || in_array($res[0], $array_penalty_weight)) && $options['target']->penalty_armor == true) {

                    $options['disadv'] = 1;
                    $options['reason'] .= 'Ношение брони без владения. ';

                }

                if ($options['reason'] == '') $options['reason'] = '-';


                switch ($res[2]) {

                    case 'Силы':
                    case 'силы':

                        $options['attr'] = $options['target']->str_save;
                        $out = $this->throw_save(['id' => $char->id, 'lid' => $char->lid, 'res' => $res[2]], $options);
                        break;

                    case 'Ловкости':
                    case 'ловкости':

                        $options['attr'] = $options['target']->dex_save;
                        $out = $this->throw_save(['id' => $char->id, 'lid' => $char->lid, 'res' => $res[2]], $options);
                        break;

                    case 'Телосложения':
                    case 'телосложения':

                        $options['attr'] = $options['target']->con_save;
                        $out = $this->throw_save(['id' => $char->id, 'lid' => $char->lid, 'res' => $res[2]], $options);
                        break;

                    case 'Интеллекта':
                    case 'интеллекта':

                        $options['attr'] = $options['target']->intl_save;
                        $out = $this->throw_save(['id' => $char->id, 'lid' => $char->lid, 'res' => $res[2]], $options);
                        break;

                    case 'Мудрости':
                    case 'мудрости':

                        $options['attr'] = $options['target']->wis_save;
                        $out = $this->throw_save(['id' => $char->id, 'lid' => $char->lid, 'res' => $res[2]], $options);
                        break;

                    case 'Харизмы':
                    case 'харизмы':

                        $out = $this->throw_save(['id' => $char->id, 'lid' => $char->lid, 'res' => $res[2]], $options);
                        break;

                    default:
                        send_error($errors["e_64"]);
                }

                break;

            case 'рукопашную':

                $options['mode'] = 'рукопашная';
                $out = $this->spell_attack($char, $options);

                break;

            case 'дальнобойную':

                $options['mode'] = 'дальнобойная';
                $out = $this->spell_attack($char, $options);

                break;

            default:
                send_error($errors["e_65"]);

        }

        return $out;

    }

    function throw_save($char, $options)
    {

        global $errors;

        if (($options['adv'] == 1 && $options['disadv'] == 1) || ($options['adv'] == null && $options['disadv'] == null))
            $dice = $this->dice('1d20');
        elseif ($options['adv'] == 1)
            $dice = $this->dice('1d20', 'advantage');
        elseif ($options['disadv'] == 1)
            $dice = $this->dice('1d20', 'disadvantage');

        $summ = $options['attr'] + $options['mod'];
        $dice['result'] += $summ;

        if ($dice['result'] >= $options['diff']) {
            $result['str'] = 'УСПЕХ!';
            $addin = 'ЗАКЛИНАНИЕ ПРОВАЛ!';
        } else {
            $result['str'] = 'ПРОВАЛ!';
            $addin = 'ЗАКЛИНАНИЕ УСПЕХ!';
        }

        $body = $options['target']->name . ';' . $options['spell']->name . ';' . ' спасбросок ' . $char['res'] . ' ' . $dice['str'] . ';' . $options['diff'] . ';' . $options['attr'] . ';' . $options['mod'] . ';' . $options['reason'] . ';' . $dice['result'] . ';' . $result['str'] . ';' . $options['spell']->id . ';' . $addin;

        $message = new Message($char['id'], $body, $char['lid'], 29);

        if ($message->post_msg())
            $out = ['success' => true];
        else
            send_error($errors["e_20"]);

        return $out;
    }

    function spell_attack($char, $options)
    {

        global $errors;

        $options['reason'] = '-';
        $eng = new Engine();

        if (($options['adv'] == 1 && $options['disadv'] == 1) || ($options['adv'] == null && $options['disadv'] == null))
            $dice = $this->dice('1d20');
        elseif ($options['adv'] == 1)
            $dice = $this->dice('1d20', 'advantage');
        elseif ($options['disadv'] == 1)
            $dice = $this->dice('1d20', 'disadvantage');

        if ($options['clid'] != 0) {
            $options['attr'] = $char->spells_slots[$options['clid']]['atk'];
        } elseif ($options['ability'] != null) {
            $options['attr'] = $eng->get_mod($char->{$options['ability']}) + $char->prof_bonus;
        } elseif (isset($char->spells_slots[0][$options["spell"]->id]['atk'])) {
            $options['attr'] = $char->spells_slots[0][$options["spell"]->id]['atk'];
        } else {
            $options['attr'] = $char->spells_slots[0]["race_default"]['atk'];
        }

        $overall_res = $dice['result'] + $options['attr'] + $options['mod'];

        if ($dice['result'] >= $char->crit_score) {

            $result = 'КРИТИЧЕСКОЕ ПОПАДАНИЕ - ДВОЙНОЙ УРОН!';

        } elseif ($dice['result'] == 1) {

            $result = 'АВТОМАТИЧЕСКИЙ ПРОМАХ!';

        } else {

            if ($overall_res >= (integer)$options['target']->ac)
                $result = 'ПОПАДАНИЕ!';

            else $result = 'ПРОМАХ!';
        }

        $body = $options['target']->name . ';' . $options['spell']->name . ';' . $options['mode'] . ' атака заклинанием ' . $dice['str'] . ';' . $options['diff'] . ';' . $options['attr'] . ';' . $options['mod'] . ';' . $options['reason'] . ';' . $overall_res . ';' . $result . ';' . $options['spell']->id;

        $message = new Message($char->id, $body, $char->lid, 30);

        if ($message->post_msg())
            $out = ['success' => true];
        else
            send_error($errors["e_20"]);

        return $out;

    }

    function make_spell_damage($str, $char, $options)
    {

        global $errors, $spell_dmg, $dmg;

        $trait = '-';

        $regex = '/(Урон|урон)?\s?(колющий|рубящий|дробящий|огнем|звуком|излучением|кислотой|некротической\sэнергией|психической\sэнергией|силовым\sполем|холодом|электричеством|ядом|излучением\sи\sогнём)?\s?(\d{1,2}[кдd]{1}\d{1,2})/ui';

        if (!preg_match($regex, $str, $res)) {
            send_error($errors['e_68']);
        }

        $res[3] = preg_replace("/к/", "d", $res[3]);
        $res[3] = preg_replace("/д/", "d", $res[3]);

        if (!preg_match('/(\d{1,2})(d\d{1,2})/', $res[3], $dice_matches)) {
            send_error($errors['e_69']);
        }

        $dice_matches[1] += (1 * $options['factor']);

        $dice = $this->dice($dice_matches[1] . $dice_matches[2], null, $options['crit']);

        $overall_res = $dice['result'] + $options['mod'] + $char->attr_spell_dmg;

        $type_dmg = isset($spell_dmg[$res[2]]) ? $spell_dmg[$res[2]] : 'неопределённый урон';

        if (($this->is_in_str($options['target']->dmg_immune, $type_dmg) && in_array($type_dmg, $dmg) && !$this->is_in_str($options['target']->dmg_immune, 'немагического'))
            || ($this->is_in_str($options['target']->dmg_immune, $type_dmg) && !in_array($type_dmg, $dmg))
        ) {

            $trait = 'Иммунитет к урону (' . $type_dmg . ')';
            $overall_res = 0;

        } elseif ($this->is_in_str($options['target']->vulnerab, $type_dmg)) {

            $trait = 'Уязвимость к урону (' . $type_dmg . ')';
            $overall_res *= 2;

        } elseif ($this->is_in_str($options['target']->resist, $type_dmg) && in_array($type_dmg, $dmg) && $this->is_in_str($options['target']->resist, 'от магического')
        || ($this->is_in_str($options['target']->resist, $type_dmg) && !in_array($type_dmg, $dmg))
        ) {

            $trait = 'Сопротивление к урону (' . $type_dmg . ')';
            $overall_res = round($overall_res / 2, 0, PHP_ROUND_HALF_DOWN);

        }

        if ($options['crit']) $dice['str'] .= ' <span style="color: red"><small>крит!</small></span>';

        $body = $options['target']->name . ';' . $options['spell']->name . ';' . $dice['str'] . ';' . $options['factor'] . ';' . $char->attr_spell_dmg . ';' . $options['mod'] . ';' . $trait . ';' . $overall_res . ';' . $type_dmg . ';' . $options['spell']->id;

        $message = new Message($char->id, $body, $char->lid, 33);

        if ($message->post_msg())
            $out = ['success' => true];
        else
            send_error($errors["e_20"]);

        return $out;


    }


    function get_mod($attr)
    {
        return floor(($attr - 10) / 2);
    }

// проверка встречается ли подстрока в строке
    function is_in_str($str, $substr)
    {
        if (!is_array($str)) {
            $result = strpos($str, $substr);
            if ($result === FALSE)
                return false;
            else
                return true;
        } else {
            foreach ($str as $hay) {
                $result = strpos($hay, $substr);
                if ($result === FALSE)
                    continue;
                else
                    return true;
            }
        }

        return false;

    }

    /*Заготовка для урона


    $regex = '/(Урон|урон)?\s?(огнём|звуком|излучением|кислотой|некротической\sэнергией|психической\sэнергией|силовым\sполем|холодом|электричеством|ядом|излучением\sи\sогнём)?\s?(\d{1,2}d\d{1,2})/';

        if (!preg_match($regex, $str, $res)) {
            send_error($errors['e_31']);
            die;
        }

        if ($res[2] != '')
            $dmg_kind = $res[2];
        else
            $dmg_kind = '';

        //if ($res[1] == '')
        //	$dmg_kind .= " (нанесённое не является уроном, а простым броском кубика - см. описание таланта)";

        $dice = $res[3];

        // $throw = $this->dice($dice, $crit);
        //$c_value[0] = $throw["int"];
        //$final = "<font color=\"yellow\">" . $throw["str"] . "</font>";


    /*
        function type_throw($throw, $throw_extra = null, $mode){

            switch($mode){
                case('advantage'):
                    if($throw['int'] >= $throw_extra['int'])
                        return ['active' => $throw, 'passive' =>$throw_extra];
                    else
                        return ['active' => $throw_extra, 'passive' =>$throw];
                break;

                case('disadvantage'):
                    if($throw['int'] <= $throw_extra['int'])
                        return ['active' => $throw, 'passive' =>$throw_extra];
                    else
                        return ['active' => $throw_extra, 'passive' =>$throw];
                    break;
                case('normal'):
                        return ['active' => $throw, 'passive' => 0];
                    break;
            }
        }

    */
}


