<?php

if (!defined("_IN_DWD_")): die; endif;

Class Feat
{

    public $effects;

    function __construct($get_effects = null)
    {

        if ($get_effects == true) {

            global $dbh;

            $sth = $dbh->prepare('SELECT events.effect, events.when_active FROM events WHERE events.type_id = ? AND events.type = ? ');
            $sth->execute([$this->id, 'feat']);

            $this->effects = $sth->fetchall(PDO::FETCH_ASSOC);

        }


    }

}

?>